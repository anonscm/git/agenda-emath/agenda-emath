# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('eventsmgmt', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Collector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Beginning the parsing at')),
                ('finished_on', models.DateTimeField(null=True, verbose_name='parsing finished at', blank=True)),
                ('check_status', models.TextField(verbose_name='status after collecting URL', blank=True)),
                ('count_failed_check', models.IntegerField(default=0, verbose_name='increment if collect failed')),
                ('limit_events_to', models.IntegerField(default=9999, verbose_name='limit number of collected events')),
                ('calendar', models.ForeignKey(to='eventsmgmt.MyCalendar')),
            ],
        ),
    ]
