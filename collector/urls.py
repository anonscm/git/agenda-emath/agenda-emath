__author__ = 'me'

from django.conf.urls import patterns, url, include

urlpatterns = patterns('',
    url(r'^calendars/(?P<pk>\d+)/collect/$', 'collector.views.collect', name='collect'),

    # TODO
    # url(r'^calendars/errors/$', MyCalendarErrorsListView.as_view(
    #         model=MyCalendar,
    #         template_name='calendar/list.html'
    #     ), name='err_calendars'),
)

