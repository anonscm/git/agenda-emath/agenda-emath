# config valid only for current version of Capistrano
#lock '3.7'
#set :default_environment, {'PBR_VERSION' => '3.0.0'}

set :application, 'agenda'
set :repo_url, 'git+ssh://git@git.renater.fr:2222/agenda-emath.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, ENV['BRANCH'] if ENV['BRANCH']

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/tmp/capagenda'

set :django_project_dir, ''
set :django_settings_dir, 'agenda'
set :django_settings, 'settings.py'
set :pip_requirements, 'requirements/prod.txt'

# Default value for :scm is :git
#set :scm, :git

set :user, "deploy"
set :use_sudo, false

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push("#{fetch(:django_settings_dir)}/settings_local.py")

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

#  after "deploy", "django:restart_daemons" unless ENV['NORESTART']
end
