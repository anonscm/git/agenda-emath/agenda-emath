from django.conf import settings # import the settings file

def context(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {'OAUTH2_PROVIDER_NAME': settings.OAUTH2_PROVIDER_NAME}