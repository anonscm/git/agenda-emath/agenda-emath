# -*- coding: utf-8 -*-
__author__ = 'me'

import logging

from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.db.models.signals import pre_delete
from django.db.models.signals import pre_save
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.core.mail import EmailMessage
from django.core import mail
from django.core.urlresolvers import reverse


from eventsmgmt.models import MyEvent
from eventsmgmt.models import MyCalendar

from agenda.settings import ADMINS
from agenda.settings_eventsmgmt import MAIL_TO_AUTH
from agenda.settings_eventsmgmt import EMAIL_SUBJECT_PREFIX_ANN_CREATED
from agenda.settings_eventsmgmt import EMAIL_SUBJECT_PREFIX_CAL_CREATED
from agenda.settings_eventsmgmt import EMAIL_SUBJECT_PREFIX_ANN_DELETED
from agenda.settings_eventsmgmt import DEFAULT_FROM_EMAIL
from agenda.settings_local import URL_PORTAIL
from agenda.settings_local import URL_CONNECT
from agenda.settings_local import REPLY_TO
from agenda.settings_eventsmgmt import PART1, PART2, PART3
from agenda.settings_eventsmgmt import X_HEADER


@receiver(user_logged_in)
def sig_user_logged_in(sender, user, request, **kwargs):
    logger = logging.getLogger(__name__)
    if request.META and 'REMOTE_ADDR' in request.META:
        logger.info("user logged in: %s at %s" % (user, request.META['REMOTE_ADDR']))
    else:
        logger.info("user logged in: %s" % user)


@receiver(user_logged_out)
def sig_user_logged_out(sender, user, request, **kwargs):
    logger = logging.getLogger(__name__)
    if request.META and 'REMOTE_ADDR' in request.META:
        logger.info("user logged out: %s at %s" % (user, request.META['REMOTE_ADDR']))
    else:
        logger.info("user logged out: %s" % user)


# on envoie un mail aux admins uniquement à la création d'un nouveau calendrier
@receiver(post_save, sender=MyCalendar)
def sig_cal_created(sender, instance, created, raw, **kwargs):
    """
    Send an email when add a calendar. Send it to admins and user
    :param sender:
    :param instance:
    :param kwargs:
    :return:
    """
    if raw:
        return
    logger = logging.getLogger(__name__)
    send_email = False
    msg = ''
    if created:
        send_email= True
        # update_fields de post_save n'est pas utilisable ;)
        msg = u"calendar created: %s with created_by %s" % (instance, instance.created_by.email)
        logger.info(msg)

        if MAIL_TO_AUTH and send_email:
            email = EmailMessage(subject=u"%s %s %s" % (PART1, u" calendrier créé ", instance),
                                body=u'Title is "%s" \nSee  %s\n%s' % (instance.title,
                                     u'%s%s' % (URL_CONNECT ,reverse('admin:eventsmgmt_mycalendar_change', args=[instance.id])), msg),
                                from_email=PART3,
                                to=[PART3],
                                headers={
                                     'Reply-To': REPLY_TO,
                                     'X-ACM': X_HEADER
                                })

            email.send()
            logger.debug("sent to %s " % (PART3))
        elif not MAIL_TO_AUTH:
            logger.info("mail not send by configuration see MAIL_TO_AUTH ")
        else:
            logger.info("mail not send: modification not creation")
    return




# ce qui suit avait été fait par Gérard pour chaque modification de calendar ou event
#

# @receiver(pre_save, sender=MyCalendar)
# def sig_cal_created1(sender, instance, raw, **kwargs):
#     """
#     Send an email when add a calendar. Send it to admins and user
#     Here we just remember what fields changed
#     :param sender:
#     :param instance:
#     :param kwargs:
#     :return:
#     """
#     if raw:
#         return
#     logger = logging.getLogger(__name__)
#     try:
#         old_object = sender.objects.get(pk=instance.pk)
#         if old_object.url_to_parse != instance.url_to_parse:
#             instance._url_to_parse_old = old_object.url_to_parse
#         if old_object.published != instance.published:
#             instance._published_old = old_object.published
#         if old_object.title != instance.title:
#             instance._title_old = old_object.title
#         if old_object.enabled != instance.enabled:
#             instance._enabled_old = old_object.enabled
#     except sender.DoesNotExist:
#         pass # Object is new, so field hasn't technically changed, but you may want to do something else here.
#
#
# #pre_save.connect(sig_cal_created1, sender=MyCalendar, dispatch_uid="mymy_unique_identifier")


# @receiver(post_save, sender=MyCalendar)
# def sig_cal_created(sender, instance, created, raw, **kwargs):
#     """
#     Send an email when add a calendar. Send it to admins and user
#     :param sender:
#     :param instance:
#     :param kwargs:
#     :return:
#     """
#     if raw:
#         return
#     logger = logging.getLogger(__name__)
#     send_email = False
#     msg = ''
#     if created:
#         logger.info(u"calendar created: %s" % instance)
#         send_email= True
#     else:
#         # update_fields de post_save n'est pas utilisable ;)
#         logger.info(u"calendar modified: %s with created_by %s" % (instance,instance.created_by.email) )
#         if hasattr(instance, '_published_old') and instance._published_old != instance.published:
#             send_email = True
#             if instance.published:
#                 msg = "Your calendar will be published and visible on Portail des Maths"
#             else:
#                 msg = "Your calendar won't be published on Portail des Maths"
#         if hasattr(instance, '_enabled_old') and instance._enabled_old != instance.enabled:
#             send_email = True
#             if instance.enabled:
#                 msg = "Your calendar will be collected, events will be available to Portail des Maths"
#             else:
#                 msg = "Your calendar won't be collected, events never refreshed on Portail des Maths"
#         if MAIL_TO_AUTH and send_email:
#             email = EmailMessage(subject="%s %s %s" % (PART1, u" calendrier modifié ", instance),
#                                 body='Title is "%s" \nSee %s/calendars/%s \n%s' % (instance.title,
#                                      URL_CONNECT, instance.id, msg),
#                                 from_email=PART3,
#                                 to=[PART3, instance.created_by.email],
#                                 headers={
#                                      'Reply-To': REPLY_TO,
#                                      'X-ACM': X_HEADER
#                                 })
#
#             email.send()
#             logger.debug("sent to %s et %s" % (PART3,instance.created_by.email))
#         elif not MAIL_TO_AUTH:
#             logger.info("mail not send by configuration see MAIL_TO_AUTH ")
#         else:
#             logger.info("mail not send: modification not creation")
#         return
#
#     if not instance.parse and not instance.category and not instance.title:
#         # creation is incomplete
#         return
#
#     message = ''
#     if MAIL_TO_AUTH and send_email:
#         email = EmailMessage(subject="%s %s %s" % (PART1, EMAIL_SUBJECT_PREFIX_CAL_CREATED, instance),
#                              body='Title is "%s" See %s/calendars/%s' % (instance.title,
#                                  URL_CONNECT, instance.id,),
#                              from_email=PART3,
#                              to=[PART3, instance.created_by.email],
#                              headers={
#                                  'Reply-To': REPLY_TO,
#                                  'X-ACM': X_HEADER
#                              })
#
#         email.send()
#         logger.debug("sent to %s" % PART3)
#     else:
#         logger.info("mail not send")
#
# @receiver(post_save, sender=MyEvent)
# def confirm_creation_by_mail(sender, instance, created, raw, **kwargs):
#     """
#     Sent an email to confirm creation of announce, not event, the difference is done by the value of category
#     :param sender:
#     :param instance:
#     :param kwargs:
#     :return:
#     """
#     if raw:
#         return
#     if not instance.calendar:
#         logger = logging.getLogger(__name__)
#         if created:
#             logger.debug("-- Begin create event -- ")
#             logger.debug("Request finished! %s" % instance.id)
#         else:
#             logger.debug("-- Modified -- ")
#         message = ''
#         if MAIL_TO_AUTH and instance.created_by.email:
#             to = [instance.created_by.email,]
#         else:
#             to = tuple(a[1] for a in ADMINS)
#         bcc = tuple(a[1] for a in ADMINS)
#         logger.debug("MAIL_TO_AUTH: %s sent to %s and bcc to %s" % (MAIL_TO_AUTH, to, bcc))
#
#         subject, from_email, to = '[%s] %s' % (
#         Site.objects.get_current().name, EMAIL_SUBJECT_PREFIX_ANN_CREATED), DEFAULT_FROM_EMAIL, to
#         current_site = Site.objects.get_current()
#         html_content = render_to_string('announce/created_mail.html', {
#             'url': 'http://%s' % current_site.domain,
#             'site': current_site.name,
#             'announce': instance,
#             'user': instance.created_by,
#             'MAIL_TO_AUTH': MAIL_TO_AUTH,
#             'URL_PORTAIL': URL_PORTAIL,
#             'URL_CONNECT': URL_CONNECT,
#         }) # ...
#         text_content = strip_tags(html_content) # this strips the html, so people will have the text as well.
#         logger.debug("mail is ready to sent")
#         try:
#             msg = EmailMultiAlternatives(subject, text_content, from_email, to, bcc=bcc,
#                                          headers={
#                                              'Reply-To': REPLY_TO,
#                                              'X-ACM': X_HEADER
#                                          })
#             msg.attach_alternative(html_content, "text/html")
#             msg.send()
#             logger.debug("mail sent")
#         except IOError, e:
#             logger.error("Unable to send message: %s" % e)
#
# @receiver(pre_delete, sender=MyEvent)
# def confirm_deletion_by_mail(sender, instance, **kwargs):
#     """
#     Sent an email to confirm creation of announce, not event, the difference is done by the value of category
#     :param sender:
#     :param instance:
#     :param kwargs:
#     :return:
#     """
#     if not instance.calendar:
#         logger = logging.getLogger(__name__)
#         logger.debug("-- Begin delete event -- ")
#         logger.debug("Request finished! %s" % instance.id)
#         message = ''
#         if MAIL_TO_AUTH and instance.created_by.email:
#             to = [instance.created_by.email]
#         else:
#             to = [a[1] for a in ADMINS]
#         logger.debug("sent to %s" % to)
#
#         subject, from_email, to = '[%s] %s' % (
#         Site.objects.get_current().name, EMAIL_SUBJECT_PREFIX_ANN_DELETED), DEFAULT_FROM_EMAIL, to
#         current_site = Site.objects.get_current()
#         html_content = render_to_string('announce/deleted_mail.html', {
#             'url': 'http://%s' % current_site.domain,
#             'site': current_site.name,
#             'announce': instance,
#             'user': instance.created_by,
#             'MAIL_TO_AUTH': MAIL_TO_AUTH,
#             'URL_PORTAIL': URL_PORTAIL,
#             'URL_CONNECT': URL_CONNECT,
#         }) # ...
#         text_content = strip_tags(html_content) # this strips the html, so people will have the text as well.
#         logger.debug("mail is ready to sent")
#         try:
#             msg = EmailMultiAlternatives(subject, text_content, from_email, to, bcc=[a[1] for a in ADMINS],
#                                          headers={
#                                              'Reply-To': REPLY_TO,
#                                              'X-ACM': X_HEADER
#                                          })
#             msg.attach_alternative(html_content, "text/html")
#             msg.send()
#             logger.debug("mail sent")
#         except IOError, e:
#             logger.error("Unable to send message: %s" % e)
#
#
