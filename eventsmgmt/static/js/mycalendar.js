/**
 * Created by me on 11/10/14.
 */
// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}
$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
$(document).ready(function () {
    // to choose default calendar
    $('.set_default_cal').click(function () {
        if ($(this).text() == "Default") {
            $(this).replaceWith("<div>" + $(this).text('Set as default?') + "</div>");
            $.ajax({
                    type: 'post',
                    url: '/ajax_set_default_cal/',
                    data: {'user': $("input[name=user]").val(),
                        'sub_id': $(this).attr('id')},
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.statusText);
                        alert(thrownError);
                    }
                }
            );
        }
        else if ($(this).text() == "Set as default?") {
            $(this).replaceWith("<div>" + $(this).text('Default') + "</div>");
            $.ajax({
                    type: 'post',
                    url: '/ajax_set_default_cal/',
                    data: {'user': $("input[name=user]").val(),
                        'sub_id': $(this).attr('id')},
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.statusText);
                        alert(thrownError);
                    }
                }
            );
        }
        return false;

    });

/*
    $('#envoyer').click(function () {
        $( "#monForm" ).trigger('submit');
        return false;
    });
*/

    $( "#monForm" ).submit(function() {
        var description = $('#description').val();
        var mail = $('#mail').val();

        if (description == '' || mail == '') {
            alert('Les champs doivent êtres remplis');
        } else {
            if (alert( "Please reload to refresh."))
            {}
            else
                window.location.reload();
            $.ajax({
                    type: 'post',
                    url: '/ajax_create_cal/',
                    data: {'user': $("input[name=user]").val(),
                        'description': description,
                        'mail': mail},
                    success: function (data) {
                        var options;
                        options = '<div>'
                        options += 'success'
                        options += '</div>'
                        $("#sub_list").html(options);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.statusText);
                        alert(thrownError);
                    },
                    dataType: 'json'
                }
            );
        }
        return false;
    });

    $( "select#id_cals_list" ).change(function() {
        var select = $("select#id_cals_list option:selected").val();
        alert( "Handler for .change() called." + select);
        $.ajax({
                type:'post',
                url:'/ajax_set_default_cal/',
                    data: {'user': $("input[name=user]").val(),
                        'sub_id': select},
                success:function (data) {
                    var options;
                    if (data.length == 0) {
                        options = '<option value=""></option>';
                    }
                    else {
                        for (var i = 0; i < data.length; i++) {
                            options += '<option value="' + parseInt(data[i].pk) + '">'
                                + data[i].fields['name'] /*+ ": " + data[i].fields['nb_tab_non_vide']*/
                                + '</option>';
                        }
                    }
                    $("#id_cals_list").html(options);
                    $("#id_cals_list option:first").attr('selected', 'selected');
                    $("#id_cals_list").attr('disabled', false);
                },
                error:function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.statusText);
                    alert(thrownError);
                },
                dataType:'json'
                });
        return false;
    });


});
