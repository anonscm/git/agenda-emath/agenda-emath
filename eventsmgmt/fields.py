# -*- coding: utf-8 -*-

# Re-redefine the field size for the django admin interface


from django.forms import * # replace the * by appropriate name
from django.db import models
#from django.core import validators
from django.contrib.admin.widgets import FilteredSelectMultiple

MAIL_SIZE=50
LONG_CHAR_SIZE=90
DEFAULT_CHAR_LENGTH=55

#-----------------------------------------------------------------------------
# Charfield
class amCharField(models.CharField):
    
    def __init__(self,*args,**kwargs): 
        
        self.size = None
        if kwargs.has_key('size'):
            self.size= kwargs['size']
            del kwargs['size']

        kwargs['max_length']=kwargs.get('max_length',DEFAULT_CHAR_LENGTH)
        super(amCharField,self).__init__(*args,**kwargs)

        # define widget size
        if self.size is None:
            self.size=self.max_length

    def formfield(self,**kwargs):
        kwargs.update({"widget": TextInput(attrs={'size':self.size})})
        #self.validators.append(validators.MaxLengthValidator(self.max_length))
        return super(amCharField, self).formfield(**kwargs)

class amLongCharField(models.CharField):
    def formfield(self,**kwargs):
        kwargs.update({"widget": TextInput(attrs={'size':LONG_CHAR_SIZE})})
        #self.validators.append(validators.MaxLengthValidator(self.max_length))
        return super(amLongCharField, self).formfield(**kwargs)

class amSmallTextField(models.TextField):
# Small text area
    def formfield(self,**kwargs):
        kwargs.update({"widget": Textarea(attrs={'cols':84,'rows':2})})
        return super(amSmallTextField, self).formfield(**kwargs)

#-----------------------------------------------------------------------------
# Many to many field, using in admin.py filter_horizontal=(nom_du_champ)
class amManyToManyFieldH(models.ManyToManyField):
# Resize manytomany field using in admin.py filter_horizontal=(nom_du_champ)
    def formfield(self,**kwargs):
        kwargs.update({"widget": FilteredSelectMultiple(self.verbose_name,False,attrs={'style':"height: 10em"})})
        return super(amManyToManyFieldH, self).formfield(**kwargs)

'''
# pb modifying width: resize only the first window, not the second one
class amManyToManyFieldV(models.ManyToManyField):
# Resize manytomany field using in admin.py filter_vertical=(nom_du_champ)
    def formfield(self,**kwargs):
        kwargs.update({"widget": FilteredSelectMultiple(self.verbose_name,True,attrs={'style':"width: 60em"})})
        return super(amManyToManyFieldV, self).formfield(**kwargs)
'''
#-----------------------------------------------------------------------------
# URLField
class amURLField(models.URLField):

    def __init__(self,*args,**kwargs):
        if not args:
            kwargs['verbose_name']=kwargs.get('verbose_name','Site web')
        super(amURLField,self).__init__(*args,**kwargs)

    def formfield(self,**kwargs):
        kwargs.update({"widget": TextInput(attrs={'size':LONG_CHAR_SIZE})})
        #self.validators.append(validators.MaxLengthValidator(self.max_length))
        return super(amURLField, self).formfield(**kwargs)

#-----------------------------------------------------------------------------
# EmailField
class   amEmailField(models.EmailField):

    def __init__(self,*args,**kwargs):
        if not args:
            kwargs['verbose_name']=kwargs.get('verbose_name','Mèl')
        super(amEmailField,self).__init__(*args,**kwargs)

    def formfield(self,**kwargs):
        kwargs.update({"widget": TextInput(attrs={'size':MAIL_SIZE})})
        #self.validators.append(validators.MaxLengthValidator(self.max_length))
        return super(amEmailField, self).formfield(**kwargs)


#-----------------------------------------------------------------------------
# TODO DateField : have only minutes not the seconds
