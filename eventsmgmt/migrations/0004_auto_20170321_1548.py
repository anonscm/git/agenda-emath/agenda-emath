# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eventsmgmt', '0003_auto_20161206_1143'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='sent_to_other',
            field=models.BooleanField(default=False, verbose_name='Sent mail to suppplemental receivers'),
        ),
        # see : http://stackoverflow.com/questions/28429933/django-migrations-using-runpython-to-commit-changes
        # sans ces 2 instructions on a : django.db.utils.OperationalError: cannot ALTER TABLE "" because it has pending trigger event change null to not null
        migrations.RunSQL('SET CONSTRAINTS ALL IMMEDIATE',
                          reverse_sql=migrations.RunSQL.noop),
        migrations.AlterField(
            model_name='subscription',
            name='description',
            field=models.CharField(default='Default', max_length=100, verbose_name='Description'),
            preserve_default=False,
        ),
        migrations.RunSQL(migrations.RunSQL.noop,
                      reverse_sql='SET CONSTRAINTS ALL IMMEDIATE'),
        migrations.AlterField(
            model_name='subscription',
            name='enabled',
            field=models.BooleanField(default=True, verbose_name='Sent me mail'),
        ),
    ]
