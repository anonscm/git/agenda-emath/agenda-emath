# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eventsmgmt', '0002_auto_20161202_1700'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='myevent',
            unique_together=set([('uid', 'calendar')]),
        ),
    ]
