# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
from django.conf import settings
import eventsmgmt.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='MyCalendar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url_to_parse', eventsmgmt.fields.amURLField(unique=True, verbose_name='URL contenant les sessions du s\xe9minaire', blank=True)),
                ('parse', models.CharField(blank=True, max_length=2, null=True, verbose_name='Parser', choices=[(b'AC', 'ACM'), (b'IC', 'iCal'), (b'JS', 'Indico/JSON'), (b'RS', 'RSS'), (b'XM', 'XML')])),
                ('title', eventsmgmt.fields.amLongCharField(max_length=200, verbose_name='Titre du s\xe9minaire')),
                ('url_to_desc', eventsmgmt.fields.amURLField(verbose_name='URL d\xe9crivant le s\xe9minaire', blank=True)),
                ('contact', eventsmgmt.fields.amLongCharField(max_length=200, verbose_name='Contact', blank=True)),
                ('relcalid', models.CharField(max_length=200, blank=True)),
                ('tz', models.CharField(max_length=200, blank=True)),
                ('prodid', models.CharField(max_length=200, blank=True)),
                ('force_utf8', models.BooleanField(default=True, verbose_name='UTF-8')),
                ('enabled', models.BooleanField(default=True, verbose_name='Autoriser le parsing')),
                ('published', models.BooleanField(default=True, verbose_name='Visible')),
                ('origin_if_imported', models.CharField(blank=True, max_length=2, null=True, verbose_name='Origin if imported', choices=[(b'NA', 'Not Applicable'), (b'AC', 'ACM'), (b'OD', 'Officiel des Maths')])),
                ('no_old_events', models.BooleanField(default=False, verbose_name="don't collect past events")),
                ('limit_events_to', models.IntegerField(default=9999, verbose_name='limit number of collected events')),
                ('created_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Creation time')),
                ('edited_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Modified')),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': 'Mon s\xe9minaire',
                'verbose_name_plural': 'Mes s\xe9minaires',
            },
        ),
        migrations.CreateModel(
            name='MyCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'ordering': ('id',),
            },
        ),
        migrations.CreateModel(
            name='MyEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.TextField(verbose_name='UID', editable=False)),
                ('dtstart', models.DateTimeField(verbose_name='Date de d\xe9but')),
                ('dtend', models.DateTimeField(null=True, verbose_name='Date de fin', blank=True)),
                ('dtstamp', models.DateTimeField(null=True, blank=True)),
                ('summary', eventsmgmt.fields.amSmallTextField(verbose_name='Titre')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('rrule', models.CharField(max_length=200, blank=True)),
                ('organizer', models.TextField(blank=True)),
                ('attendee', eventsmgmt.fields.amLongCharField(max_length=300, verbose_name='Orateurs', blank=True)),
                ('location', eventsmgmt.fields.amSmallTextField(verbose_name='Lieu', blank=True)),
                ('url', eventsmgmt.fields.amURLField(max_length=300, verbose_name='Site web', blank=True)),
                ('comment', models.TextField(blank=True)),
                ('status', models.CharField(max_length=200, blank=True)),
                ('published', models.BooleanField(default=True, verbose_name='Visible for everyone')),
                ('category', models.CharField(max_length=200, verbose_name="Type d'\xe9v\xe8nement", blank=True)),
                ('created_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Creation time')),
                ('edited_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Modification time')),
                ('calendar', models.ForeignKey(blank=True, to='eventsmgmt.MyCalendar', null=True)),
                ('category_announce', models.ForeignKey(blank=True, to='eventsmgmt.MyCategory', null=True)),
                ('created_by', models.ForeignKey(related_name='event_created_by', editable=False, to=settings.AUTH_USER_MODEL, verbose_name='User')),
                ('edited_by', models.ForeignKey(related_name='event_modified_by', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='User')),
            ],
            options={
                'ordering': ('-dtstart',),
                'verbose_name': 'Mon annonce',
                'verbose_name_plural': 'Mes annonces',
            },
        ),
        migrations.CreateModel(
            name='MyOrganisation',
            fields=[
                ('name', models.CharField(max_length=1024, verbose_name='Name of entity')),
                ('ref', models.IntegerField(default=0, serialize=False, verbose_name='External Reference', primary_key=True)),
                ('location', models.CharField(max_length=1024, null=True, verbose_name='Location', blank=True)),
                ('wardship', models.CharField(max_length=1024, null=True, verbose_name='Wardship', blank=True)),
                ('alias', models.CharField(max_length=1024, null=True, verbose_name='Alias', blank=True)),
                ('uri', models.URLField(null=True, verbose_name='URI', blank=True)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1024, verbose_name='Name of region')),
            ],
            options={
                'ordering': ('id',),
            },
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('enabled', models.BooleanField(default=True, verbose_name='Abonnement Activ\xe9')),
                ('description', eventsmgmt.fields.amSmallTextField(null=True, verbose_name='Description', blank=True)),
                ('non_human_receivers', models.TextField(null=True, verbose_name='Supplemental receivers', blank=True)),
                ('email', models.EmailField(max_length=75, verbose_name='Subscriber Mail')),
                ('created_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Creation time')),
                ('edited_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Modification time')),
                ('calendar', models.ManyToManyField(related_name='subscription_by_cal', verbose_name='S\xe9minaires', to='eventsmgmt.MyCalendar', blank=True)),
                ('created_by', models.ForeignKey(related_name='sub_created_by', editable=False, to=settings.AUTH_USER_MODEL)),
                ('edited_by', models.ForeignKey(related_name='sub_modified_by', editable=False, to=settings.AUTH_USER_MODEL, null=True)),
                ('event', models.ManyToManyField(related_name='subscription_by_evt', verbose_name='Announces', to='eventsmgmt.MyEvent', blank=True)),
            ],
            options={
                'ordering': ('id',),
                'verbose_name': 'Mon abonnement',
                'verbose_name_plural': 'Mes abonnements',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tel', models.CharField(max_length=100, null=True, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('my_default_subscription', models.ForeignKey(blank=True, to='eventsmgmt.Subscription', null=True)),
                ('org', models.ForeignKey(blank=True, to='eventsmgmt.MyOrganisation', null=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['user__username'],
            },
        ),
        migrations.AddField(
            model_name='myorganisation',
            name='region',
            field=models.ForeignKey(blank=True, to='eventsmgmt.Region', null=True),
        ),
        migrations.AddField(
            model_name='myevent',
            name='org',
            field=models.ManyToManyField(to='eventsmgmt.MyOrganisation', verbose_name='Organisation'),
        ),
        migrations.AddField(
            model_name='mycalendar',
            name='category',
            field=models.ForeignKey(blank=True, to='eventsmgmt.MyCategory', null=True),
        ),
        migrations.AddField(
            model_name='mycalendar',
            name='created_by',
            field=models.ForeignKey(related_name='cal_created_by', editable=False, to=settings.AUTH_USER_MODEL, verbose_name='Author'),
        ),
        migrations.AddField(
            model_name='mycalendar',
            name='edited_by',
            field=models.ForeignKey(related_name='cal_modified_by', editable=False, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='mycalendar',
            name='org',
            field=models.ManyToManyField(to='eventsmgmt.MyOrganisation', verbose_name='Organisations'),
        ),
        migrations.CreateModel(
            name='MySeminar',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('eventsmgmt.mycalendar',),
        ),
    ]
