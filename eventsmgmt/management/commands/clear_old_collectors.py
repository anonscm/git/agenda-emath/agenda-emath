# -*- coding: utf-8 -*-
from datetime import timedelta

from optparse import make_option
from django.core.management.base import BaseCommand

from collector.models import *


class Command(BaseCommand):
    args = ''
    help = 'delete collectors older than 2 months - list only if no -w'
    option_list = BaseCommand.option_list + (
        make_option("-w", dest="write", action='store_true', help='effective deletion'),
        )

    def handle(self, *args, **options):
        startdate = timezone.now() - timedelta(days=7)
        if options['write']:
            collectors = Collector.objects.filter(created_on__lte=startdate)
            print("on supprime %s les collectors" % collectors.count())
            collectors.delete()
        else:
            collectors = Collector.objects.filter(created_on__lte=startdate)
            print("on pourrait supprimer %s collectors" % collectors.count())
