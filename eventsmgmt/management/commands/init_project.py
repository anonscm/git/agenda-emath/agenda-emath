__author__ = 'me'
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.contrib.sites.models import Site

from optparse import make_option
import logging
import os

from agenda.settings import LOG_PATH
from agenda.settings import SITE_ID
from agenda.settings import SITES

class Command(BaseCommand):
    args = ''
    help = 'create dirs needed by project if necessary'
    option_list = BaseCommand.option_list + (
        make_option('--modify-db',
            action="store_true", dest="modify_db",
            help='accept to modify the database. Use with caution!'),
        )

    def handle(self, *args, **options):
        logger = logging.getLogger(__name__)
        logger.debug('Start')

        directory = LOG_PATH
        created_log = False
        if not os.path.exists(directory):
            logger.error("have to create %s" % LOG_PATH)
            os.makedirs(directory)
            created_log = True
        if os.path.exists(directory):
            if created_log:
                logger.info("%s created" % LOG_PATH)
                print("%s created" % LOG_PATH)
            else:
                logger.info("%s exists" % LOG_PATH)
                print("%s exists" % LOG_PATH)

        current_site = Site.objects.get_current()
        uname = os.uname()[1]
        logger.info("current %s" % current_site.domain)
        logger.info("uname %s" % uname)
        if current_site.id != SITE_ID:
            logger.error(" current site %s and SITE_ID %s differs?! FIXME" % (current_site.id, SITE_ID))
        else:
            (current_site.domain, current_site.name) = SITES[0]
            current_site.save()
        for site in Site.objects.all():
            try:
                for st in SITES:
                    if 'ical' in st[1]:
                        site.domain = st[0]
                        site.save()
                    if 'rss' in st[1]:
                        site.domain = st[0]
                        site.save()
            except Site.DoesNotExist, e:
                logger.error("%s %s" % (e, st[0]))
        for site in Site.objects.all():
            logger.info("site %s %s %s" % (site.id, site.domain, site.name))



