# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db import connections
from django.db.utils import ConnectionDoesNotExist
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import send_mail, BadHeaderError, EmailMessage, mail_managers, EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.db.models import Q

from eventsmgmt.models import *
from optparse import make_option

from agenda.settings import ADMINS
from agenda.version import my_version

class Command(BaseCommand):
    args = ''
    help = 'Manage subscriptions'
    option_list = BaseCommand.option_list + (
        make_option("-u", "--user", dest="subscriber",
            help='Send subscriptions messages to only one subscriber'),
        make_option("-d", "--dest", dest="destination",
            help='Test subscriptions messages by sending to othr user'),
        make_option("-n", type="int", dest="arret", default=10,
            help='number of subscribers to treat (default=10)'),
        )

    def handle(self, *args, **options):
        print 'command start'
        print options
        print args

        if options['subscriber']:
            user = options['subscriber']
        else:
            user = None
        if options['destination']:
            destination = options['destination']
        else:
            destination = None
        if options['arret']:
            arret = options['arret']
        else:
            arret = None

        send_subscriptions(user, destination, arret)


def send_subscriptions(subscriber=None, dest=None, arret=None):
    """
    Send subscriptions to all subscribers by default
    if subscriber, it concerns only one subscriber's subscriptions
    if dest, the messages are send to him (test case)
    if arret, stop sending after arret messages
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    logger.debug("subscriber=%s, dest=%s, arret=%s" %(subscriber, dest, arret))
    message = ''
    version = my_version()
    n = 0
    for user in Subscription.objects.values('created_by').distinct():
        if send_subscription(user=User.objects.get(id=user['created_by']), dest=dest):
            n += 1
            logger.debug("mail sent")
        if n > arret:
            logger.debug("%d msg sent STOP NOW" % n)
            break
    logger.debug("%d/%s subscriptions sent successfully" % (n, Subscription.objects.values('created_by').distinct().count()))

def send_subscription(user, dest=None):
    """
    Send next events from now + X days to user
    you can use this function to test the message without sending it to every mail known in subscription
    if subscription has no email, prevent user to add it!
    MAIL_TO_SUB False means debug
    the choice of template mail.html or mail2.html depends on email. If send to a group, for instance
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    message = ''
    version = my_version()

    logger.debug("examining %s sub" % user.sub_created_by.count())
    current_site = Site.objects.get_current()
    for sub in user.sub_created_by.filter(enabled=True).filter(Q(calendar__isnull=False)|Q(event__isnull=False)).distinct():
        url_sub = 'http://%s%s' % (current_site.domain, reverse('detail_subscription', kwargs={'pk': sub.id}))
        logger.debug("url resultat %s{rss,ical}" % url_sub)

        qs = MyEvent.objects.filter(dtstart__gte=timezone.now())\
            .filter(dtstart__lte=timezone.now() + timedelta(days=RECURRENCE))\
            .filter(Q(calendar__in=[cal for cal in sub.calendar.all()])|Q(subscription_by_evt=sub))\
            .order_by('dtstart')
        if sub.email == user.email:
            logger.debug("mail.html: %s sent to %s (%s)" % (sub.description or "Missing description", sub.email, user.email))
            template = 'subscription/mail.html'
        else:
            logger.debug("mail2.html: %s sent to %s (%s)" % (sub.description or "Missing description", sub.email, user.email))
            template = 'subscription/mail2.html'
        html_content = render_to_string(template, {
            'res': qs,
            'user': user,
            'sub': sub,
            'url': 'http://%s' % current_site.domain,
            'site': current_site.name,
            'version': version,
            'MAIL_TO_SUB': MAIL_TO_SUB,
        }) # ...
        text_content = strip_tags(html_content) # this strips the html, so people will have the text as well.

        if MAIL_TO_SUB:
            if dest:
                to = [dest]
            elif sub.email:
                to = [sub.email]
            else:
                logger.info("%s mail missing FIXME" % sub)
                return False
        else:
            to = [a[1] for a in ADMINS] # only for debug
        subject, from_email, to = EMAIL_SUBJECT_PREFIX, DEFAULT_FROM_EMAIL, to
        logger.debug("mail is ready to sent to %s" % to)
        try:
            msg = EmailMultiAlternatives(subject, text_content, from_email, to=to, bcc=[a[1] for a in ADMINS],
                                         headers={
                                             'Reply-To': REPLY_TO,
                                             'X-ACM': X_HEADER
                                         })
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            logger.debug("mail sent")
        except IOError, e:
            logger.error("Unable to send message: %s" % e)
            return False
    return True


