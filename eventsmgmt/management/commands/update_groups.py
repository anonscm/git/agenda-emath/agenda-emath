__author__ = 'bligny'

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError

# Imports
from django.contrib.auth.models import Group, Permission, User
from django.db.models import get_app
from django.db.models import get_models as get_all_models
from django.contrib.contenttypes.models import ContentType
import logging

GROUP_NAME = 'author'
ATTR_NAME = 'auth_groups'
APP_NAME = 'eventsmgmt'

class Command(BaseCommand):
    help = 'Update the group rights. This command should be run in case of DB creation au new table'

    def handle(self, *args, **options):

        # Create or get the groups
        (author, created) = Group.objects.get_or_create(name=GROUP_NAME)

        # for each model in evenetsmgmt application, add the group permissions if
        # auth_group is defined.

        logger = logging.getLogger(__name__)
        logger.debug("-- Update group permissions -- ")

        # get eventsmgmt app
        app=get_app(APP_NAME)

        # get all models (=tables) in the app
        for model in get_all_models(app):
            if hasattr(model, ATTR_NAME):
                # Then the three permission per model : add, remove, change.
                content_type = ContentType.objects.get_for_model(model)
                permissions = Permission.objects.filter(content_type=content_type)

                # add the permissions to the proper group, and possibly remove
                # them from other groups
                if GROUP_NAME in getattr(model,ATTR_NAME):
                    for permission in permissions:
                        author.permissions.add(permission)
                else:
                    for permission in permissions:
                        author.permissions.remove(permission)
                # add the dashbord permissions? Seems to work without

        logger.debug('Permissions groups updated')

        # Update the user table : add author group to every user but the superadmins
        users = User.objects.filter(is_superuser=False).exclude(groups__name=GROUP_NAME)
        for u in users:
	        u.groups.add(author)
        logger.debug(str(users.count())+' users updated')

        # Test : what if a table is removed?
