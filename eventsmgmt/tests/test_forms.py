# -*- coding: utf-8 -*-
__author__ = 'me'

import logging

from django.test import TestCase
from django.utils import timezone

from eventsmgmt.forms import EventForm

from factories import *

class MyTests(TestCase):
    def setUp(self):
        self.user = UserFactory()
        self.org1 = OrgFactory()
        self.category = CategoryFactory()

    def test_forms(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")

        # form_data = {
        #     u'comment': [u''],
        #
        #     u'description': [u'coucou'],
        #     u'url': [u''],
        #     u'summary': [u'ceci est un test'],
        #     u'location': [u''],
        #     u'orgs_list': [u'209'],
        #     u'dtstart': [u'13/04/2016'],
        #     u'dtend': [u'21/04/2016'],
        #     u'organizer': [u'']
        # }
        logger.debug("org is %s" % self.org1)
        orgs = [(self.org1.ref, self.org1.name),]
        orgs_list = sorted(orgs, key=lambda tup: tup[1])
        form_data = {
            'category_announce': self.category.id,
            'description': u'coucou',
            'summary': u'ceci est un test',
            'dtstart_0': timezone.now().date(),
            'dtstart_1': timezone.now().time(),
            'orgs_list': [self.org1.ref,],
        }
        form = EventForm(data=form_data, orgs_list=orgs_list)
        if not form.is_valid():
            logger.debug("form not valid %s" % form.errors)
        self.assertTrue(form.is_valid())
        print("dtstart %s" % form.cleaned_data['dtstart'])
