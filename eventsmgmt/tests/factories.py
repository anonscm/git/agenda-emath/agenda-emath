# -*- coding: utf-8 -*-

import datetime
import factory
from pytz import UTC
from factory.fuzzy import FuzzyDateTime

from django.utils import timezone

class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'auth.User'  # Equivalent to ``model = myapp.models.User``
        django_get_or_create = ('username', )

    username = factory.Sequence(lambda n: 'john%s' % n)
    email = factory.LazyAttribute(lambda o: '%s@example.com' % o.username)
    password = factory.PostGenerationMethodCall('set_password',
                                                'defaultpassword')


class SubscriptionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'eventsmgmt.Subscription'  # Equivalent to ``model = myapp.models.User``
        django_get_or_create = ('email',)

    enabled = True
    description = 'Professionnal'
    created_by = factory.SubFactory(UserFactory)
    email = 'john.doe@example.com'


class OrgFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'eventsmgmt.MyOrganisation'  # Equivalent to ``model = myapp.models.User``
        django_get_or_create = ('ref',)

    name = factory.LazyAttribute(lambda obj: "Laboratoire #%s" % obj.ref)
    @factory.sequence
    def ref(n):
        return '%d' % n



class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'eventsmgmt.MyCategory'  # Equivalent to ``model = myapp.models.User``

    name = factory.Sequence(lambda n: 'Categorie %s' % n)


class CalendarFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'eventsmgmt.MyCalendar'  # Equivalent to ``model = myapp.models.User``
        django_get_or_create = ('url_to_parse', )

    title = factory.Sequence(lambda n: u'Séminaire Y%03d' % n)
    url_to_parse = factory.Sequence(lambda n: "http://%03d.url.to.parse/seminaire" % n)
    created_by = factory.SubFactory(UserFactory)
    category = factory.SubFactory(CategoryFactory)
    enabled = True
    parse = 'IC'

    @factory.post_generation
    def org(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of orgs were passed in, use them
            for org in extracted:
                self.org.add(org)

class EventFactory(factory.django.DjangoModelFactory):
    """
    event is created at now+2d
    """
    class Meta:
        model = 'eventsmgmt.MyEvent'  # Equivalent to ``model = myapp.models.User``

    # @factory.sequence
    # def dtstart(n):
    #     return datetime.datetime(2008, 1, '%d' % n, tzinfo=UTC)

    uid = factory.Sequence(lambda n: "uid-%03d@local.test" % n)
    summary = factory.Sequence(lambda n: "This is %03d summary" % n)
    # dtstart = timezone.now().replace(second=0, microsecond=0) + datetime.timedelta(days=2)
    # dtstart = FuzzyDateTime(datetime.datetime(2008, 1, 1, tzinfo=UTC), datetime.datetime(2009, 1, 1, tzinfo=UTC),
    dtstart = FuzzyDateTime(timezone.now().replace(second=0, microsecond=0),
                            timezone.now().replace(second=0, microsecond=0) + datetime.timedelta(days=365),
                        force_hour=10,
                        force_minute=30,
                        force_second=0, force_microsecond=0)

#    dtend = dtstart.replace(hour=0)
    # organizer = u'Jean Dupont'  # depend of calendar!? FIXME
    status = u'CONFIRMED'
    category = u'Séminaire'  # depend of calendar!? FIXME
    dtstamp = timezone.now()
    url = u'http://local.test/seminaire/evenement/141'
    attendee = factory.Sequence(lambda n: "Prenom%03d Nom%03d" % (n, n))
    location = u'Bâtiment S'
    description = factory.Sequence(lambda n: u"Ceci est description %03d alambiquée" % n)
    created_by = factory.SubFactory(UserFactory)
    calendar = factory.SubFactory(CalendarFactory)

    @factory.post_generation
    def org(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            print "couucou"
            # A list of orgs were passed in, use them
            for org in extracted:
                self.org.add(org)


class AnnounceFactory(factory.django.DjangoModelFactory):
    """
    event is created at now+2d
    """
    class Meta:
        model = 'eventsmgmt.MyEvent'  # Equivalent to ``model = myapp.models.User``

    # @factory.sequence
    # def dtstart(n):
    #     return datetime.datetime(2008, 1, '%d' % n, tzinfo=UTC)

    uid = factory.Sequence(lambda n: "uid-%03d@local.test" % n)
    summary = factory.Sequence(lambda n: "This is %03d summary" % n)
    dtstart = FuzzyDateTime(timezone.now().replace(second=0, microsecond=0),
                            timezone.now().replace(second=0, microsecond=0) + datetime.timedelta(days=365),
                        force_hour=10,
                        force_minute=30,
                        force_second=0, force_microsecond=0)

    status = u'CONFIRMED'
    category = u'Workshop'  # depend of calendar!? FIXME
    dtstamp = timezone.now()
    url = u'http://local.test/workshop/evenement/141'
    attendee = factory.Sequence(lambda n: "Prenom%03d Nom%03d" % (n, n))
    location = u'Bâtiment S'
    description = factory.Sequence(lambda n: u"Ceci est description %03d alambiquée" % n)
    created_by = factory.SubFactory(UserFactory)

    @factory.post_generation
    def org(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for group in extracted:
                self.org.add(group)

