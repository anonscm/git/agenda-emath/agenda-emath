# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from agenda.interface_annuaire import *


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)


class MathdocInterfaceTest(TestCase):
    def test_basic_read(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        localfile = "samples/json/entities_mathdoc.json"
        get_orgs(localfile)
