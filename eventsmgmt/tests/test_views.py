# -*- coding: utf-8 -*-
__author__ = 'me'


import logging

from django.utils import timezone
from factories import *
from django.test import TestCase, RequestFactory
from django.test import Client
from django.contrib.auth.models import User
from django.contrib.auth.models import AnonymousUser
from eventsmgmt.models import MyOrganisation
from eventsmgmt.models import MyEvent
from eventsmgmt.models import MyCalendar
from eventsmgmt.models import Subscription
from eventsmgmt.views import home


def generate_dates(n):
    """
    generates a datetime every hour from now!
    :param n: size of list
    :return: a list of different datetimes
    """
    result = []
    for i in xrange(n):
        result.append(timezone.now().replace(second=0, microsecond=0) + datetime.timedelta(hours=i))
    return result

class SimpleTest(TestCase):
    """
    Verify that web pages are displayed without errors
    """
    def setUp(self):
        self.org1 = OrgFactory()
        self.cal = CalendarFactory.create(org=(self.org1,))
        self.assertEqual(MyCalendar.objects.count(), 1)

    def test_details(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    # def test_index(self):
    #     response = self.client.get('/events/')
    #     self.assertEqual(response.status_code, 200)

    # def test_future_events(self):
    #     """
    #     this url returns nothing
    #     """
    #     self.auteur = UserFactory()
    #     self.subscriber = UserFactory()
    #     self.org1 = OrgFactory()
    #     self.org1.display()
    #     self.cal = CalendarFactory.create(org=(self.org1,))
    #     self.cal.display()
    #     dates = generate_dates(3)
    #     print dates
    #     for date in dates:
    #         self.event = EventFactory.create(calendar=self.cal, dtstart=date, dtend=date + datetime.timedelta(hours=1))
    #     for evt in MyEvent.objects.all().order_by('dtstart'):
    #         evt.display()
    #
    #     response = self.client.get('/future_events/')
    #     self.assertEqual(response.status_code, 200)

    # TODO see collector for errors
    # def test_errors(self):
    #     response = self.client.get('/calendars/errors/')
    #     self.assertEqual(response.status_code, 200)
    #
    # def test_verify(self):
    #     response = self.client.get('/calendars/verify/')
    #     self.assertEqual(response.status_code, 200)


class AuthTest(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.user = UserFactory()
        qs = User.objects.all()
        if qs.count() == 1:
            print qs[0].id
        else:
            print "pas de user en bd!!!"
        self.user = qs[0]

    def test_details(self):
        # Create an instance of a GET request.
        request = self.factory.get('/')

        # Recall that middleware are not supported. You can simulate a
        # logged-in user by setting request.user manually.
        request.user = self.user

        # Or you can simulate an anonymous user by setting request.user to
        # an AnonymousUser instance.
        request.user = AnonymousUser()

        # Test my_view() as if it were deployed at /customer/details
        response = home(request)
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        self.user = User.objects.create_user(username='lauren', email='jacob@toto.fr', password='secret')
        self.user.is_staff = True
        self.user.save()
        # self.client.login(username='lauren', password='secret')
        # print("Login? %s" % self.user.is_authenticated())

        self.client = Client()
        response = self.client.login(username=self.user.username, password='secret')
        print("Login? %s" % self.user.is_authenticated())
        self.assertTrue(response)
        self.assertIn('_auth_user_id', self.client.session)



# class NewAnnounceTest(TestCase):
#     def setUp(self):
#         password = 'secret'
#         self.user = User.objects.create_user(username='lauren', email='jacob@toto.fr', password=password)
#         self.user.is_staff = True
#         self.user.save()
#         self.client = Client()
#         response = self.client.post('/userAdmin/login/', {'name': self.user.username, 'passwd': password},
#                          follow=True)
#         print response.content
#         print("status_code %s" % response.status_code)
#         print("Login? %s" % self.user.is_authenticated())
#
#     def test_create_one(self):
#         print User.objects.all()
#         org1 = OrgFactory()
#         summary = 'Titre 42'
#         response = self.client.post('/userAdmin/eventsmgmt/myevent/add/',
#                                     {
#                                         'summary': summary,
#                                         'category_announce': '5',
#                                         'dtstart_0': '21/04/2015',
#                                         'dtstart_1': '12:00:00',
#                                         'dtend_0': '22/04/2015',
#                                         'dtend_1': '12:00:00',
#                                         'location': 'Toulon',
#                                         'attendee': '',
#                                         'url': '',
#                                         'org': org1.ref,
#                                         'description': '',
#                                         '_save': 'Enregistrer',
#                                     }, follow=True)
#         # for 302 see https://en.wikipedia.org/wiki/HTTP_302
#         self.assertRedirects(response, '/userAdmin/eventsmgmt/myevent/', status_code=302, target_status_code=200,
#                              msg_prefix='')
#         event = MyEvent.objects.filter(calendar=None)
#         print event
# #        self.assertEqual(event.dtstart, evt['dtstart'])
#         self.assertEqual(event.summary, summary)
#
#
# class AnonymousSearchTest(TestCase):

    def test_search_one(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


# class AjaxSubscribeTest(TestCase):
#
#     def test_search_one(self):
#         user = UserFactory()
#         print user.email
#
#         org1 = OrgFactory()
#         cal = CalendarFactory.create(org=(org1, ))
#         SubscriptionFactory.create(created_by=user)
#
#         print "Users: ", User.objects.all()
#         print "Calendars: ", MyCalendar.objects.all()
#         response = self.client.post('/ajax_subscribe/', {'cal_id': cal.id, 'user': user.id}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
#         self.assertEqual(response.status_code, 200)


class SubscriptionTest(TestCase):
    def setUp(self):
        self.auteur = UserFactory()
        self.subscriber = UserFactory()
        self.org1 = OrgFactory()
        self.cal = CalendarFactory.create(org=(self.org1,))
        self.event = EventFactory()

        self.client = Client()

    def test_ical(self):
        """
        simple event
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        message = ''
        sub1 = Subscription.objects.create(created_by=self.subscriber)
        sub1.event.add(self.event)

        response = self.client.get("/subscriptions/%s/ical/" % sub1.id)
        self.assertEqual(response.status_code, 200)

    def test_ical2(self):
        """
        event created by plugin spip/seminaire
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        message = ''

        self.event.attendee = "Albert Enstein"
        self.event.summary = u"%s - %s" % (self.event.attendee, self.event.summary)
        self.event.save()
        sub1 = Subscription.objects.create(created_by=self.subscriber)
        sub1.event.add(self.event)

        response = self.client.get("/subscriptions/%s/ical/" % sub1.id)
        self.assertEqual(response.status_code, 200)


    def test_rss(self):
        """
        RSS Feed
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        message = ''
        sub1 = Subscription.objects.create(created_by=self.subscriber)
        sub1.calendar.add(self.cal)
        response = self.client.get("/subscriptions/%s/rss/" % sub1.id)
        self.assertEqual(response.status_code, 200)

        try:
            result = "/tmp/toto"
            f = open(result, 'w')
            f.write(response.content)
            f.close()
        except IOError:
            logger.error('cannot open %s' % result)
            message += 'cannot open %s' % result


a = {
    "IDP": "PLM Mathrice",
    "UserName": "Gilles.Bares@ceremade.dauphine.fr",
    "EPPN": "bares@math.cnrs.fr",
    "MAIL": "Gilles.Bares@ceremade.dauphine.fr",
    "MAILI": "gilles.bares@ceremade.dauphine.fr",
    "MAILS": ["gilles.bares@ceremade.dauphine.fr", "gilles.bares@math.cnrs.fr"],
    "CN": "Gilles",
    "SN": "Bares",
    "givenName": "Gilles",
    "displayName": "Gilles Bares",
    "FROM": "plm",
    "STATUS": "plm",
    "USER": "bares",
    "mailLAB": "Gilles.Bares@ceremade.dauphine.fr",
    "OU": ["7534"],
    "ADMIN": ["7534"],
    "mailPLM": "Gilles.Bares@math.cnrs.fr",
    "LAB": []
}
b = {"IDP": "Université d'Aix-Marseille",
     "UserName": "gerard.henry@univ-amu.fr",
     "EPPN": "ghenry@univ-amu.fr",
     "MAIL": "gerard.henry@univ-amu.fr",
     "MAILI": "gerard.henry@univ-amu.fr",
     "MAILS": ["gerard.henry@univ-amu.fr", "gerard.henry@math.cnrs.fr"],
     "CN": "HENRY Gerard",
     "SN": "HENRY",
     "displayName": "Gerard Henry",
     "givenName": "Gerard",
     "FROM": "emath",
     "STATUS": "plm",
     "USER": "ghenry",
     "mailLAB": "gerard.henry@univ-amu.fr",
     "OU": ["10012"], "ADMIN": ["6206", "10012"],
     "mailPLM": "gerard.henry@math.cnrs.fr",
     "LAB": ["33", "233", "133"]
}

c = {"IDP":"Université d'Aix-Marseille",
     "UserName":"gerard.henry@univ-amu.fr",
     "EPPN":"ghenry@univ-amu.fr",
     "MAIL":"gerard.henry@univ-amu.fr",
     "MAILI":"gerard.henry@univ-amu.fr",
     "MAILS":["gerard.henry@univ-amu.fr","gerard.henry@math.cnrs.fr"],
     "CN":"HENRY Gerard",
     "SN":"HENRY",
     "displayName":"Gerard Henry",
     "givenName":"Gerard",
     "FROM":"emath",
     "STATUS":"plm",
     "USER":"ghenry",
     "mailLAB":"gerard.henry@univ-amu.fr",
     "OU":["10012"],
     "ADMIN":["6206","10012"],
     "mailPLM":"gerard.henry@math.cnrs.fr",
     "LAB":["33","233","133"]}