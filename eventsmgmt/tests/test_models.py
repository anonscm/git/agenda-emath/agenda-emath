# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from datetime import datetime
import time
import os
import glob

from icalendar import Calendar
from icalendar import Event
from icalendar import vCalAddress
from icalendar import Parameters
from icalendar import vText
import logging
import string
import requests
import sys

from django.test import TestCase
from django.contrib.auth.models import User
from django.db.models import Q
from django.test.client import Client
from django.db.utils import IntegrityError
from django.utils.timezone import utc
from django.utils import timezone

from eventsmgmt.models import MyEvent
from eventsmgmt.models import MyCalendar
from eventsmgmt.models import MyOrganisation
from eventsmgmt.models import Subscription
from eventsmgmt.models import MyCategory

from agenda.settings_local import url_collect_begin, dir4collect
from agenda.settings_local import EMAIL_HOST, EMAIL_PORT
from eventsmgmt.extras import create_ical
from eventsmgmt.models import create_uid

from factories import *

class OrganismTest(TestCase):
    def setUp(self):
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"

    def test_1basic_creation(self):
        """
        """
        print u"cree un premier objet"
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)
        self.assertEqual(MyOrganisation.objects.count(), 1)
        org = MyOrganisation.objects.get(ref=self.ref)
        self.assertEqual((org.name, org.ref), (self.name, self.ref))


    def test_2repeat_creation(self):
        """
        Verify that with same ref and name, no new object is created
        """
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)
        self.assertEqual(MyOrganisation.objects.count(), 1)
        print u"try to create a new object"
        self.org1 = MyOrganisation.objects.get_or_create(name=self.name, ref=self.ref)
        self.assertEqual(MyOrganisation.objects.count(), 1)  # it's the same object
        org2 = MyOrganisation.objects.create(name="New org", ref=self.ref+1)
        self.assertEqual(MyOrganisation.objects.count(), 2)

    def test_3fail_creation(self):
        """

        """
        org1 = OrgFactory()
        org2 = OrgFactory()
        org3 = OrgFactory()

        self.assertEqual(MyOrganisation.objects.count(), 3)


    def tearDown(self):
        list = MyOrganisation.objects.filter(Q(name=self.name) | Q(ref=self.ref))
        if list:
            for org in list:
                # print u'%s "%s" exists? "%d" "%s"\n' % (self.name, self.ref, list.count(), list)
                msg = u'%s "%s" exists? "%d"\n' % (self.name, self.ref, list.count())
                print msg.encode('utf-8')
        else:
            print "no organism yet\n"


class CalendarTest(TestCase):
    """


    """
    def setUp(self):
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)

        self.calendar_title = u'CalendarTest'
        self.auteur = User.objects.create(username='auteur')

    def test_1factory(self):
        """
        simple calendar
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        org3 = OrgFactory()
        self.cal = CalendarFactory.create(org=(self.org1,))
        self.assertEqual(MyCalendar.objects.count(), 1)


    def test_create(self):
        """
        simple calendar
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        cal = MyCalendar.objects.create(title=self.calendar_title, created_by=self.auteur)
        cal.org.add(self.org1)
        cal.save()

        self.assertEqual(MyCalendar.objects.count(), 1)
        self.assertIsNotNone(cal.title)
        self.assertEqual(cal.title, self.calendar_title)
        self.assertIsNotNone(cal.url_to_parse)
        #
        self.assertIsNotNone(cal.created_on)
        self.assertIsNone(cal.edited_by)
        print u'url_to_parse "%s"' % cal.url_to_parse
        self.assertTrue(cal.enabled)


    def test_create_utf8(self):
        """
        simple calendar but title contains special chars
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        for self.calendar_title in [
            # 'S\xc3\xa9minaire de g\xc3\xa9ometrie',
            u"Séminaire de bioloigie",
            "Transition sans accent"
        ]:
            cal = MyCalendar.objects.create(title=self.calendar_title, created_by=self.auteur)
            cal.org.add(self.org1)
            cal.save()

            self.assertEqual(MyCalendar.objects.count(), 1)
            self.assertIsNotNone(cal.title)
            self.assertEqual(cal.title, self.calendar_title)
            self.assertIsNotNone(cal.url_to_parse)
            #
            self.assertIsNotNone(cal.created_on)
            self.assertIsNone(cal.edited_by)
            print u'url_to_parse "%s"' % cal.url_to_parse
            self.assertTrue(cal.enabled)
            cal.delete()


    def test_update(self):
        """
        verify the name of the calendar
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        cal = MyCalendar.objects.create(title=self.calendar_title, created_by=self.auteur)
        cal.org.add(self.org1)
        cal.save()
        old_title = cal.title
        # the title has changed when collect
        dict= {
            'title': u"New Calendar",
        }
        cal.update(dict, modify_db=True)
        self.assertEqual(MyCalendar.objects.count(), 1)
        for cal in MyCalendar.objects.all():
            self.assertNotEqual(cal.title, dict['title'])
            self.assertEqual(cal.title, old_title)

        cal.edited_on = cal.edited_on.replace(tzinfo=None)
        cal.created_on = cal.created_on.replace(tzinfo=None)
        # verify that edited_on is incremented
        print cal.created_on
        print cal.edited_on
        self.assertGreaterEqual(cal.edited_on, cal.created_on)
        self.assertIsNone(cal.edited_by)

    def test_update2(self):
        """
        verify dates creation modification
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        created = datetime.datetime(2013, 6, 4, 11, 30)
        edited = datetime.datetime(2013, 6, 6, 11, 30)
        cal = MyCalendar.objects.create(title=self.calendar_title,
                                        created_by=self.auteur,
                                        created_on=created)
        cal.org.add(self.org1)
        cal.save()

        cal.parse = 'IC'
        cal.edited_on = edited
        cal.save()
        self.assertGreaterEqual(cal.edited_on, cal.created_on)
        self.assertIsNone(cal.edited_by)


class EventTest(TestCase):
    def setUp(self):
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"
        self.calendar_title = u'CalendarTest'
        self.auteur = User.objects.create(username='auteur')
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)
        url = "http://www.changeme.fr"
        title = "mon calendrier"
        self.user = User.objects.get(username='auteur')

        self.cal = MyCalendar(title=title, url_to_parse=url, created_by=self.user)
        self.cal.parse = 'IC'
        self.cal.enabled = True
        self.cal.save()
        # on l'ajoute au labo
        self.cal.org.add(self.org1)
        self.cal.save()

    def test_basic_creation(self):
        """
        Add a simple event
        """
        print "cree un evenement"
        dtstart = timezone.now().replace(second=0, microsecond=0) + datetime.timedelta(days=1)
        dtend = dtstart + datetime.timedelta(hours=2)
        evt = {
            'status': 'CONFIRMED',
            'category': '02. S\xc3\xa9minaires 2012-2013',
            'dtstamp': datetime.datetime(2013, 6, 4, 11, 30),
            'uid': '20130604T113000-a426-e141@lmv.math.cnrs.fr',
            'url': 'http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaires-de-probabilites/article/seminaires-2012-2013?id_evenement=141',
            'attendee': 'C\xc3\xa9cile Mailler',
            'summary': 'SUMMARY:Equations de point fixe pour les grandes urnes de Plya',
            'location': 'b\xc3\xa2timent Fermat\\, en salle 2203 ',
            'dtstart': dtstart,
            'dtend': dtend,
            'organizer': 'Nad\xc3\xa8ge Arnaud',
            'description': "Je m'int\xc3\xa9resse dans cet expos\xc3\xa9 aux grandes urnes de P$\\\\'o$lya. L'\xc3\xa9tude du comportement asymptotique d'une telle urne fait intervenir une variable al\xc3\xa9atoire not\xc3\xa9e W. La structure arborescente de l'urne nous permet de voir W comme solution d'une \xc3\xa9quation de point fixe\\, et d'\xc3\xa9tudier par exemple ainsi la suite de ses moments ou l'existence d'une densit\xc3\xa9. Ce travail peut \xc3\xaatre r\xc3\xa9alis\xc3\xa9 aussi bien sur l'urne discr\xc3\xa8te elle-m\xc3\xaame que sur son plongement en temps continu. Bien que les deux variables W (associ\xc3\xa9es au temps discret et au temps continu) soient diff\xc3\xa9rentes\\, elles peuvent \xc3\xaatre reli\xc3\xa9es par diff\xc3\xa9rentes connexions qui permettent souvent de transporter les r\xc3\xa9sultats de l'une \xc3\xa0 l'autre. Ce travail est une collaboration avec Brigitte Chauvin et Nicolas Pouyanne."}

        self.event = MyEvent.objects.create(calendar=self.cal,
#                                            uid=evt['uid'],
                                            dtstart=evt['dtstart'],
                                            summary=evt['summary'],
                                            created_by=self.user,
        )
        self.assertEqual(MyEvent.objects.count(), 1)

        dtstart = timezone.now().replace(second=0, microsecond=0) + datetime.timedelta(days=2)
        dtend = dtstart + datetime.timedelta(hours=2)
        uid = '20130604T113000-a426-e141@lmv.math.cnrs.fr'
        evt = {
            'status': 'CONFIRMED',
            'category': '02. S\xc3\xa9minaires 2012-2013',
            'dtstamp': datetime.datetime(2013, 6, 4, 11, 30),
            'uid': uid,
            'url': 'http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaires-de-probabilites/article/seminaires-2012-2013?id_evenement=141',
            'attendee': 'C\xc3\xa9cile Mailler',
            'summary': 'SUMMARY:Equations de point fixe pour les grandes urnes de Plya',
            'location': 'b\xc3\xa2timent Fermat\\, en salle 2203 ',
            'dtstart': dtstart,
            'dtend': dtend,
            'organizer': 'Nad\xc3\xa8ge Arnaud',
            'description': "Je m'int\xc3\xa9resse dans cet expos\xc3\xa9 aux grandes urnes de P$\\\\'o$lya. L'\xc3\xa9tude du comportement asymptotique d'une telle urne fait intervenir une variable al\xc3\xa9atoire not\xc3\xa9e W. La structure arborescente de l'urne nous permet de voir W comme solution d'une \xc3\xa9quation de point fixe\\, et d'\xc3\xa9tudier par exemple ainsi la suite de ses moments ou l'existence d'une densit\xc3\xa9. Ce travail peut \xc3\xaatre r\xc3\xa9alis\xc3\xa9 aussi bien sur l'urne discr\xc3\xa8te elle-m\xc3\xaame que sur son plongement en temps continu. Bien que les deux variables W (associ\xc3\xa9es au temps discret et au temps continu) soient diff\xc3\xa9rentes\\, elles peuvent \xc3\xaatre reli\xc3\xa9es par diff\xc3\xa9rentes connexions qui permettent souvent de transporter les r\xc3\xa9sultats de l'une \xc3\xa0 l'autre. Ce travail est une collaboration avec Brigitte Chauvin et Nicolas Pouyanne."}

        self.event = MyEvent.objects.create(calendar=self.cal,
                                            uid=evt['uid'],
                                            dtstart=evt['dtstart'],
                                            summary=evt['summary'],
                                            created_by=self.user,
        )
        try:
            evt_res = MyEvent.objects.get(uid=evt['uid'])
        except MyEvent.DoesNotExist, e:
            self.assertEqual(1, 0)
        self.assertEqual(evt_res.summary, evt['summary'])

    #        self.assertEqual((self.org1.name, self.org1.ref), (self.name, self.ref))

    def test_basic_creation2(self):
        """
        Cree evenement sans date de fin
        """
        print "cree un evenement"
        dtstart = timezone.now().replace(second=0, microsecond=0) + datetime.timedelta(days=2)
        dtend = dtstart + datetime.timedelta(hours=2)
        evt = {
            'uid': '20130604T113000-a426-e141@lmv.math.cnrs.fr',
            'summary': 'SUMMARY:Equations de point fixe pour les grandes urnes de Plya',
            'dtstart': dtstart,
            'organizer': 'Nad\xc3\xa8ge Arnaud',
            'status': 'CONFIRMED',
            'category': '02. S\xc3\xa9minaires 2012-2013',
            'dtstamp': datetime.datetime(2013, 6, 4, 11, 30),
            'url': 'http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaires-de-probabilites/article/seminaires-2012-2013?id_evenement=141',
            'attendee': 'C\xc3\xa9cile Mailler',
            'location': 'b\xc3\xa2timent Fermat\\, en salle 2203 ',
            'dtend': dtend,
            'description': "Je m'int\xc3\xa9resse dans cet expos\xc3\xa9 aux grandes urnes de P$\\\\'o$lya. L'\xc3\xa9tude du comportement asymptotique d'une telle urne fait intervenir une variable al\xc3\xa9atoire not\xc3\xa9e W. La structure arborescente de l'urne nous permet de voir W comme solution d'une \xc3\xa9quation de point fixe\\, et d'\xc3\xa9tudier par exemple ainsi la suite de ses moments ou l'existence d'une densit\xc3\xa9. Ce travail peut \xc3\xaatre r\xc3\xa9alis\xc3\xa9 aussi bien sur l'urne discr\xc3\xa8te elle-m\xc3\xaame que sur son plongement en temps continu. Bien que les deux variables W (associ\xc3\xa9es au temps discret et au temps continu) soient diff\xc3\xa9rentes\\, elles peuvent \xc3\xaatre reli\xc3\xa9es par diff\xc3\xa9rentes connexions qui permettent souvent de transporter les r\xc3\xa9sultats de l'une \xc3\xa0 l'autre. Ce travail est une collaboration avec Brigitte Chauvin et Nicolas Pouyanne."}

        self.event = MyEvent(calendar=self.cal)
        self.event.uid = evt['uid']
        self.event.dtstart = evt['dtstart']
        self.event.summary = evt['summary']
        self.event.created_by = self.user
        try:
            self.event.save()
        except IntegrityError, e:
            print u"%s" % e
        self.event.status = evt['status']
        self.event.save()
        self.event.category = evt['category']
        self.event.save()
        self.event.dtstamp = evt['dtstamp']
        self.event.save()
        self.event.url = evt['url']
        self.event.save()
        self.event.attendee = evt['attendee']
        self.event.save()
        self.event.location = evt['location']
        self.event.save()
        self.event.description = evt['description']
        self.event.save()

    #        self.assertEqual((self.org1.name, self.org1.ref), (self.name, self.ref))

    def test_basic_creation3(self):
        """
        fill a calendar with many events
        """
        print "cree un evenement"
        dtstart = timezone.now().replace(second=0, microsecond=0) + datetime.timedelta(days=2)
        dtend = dtstart + datetime.timedelta(hours=2)
        evt = {
            'uid': u'20130604T113000-a426-e141@lmv.math.cnrs.fr',
            'summary': u'SUMMARY:Equations de point fixe pour les grandes urnes de Plya',
            'dtstart': dtstart,
            'organizer': 'Nad\xc3\xa8ge Arnaud',
            'status': u'CONFIRMED',
            'category': u'02. S\xc3\xa9minaires 2012-2013',
            'dtstamp': datetime.datetime(2013, 6, 4, 11, 30),
            'url': u'http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaires-de-probabilites/article/seminaires-2012-2013?id_evenement=141',
            'attendee': u'C\xc3\xa9cile Mailler',
            'location': u'b\xc3\xa2timent Fermat\\, en salle 2203 ',
            'dtend': dtend,
            'description': u"Je m'int\xc3\xa9resse dans cet expos\xc3\xa9 aux grandes urnes de P$\\\\'o$lya. L'\xc3\xa9tude du comportement asymptotique d'une telle urne fait intervenir une variable al\xc3\xa9atoire not\xc3\xa9e W. La structure arborescente de l'urne nous permet de voir W comme solution d'une \xc3\xa9quation de point fixe\\, et d'\xc3\xa9tudier par exemple ainsi la suite de ses moments ou l'existence d'une densit\xc3\xa9. Ce travail peut \xc3\xaatre r\xc3\xa9alis\xc3\xa9 aussi bien sur l'urne discr\xc3\xa8te elle-m\xc3\xaame que sur son plongement en temps continu. Bien que les deux variables W (associ\xc3\xa9es au temps discret et au temps continu) soient diff\xc3\xa9rentes\\, elles peuvent \xc3\xaatre reli\xc3\xa9es par diff\xc3\xa9rentes connexions qui permettent souvent de transporter les r\xc3\xa9sultats de l'une \xc3\xa0 l'autre. Ce travail est une collaboration avec Brigitte Chauvin et Nicolas Pouyanne.",
            'created_by': self.user,
            }

        MyEvent.bulk(self.cal, [evt, ], modify_db=True)
        self.assertEqual(self.cal.myevent_set.count(), 1)


    def test_encoding(self):
        """
        see https://github.com/collective/icalendar/blob/master/src/icalendar/tests/test_property_params.py
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")

        # cal_address = vCalAddress("Джон Доу")
        # #cal_address.params["CN"] = "Джон Доу"
        # vevent = Event()
        # vevent['ATTENDEE'] = cal_address
        # self.assertEqual(vevent.to_ical(),
        #     'BEGIN:VEVENT\r\nATTENDEE:Джон Доу\r\nEND:VEVENT\r\n'
        # )
        # self.assertEqual(vevent['ATTENDEE'], 'Джон Доу')
        cal_address = vCalAddress('mailto:john.doe@example.org')
        cal_address.params["CN"] = "Джон Доу"
        vevent = Event()
        vevent['ORGANIZER'] = cal_address
        self.assertEqual(
            vevent.to_ical().decode('utf-8'),
            u'BEGIN:VEVENT\r\n'
            u'ORGANIZER;CN="Джон Доу":mailto:john.doe@example.org\r\n'
            u'END:VEVENT\r\n'
        )
        self.assertEqual(vevent['ORGANIZER'].params['CN'],
                         'Джон Доу')


class AnnounceTest(TestCase):
    def setUp(self):
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"
        self.auteur = User.objects.create(username='auteur', email="toto@titi.fr",
                                          first_name='Jean', last_name='Braye')
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)
        self.user = User.objects.get(username='auteur')
        self. cat = MyCategory.objects.create(name=u'Séminaire')

    def test_basic_creation(self):
        """
        Create a simple organism
        """
        print "New Announce"

        dtstart = timezone.now().replace(second=0, microsecond=0)
        evt = {
            'dtstart': dtstart,
            'summary': u'SUMMARY:Announce Number 1 from Me',
            'description': u"La structure arborescente de l'urne nous permet de voir W ",
            'location': u'en salle 2203 ',
            'organizer': u'Nadege Arnaud',
            'category_announce': self.cat,
            'url': 'http://it.describes.the.announce/',
            }

        self.event = MyEvent.objects.create(dtstart=evt['dtstart'],
                                            summary=evt['summary'],
                                            category_announce=evt['category_announce'],
                                            created_by=self.auteur,
                                            )
        self.event.org.add(self.org1)
        self.event.save()
        self.event.uid = create_uid(self.event)
        self.event.save()

        for event in MyEvent.objects.filter(calendar=None):
            print event
            self.assertEqual(event.dtstart, evt['dtstart'])
            self.assertEqual(event.summary,evt['summary'])
        #print u"%s" % result
    #        self.assertEqual((self.org1.name, self.org1.ref), (self.name, self.ref))

    def test_encodings(self):
        """
        Create a simple announce
        """
        print "cree un evenement"
        evt = {
            'dtstart': timezone.now().date(),
            'summary': u'SUMMARY:Announce Number 1 from Me',
            'description': u"Je m'int\xc3\xa9resse dans cet expos\xc3\xa9 aux grandes \
            urnes de P$\\\\'o$lya. L'\xc3\xa9tude du comportement asymptotique d'une telle urne fait \
            intervenir une variable al\xc3\xa9atoire not\xc3\xa9e W. La structure arborescente de l'urne nous permet de voir W comme solution d'une \xc3\xa9quation de point fixe\\, et d'\xc3\xa9tudier par exemple ainsi la suite de ses moments ou l'existence d'une densit\xc3\xa9. Ce travail peut \xc3\xaatre r\xc3\xa9alis\xc3\xa9 aussi bien sur l'urne discr\xc3\xa8te elle-m\xc3\xaame que sur son plongement en temps continu. Bien que les deux variables W (associ\xc3\xa9es au temps discret et au temps continu) soient diff\xc3\xa9rentes\\, elles peuvent \xc3\xaatre reli\xc3\xa9es par diff\xc3\xa9rentes connexions qui permettent souvent de transporter les r\xc3\xa9sultats de l'une \xc3\xa0 l'autre. Ce travail est une collaboration avec Brigitte Chauvin et Nicolas Pouyanne.",
            'location': u'b\xc3\xa2timent Fermat\\, en salle 2203 ',
            'dtend': timezone.now().date() + datetime.timedelta(days=1),
            'organizer': u'Nad\xc3\xa8ge Arnaud',
            'category_announce': self.cat,
            'url': 'http://it.describes.the.announce/',
            }


class SubscriptionTest(TestCase):
    def setUp(self):
        self.user = UserFactory()

    def test_1basic_creation(self):
        """
        """
        sub = Subscription.objects.create(created_by=self.user,
                                          email="i_am_subscriber@domain.org")
        self.assertEqual(Subscription.objects.count(), 1)
        qs = Subscription.objects.all()
        sub1 = qs[0]
        self.assertEqual(sub1.enabled, True)
        print("sub email: %s" % sub1.email)
        self.assertIsNotNone(sub1.email)
        self.assertNotEqual(sub1.email, '')
        self.assertNotEqual(sub1.email, u'')

    def test_sub_to_cal(self):
        """
        """
        self.calendar = CalendarFactory()
        sub = Subscription.objects.create(created_by=self.user,
                                          email="i_am_subscriber@domain.org")
        sub.calendar.add(self.calendar)
        sub.save()
        qs = Subscription.objects.all()
        sub1 = qs[0]
        for cal in sub.calendar.all():
            self.assertEqual(cal, self.calendar)

    def test_sub_to_event(self):
        """
        """
        event = EventFactory()
        sub = Subscription.objects.create(created_by=self.user,
                                          email="i_am_subscriber@domain.org")
        sub.event.add(event)
        sub.save()
        qs = Subscription.objects.all()
        sub1 = qs[0]
        for evt in sub.event.all():
            self.assertEqual(evt, event)
