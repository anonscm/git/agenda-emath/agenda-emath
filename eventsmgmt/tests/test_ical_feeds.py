# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from factories import *

from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from django.test import Client
from django.core.urlresolvers import reverse

from eventsmgmt.feeds import LatestEntriesFeed
from eventsmgmt.feeds import LatestAnnouncesFeed
from eventsmgmt.feeds import MyCalendarFeed
from eventsmgmt.feeds import SubscribeFeed
from eventsmgmt.feeds import SubscribeIcal
from eventsmgmt.feeds import MyEventFeed, MyEventICS, MyCalendarICS

class CalendarTest(TestCase):
    """
    we test LatestEntriesFeed() that gives RSS
    """
    def setUp(self):
        self.auteur = UserFactory()
        self.subscriber = UserFactory()
        self.org1 = OrgFactory()
        self.cal = CalendarFactory.create(org=(self.org1,))
        self.event = EventFactory()
        self.client = Client()

    def test_details(self):
        # Create an instance of a GET request.
        response = self.client.get(reverse('calics', kwargs={'cal_id': self.cal.id}))
        self.assertEqual(response.status_code, 200)
        print response.content


class EventTest(TestCase):
    """
    we test LatestEntriesFeed() that gives RSS
    """
    def setUp(self):
        self.auteur = UserFactory()
        self.subscriber = UserFactory()
        self.org1 = OrgFactory()
        self.cal = CalendarFactory.create(org=(self.org1,))
        self.event = EventFactory()
        self.client = Client()

    def test_details(self):
        # Create an instance of a GET request.
        response = self.client.get(reverse('eventics', kwargs={'evt_id': self.event.id}))
        self.assertEqual(response.status_code, 200)
        print response.content


class AnnounceTest(TestCase):
    """
    we test LatestEntriesFeed() that gives RSS
    """
class AnnounceTest(TestCase):
    def setUp(self):
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"
        self.auteur = User.objects.create(username='auteur')
        self.org1 = OrgFactory()
        self.user = User.objects.get(username='auteur')
        self.event = AnnounceFactory.create(org=(self.org1,))

    def test_details(self):
        # Create an instance of a GET request.
        response = self.client.get(reverse('eventics', kwargs={'evt_id': self.event.id}))
        self.assertEqual(response.status_code, 200)
        print response.content


class SubscriptionTest(TestCase):
    """
    """
    def setUp(self):
        self.auteur = UserFactory()
        self.subscriber = UserFactory()
        self.subscription = SubscriptionFactory()
        self.event = EventFactory()
        self.subscription.calendar.add(self.event.calendar)
        self.event = EventFactory()
        self.subscription.calendar.add(self.event.calendar)
        self.client = Client()
        self.user = User.objects.create_user(username='lauren', email='jacob@toto.fr', password='secret')
        self.user.is_staff = True
        self.user.is_superuser = True
        self.user.save()
        self.client.login(username='lauren', password='secret')

    def test_details(self):
        # Create an instance of a GET request.
        response = self.client.get(reverse('ical_by_user', kwargs={'sub_id': self.subscription.id}))
        self.assertEqual(response.status_code, 200)
        print response.content


