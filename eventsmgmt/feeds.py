# -*- coding: utf-8 -*-
__author__ = 'me'

from django.contrib.syndication.views import Feed
from django.contrib.syndication.views import FeedDoesNotExist
from django.shortcuts import get_object_or_404
from django.db.models import Q

from django_ical.views import ICalFeed
from eventsmgmt.models import *
from agenda.settings_eventsmgmt import *
from agenda.settings import TIME_ZONE


class LatestEntriesFeed(Feed):
    title = u"Agenda des mathématiques"
    link = "/sitenews/"
    description = u"Next (%s) events related to mathematics." % GLOBAL_NEXT_EVENTS_FEED

    def items(self):
        return MyEvent.future_objects.all().order_by('dtstart')[:GLOBAL_NEXT_EVENTS_FEED]

    def item_pubdate(self, item):
        return item.dtstart

    def item_title(self, item):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        if item.attendee and item.attendee not in item.summary:
            return u'%s: %s' % (item.attendee or '', item.summary or 'No title')
        else:
            return u'%s' % item.summary or 'No title'

    def item_description(self, item):
        line = u''
        if item.calendar:
            line += u'%s ' % item.calendar
            if item.calendar.org.all():
                line += "("
            for org in item.calendar.org.all():
                line += u'%s ' % org
            if item.calendar.org.all():
                line += ")"
        line += u'%s ' % item.description or 'No description'
        return u"%s" % line

    def item_categories(self, item):
        return u"%s" % item.category or 'No category'

    def item_link(self, item):
        return item.get_absolute_url() or 'No url'


class LatestAnnouncesFeed(Feed):
    title = u"Agenda des mathématiques"
    link = "/sitenews/"
    description = u"Next (%s) announces related to mathematics." % GLOBAL_NEXT_EVENTS_FEED

    def items(self):
        return MyEvent.all_announces.all().order_by('dtstart')[:GLOBAL_NEXT_EVENTS_FEED]

    def item_pubdate(self, item):
        return item.dtstart

    def item_title(self, item):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        if item.attendee and item.attendee not in item.summary:
            return u'%s: %s' % (item.attendee or '', item.summary or 'No title')
        else:
            return u'%s' % item.summary or 'No title'

    def item_description(self, item):
        line = u''
        if item.calendar.org.all():
            line += "("
        for org in item.calendar.org.all():
            line += u'%s ' % org
        if item.calendar.org.all():
            line += ")"
        line += u'%s ' % item.description or 'No description'
        return u"%s" % line

    def item_categories(self, item):
        return u"%s" % item.category or 'No category'

    def item_link(self, item):
        return item.get_absolute_url() or 'No url'


class SubscribeFeed(Feed):
    description_template = 'feeds/subscription_description.html'

    def get_object(self, request, sub_id):
        return get_object_or_404(Subscription, pk=sub_id)

    def title(self, obj):
        return u'Agenda des mathématiques: "%s"' % obj.description

    def link(self, obj):
        return obj.get_absolute_url()

    def description(self, obj):
        return "Limited to next %d events" % NEXT_EVENTS_FEED

    def items(self, obj):
        if obj.calendar:
            queryset = MyEvent.objects.filter(Q(calendar__in=obj.calendar.all()) | Q(subscription_by_evt=obj))
        else:
            queryset = MyEvent.objects.filter(subscription_by_evt=obj)
        return queryset.filter(dtstart__gte=timezone.now()).order_by('dtstart')[:NEXT_EVENTS_FEED]

    def item_title(self, item):
        if item.attendee and item.attendee not in item.summary:
            return u'%s: %s' % (item.attendee or '', item.summary or 'No title')
        else:
            return u'%s' % item.summary or 'No title'

    def item_pubdate(self, item):
        return item.dtstart

    def item_description(self, item):
        if item.calendar:
            ret = u"%s %s" % (item.description, item.calendar)
        if item.org.all():
            ret = u"%s " % item.description
            for org in item.org.all():
                ret += u"%s " % org
        return ret

# def item_link(self, item):
#        return "/event/" + str(item.id) + "/"

class MyCalendarFeed(Feed):
    description_template = 'feeds/mycalendar_description.html'

    def get_object(self, request, cal_id):
        return get_object_or_404(MyCalendar, pk=cal_id)

    def title(self, obj):
        org = u''
        for o in obj.org.all():
            org += u'%s ' % o
        return "%s of %s" % (obj.title, org)

    def link(self, obj):
        return obj.get_absolute_url()

    def description(self, obj):
        description = u''
        if obj.relcalid:
            description += u'%s ' % obj.relcalid
        if obj.prodid:
            description += u'%s ' % obj.prodid
        else:
            description += u'No description'
        return " %s" % description

    def items(self, obj):
        return obj.myevent_set.filter(dtstart__gte=timezone.now())[:NEXT_EVENTS_FEED]

    def item_pubdate(self, item):
        return item.dtstart

    def item_title(self, item):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        if item.attendee and item.attendee not in item.summary:
            return u'%s: %s' % (item.attendee or '', item.summary or 'No title')
        else:
            return u'%s' % item.summary or 'No title'

    def item_description(self, item):
        return item.description or 'No abstract'

        # def item_categories(self, item):
        #     return item.category or 'No category'
        #
        # def item_link(self, item):
        #     return item.url or item.calendar.url_to_desc or item.calendar.url_to_parse


class MyEventFeed(ICalFeed):
    """
    all future events (limited by NEXT_EVENTS_FEED)
    """
    product_id = '-//Agenda des Maths//Django Alpha//EN'
    timezone = TIME_ZONE
    title = "Agenda des Maths (Alpha)"
    description = "Agenda des Maths (Alpha)"
    method = "PUBLISH"  # see rfc2446bis

    def items(self):
        return MyEvent.future_objects.all().order_by('dtstart')[:NEXT_EVENTS_FEED]

    def item_title(self, item):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        if item.attendee and item.attendee not in item.summary:
            return u'%s: %s' % (item.attendee or '', item.summary or 'No title')
        else:
            return u'%s' % item.summary or 'No title'

    def item_description(self, item):
        return item.description or 'No abstract'

    def item_end_datetime(self, item):
        return item.dtend

    def item_start_datetime(self, item):
        return item.dtstart

    def item_guid(self, item):
        return item.uid

    def item_location(self, item):
        return item.location


class MyCalendarICS(ICalFeed):
    """
    all future events for a calendar
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    product_id = '-//Agenda des Maths//Django Alpha//EN'
    timezone = TIME_ZONE
    #description = "Agenda des Maths (Alpha)"
    method = "PUBLISH"  # see rfc2446bis
    items = []

    def get_object(self, request, cal_id):
        return get_object_or_404(MyCalendar, pk=cal_id)

    def description(self, obj):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        description = u''
        if obj.relcalid:
            logger.debug(u"%s has relcalid %s" % (obj, obj.relcalid))
            description += u'%s ' % obj.relcalid
        elif obj.prodid:
            logger.debug(u"%s has prodid %s" % (obj, obj.prodid))
            description += u'%s ' % obj.prodid
        elif obj.url_to_desc:
            logger.debug(u"%s has url_to_desc %s" % (obj, obj.url_to_desc))
            description += u'%s ' % obj.url_to_desc
        else:
            logger.debug(u"%s has no relcalid nor prodid" % obj)
            description += u'No description'
        return " %s" % description

    def title(self, obj):
        return u" %s" % obj.title

    def items(self, obj):
        return obj.myevent_set.filter(dtstart__gte=timezone.now() - timedelta(days=ICAL_DAYS_BEFORE)).order_by(
            'dtstart')

    def item_title(self, item):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        if item.attendee and item.attendee not in item.summary:
            return u'%s: %s' % (item.attendee or '', item.summary or 'No title')
        else:
            return u'%s' % item.summary or 'No title'

    def item_description(self, item):
        return item.description or 'No abstract'

    def item_end_datetime(self, item):
        return item.dtend

    def item_start_datetime(self, item):
        return item.dtstart

    def item_guid(self, item):
        return item.uid

    def item_location(self, item):
        return item.location

    def item_link(self, item):
        return item.url


class MyEventICS(ICalFeed):
    """
    on event in a calendar
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    product_id = '-//Agenda des Maths//Django Alpha//EN'
    timezone = TIME_ZONE
    #description = "Agenda des Maths (Alpha)"
    method = "PUBLISH"  # see rfc2446bis
    items = []

    def get_object(self, request, evt_id):
        return get_object_or_404(MyEvent, pk=evt_id)

    def description(self, evt):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        description = u''
        if evt.calendar:
            # event
            obj = evt.calendar
            if obj.relcalid:
                logger.debug(u"%s has relcalid %s" % (obj, obj.relcalid))
                description += u'%s ' % obj.relcalid
            elif obj.prodid:
                logger.debug(u"%s has prodid %s" % (obj, obj.prodid))
                description += u'%s ' % obj.prodid
            elif obj.url_to_desc:
                logger.debug(u"%s has url_to_desc %s" % (obj, obj.url_to_desc))
                description += u'%s ' % obj.url_to_desc
            else:
                logger.debug(u"%s has no relcalid nor prodid" % obj)
                description += u'No description'
        elif evt.org.all():
            # announce
            for org in evt.org.all():
                description += u"%s " % org
        else:
            logger.error(u"%s is neither announce or event" % evt)
            description = "ERROR"
        return description

    def title(self, obj):
        ret = ""
        if obj.calendar:
            ret = obj.calendar.title
            return ret
        if obj.org.all():
            for org in obj.org.all():
                ret += "%s " % org
        return ret


    def items(self, obj):
        """
        we need one event by a queryset
        """
        if obj.calendar:
            return obj.calendar.myevent_set.filter(id=obj.id)
        else:
            return MyEvent.objects.filter(id=obj.id)

    def item_title(self, item):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        if item.attendee and item.attendee not in item.summary:
            return u'%s: %s' % (item.attendee or '', item.summary or 'No title')
        else:
            return u'%s' % item.summary or 'No title'

    def item_description(self, item):
        return item.description or 'No abstract'

    def item_end_datetime(self, item):
        return item.dtend

    def item_start_datetime(self, item):
        return item.dtstart

    def item_guid(self, item):
        return item.uid

    def item_location(self, item):
        return item.location

    def item_link(self, item):
        return item.url


class SubscribeIcal(ICalFeed):
    """
    all future events for a subscriber
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    product_id = '-//Agenda des Maths//Django Alpha//EN'
    timezone = 'Europe/Paris'
    #description = "Agenda des Maths (Alpha)"
    method = "PUBLISH"  # see rfc2446bis
    items = []

    def get_object(self, request, sub_id):
        return get_object_or_404(Subscription, pk=sub_id)

    def description(self, obj):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        chaine = u''
        for cal in obj.calendar.all():
            chaine += u'%s, ' % cal
        return " %s" % u'%s' % chaine

    def title(self, obj):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        title = "Agenda des Maths (Alpha)"
        return " %s" % title

    def items(self, obj):
        if obj.calendar:
            queryset = MyEvent.objects.filter(Q(calendar__in=obj.calendar.all()) | Q(subscription_by_evt=obj))
        else:
            queryset = MyEvent.objects.filter(subscription_by_evt=obj)
        return queryset.filter(dtstart__gte=timezone.now() - timedelta(days=ICAL_DAYS_BEFORE)).order_by('dtstart')

    def item_title(self, item):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        if item.attendee and item.attendee not in item.summary:
            return u'%s: %s' % (item.attendee or '', item.summary or 'No title')
        else:
            return u'%s' % item.summary or 'No title'

    def item_description(self, item):
        if item.description:
            return item.description

    def item_end_datetime(self, item):
        return item.dtend

    def item_start_datetime(self, item):
        if item.dtstart:
            dt_unaware = item.dtstart.replace(tzinfo=None)
            return dt_unaware

    def item_end_datetime(self, item):
        if item.dtend:
            dt_unaware = item.dtend.replace(tzinfo=None)
            return dt_unaware

    def item_guid(self, item):
        return item.uid

    def item_location(self, item):
        return item.location





