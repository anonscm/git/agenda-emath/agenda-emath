BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//SPIP//NONSGML v1.0//FR
X-WR-TIMEZONE:Europe/Paris
CALSCALE:GREGORIAN
X-WR-CALNAME;VALUE=TEXT:<a href="http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaires-ps/article/seminaires-2012-2013?date_debut=1900-01-01">Séminaires 2012-2013</a> -- Laboratoire de Mathématiques de Versailles
X-WR-RELCALID:seminaires-et-groupes-de-travail/seminaires-de-probabilites/article/seminaires-2012-2013
BEGIN:VEVENT
SUMMARY:SUMMARY:Equations de point fixe pour les grandes urnes de Plya
UID:20130604T113000-a426-e141@lmv.math.cnrs.fr
DTSTAMP:20130604T113000
DTSTART:20130604T113000
DTEND:20130604T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Cécile Mailler
DESCRIPTION:Je m'intéresse dans cet exposé aux grandes urnes de P$\\'
 o$lya. L'étude du comportement asymptotique d'une telle urne fait int
 ervenir une variable aléatoire notée W. La structure arborescente de
  l'urne nous permet de voir W comme solution d'une équation de point 
 fixe\, et d'étudier par exemple ainsi la suite de ses moments ou l'exi
 stence d'une densité. Ce travail peut être réalisé aussi bien sur 
 l'urne discrète elle-même que sur son plongement en temps continu. B
 ien que les deux variables W (associées au temps discret et au temps 
 continu) soient différentes\, elles peuvent être reliées par différ
 entes connexions qui permettent souvent de transporter les résultats 
 de l'une à l'autre. Ce travail est une collaboration avec Brigitte Ch
 auvin et Nicolas Pouyanne.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=141
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:exposé de Lucas Mercier
UID:20130423T113000-a426-e115@lmv.math.cnrs.fr
DTSTAMP:20130423T113000
DTSTART:20130423T113000
DTEND:20130423T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Lucas Mercier
DESCRIPTION:
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=115
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Grandes déviations pour une classe d'EDP perturbées par une 
 force aléatoire.
UID:20130409T113000-a426-e118@lmv.math.cnrs.fr
DTSTAMP:20130409T113000
DTSTART:20130409T113000
DTEND:20130409T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Vahagn Nersesyan
DESCRIPTION:Dans cet exposé\, nous allons nous intéresser au comporte
 ment en temps long des solutions de quelques EDP perturbées par une f
 orce aléatoire. On suppose que la force aléatoire est non dégénér
 ée\, de sorte que le système admet une unique mesure stationnaire qui
  attire toutes les solutions. Notre résultat principal montre que la 
 suite des mesures empiriques des solutions avec une position initiale 
 portée par le support de la mesure stationnaire satisfait un principe
  de grandes déviations. La preuve est basée sur un critère de Kifer
  et un théorème général sur le comportement en temps long des semi
 groupes markoviens généralisés. Travail en collaboration avec V. Ja
 ksic\, C.-A. Pillet et A. Shirikyan.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=118
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Un algorithme de Robbins-Monro pour l'estimation semi-paramét
 rique dans les modèles de déformation
UID:20130402T113000-a426-e105@lmv.math.cnrs.fr
DTSTAMP:20130402T113000
DTSTART:20130402T113000
DTEND:20130402T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Bernard Bercu 
DESCRIPTION:On s'intéresse à l'estimation semi-paramétrique dans le
 s modèles de déformation périodique. On propose un algorithme de Ro
 bbins-Monro très efficace et facile à mettre en oeuvre pour l'estima
 tion du paramètre de translation du modèle de déformation. De plus\,
  on utilise un estimateur de Nadaraya-Watson récursif pour l'estimati
 on de la fonction de déformation. On montre la convergence presque su
 re des estimateurs de Robbins-Monro et de Nadaraya-Watson. On établit
  également la normalité asymptotique de ces estimateurs. Finalement\,
  on illustre notre procédure d'estimation semi-paramétrique sur des 
 données simulées ainsi que sur des électrocardiogrammes afin de dé
 tecter des troubles du rythme cardiaque.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=105
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Variance d'une marche aléatoire de marches aléatoires.
UID:20130326T113000-a426-e114@lmv.math.cnrs.fr
DTSTAMP:20130326T113000
DTSTART:20130326T113000
DTEND:20130326T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Serge Cohen 
DESCRIPTION:Combien de temps faut-il à $K+1$ prisonniers enchainés e
 t ivres pour s'enfuir de leur prison ? Nous montrons que la marche al�
 �atoire des prisonniers est ralentie par un facteur de type variance $
 \\sigma_K^2=\\frac1{K+2}$ si l'on compare à la fuite d'un seul prisonni
 er. ___ _ L'exposé est extrait de l'article : - Emmanuel Boissard\, Se
 rge Cohen\, Thibault Espinasse\, James Norris\, {Diffusivity of a random 
 walk on random walks}
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=114
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Une construction d'ensembles régénératifs emboités
UID:20130226T113000-a426-e121@lmv.math.cnrs.fr
DTSTAMP:20130226T113000
DTSTART:20130226T113000
DTEND:20130226T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Philippe Marchal 
DESCRIPTION: Nous construisons une classe d'ensembles régénératifs 
 $R^{(\\alpha)}$ indexés par toutes les fonctions mesurables $\\alpha: [
 0\,1]\\to[0\,1]$\, Ces subordinateurs ont une propriété d'emboitement : 
 si $\\alpha\\leq \\beta$\, alors $R^{(\\alpha)} \\subset R^{(\\beta)}$. Le ca
 s $\\alpha$ constante correspond aux subordinateurs stables et aux casc
 ades de Ruelle.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=121
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Waiting times for particles in a branching Brownian motion to 
 reach the rightmost position
UID:20130219T113000-a426-e116@lmv.math.cnrs.fr
DTSTAMP:20130219T113000
DTSTART:20130219T113000
DTEND:20130219T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Xinxin Chen
DESCRIPTION:It has been proved by [Lalley and Sellke 1987] that every 
 particle born in a branching Brownian motion has a descendant reaching
  the rightmost position at some future time. We are interested in the 
 first time when every particle alive at the time s has had a descendan
 t reaching the rightmost position. ___ _ Références bibliographiques
  : - [Bramson 1978] M. Bramson\, Maximal displacement of branching Brow
 nian motion. Comm. Pure Appl. Math. 31(5) (1978)\, 531-581. - [Chen 201
 3] X. Chen\, Waiting times for particles in a branching Brownian motion
  to reach the rightmost position. Available at arXiv:1301.7606[mathPR]
  (2013). - [Lalley and Sellke 1987] S. Lalley and T. Sellke\, A conditi
 onal limit theorem for the frontier of a branching Brownian motion. An
 n. Probab. 15 (1987)\, 1052-1061.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=116
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Rates of convergence in relative entropy for normalized sums o
 f independent random variables.
UID:20130212T113000-a426-e117@lmv.math.cnrs.fr
DTSTAMP:20130212T113000
DTSTART:20130212T113000
DTEND:20130212T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Sergey Bobkov 
DESCRIPTION:Under the 4-th moment assumption\, the convergence in relat
 ive entropy for i.i.d. random variables is shown to hold with rate at 
 worst 1/n.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=117
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Étude quantitative de la variance des valeurs propres de matr
 ices de Wigner
UID:20130205T113000-a426-e95@lmv.math.cnrs.fr
DTSTAMP:20130205T113000
DTSTART:20130205T113000
DTEND:20130205T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Sandrine Dallaporta
DESCRIPTION:Dans cet exposé\, nous cherchons à établir des bornes no
 n asymptotiques sur la variance des valeurs propres de matrices de Wig
 ner. Nous commençons par présenter les enjeux de l'étude non asympt
 otique de matrices aléatoires. Les bornes sur la variance des valeurs
  propres sont établies en premier lieu pour le GUE\, avant d'être ét
 endues à des matrices de Wigner plus générales. Cette extension est
  possible grâce à l'utilisation conjointe de deux résultats récent
 s : le théorème des quatre moments de Tao et Vu\, ainsi que des propr
 iétés de localisation établies par Erdös\, Yau et Yin.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=95
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:exposé de Valentin Féray
UID:20130129T113000-a426-e94@lmv.math.cnrs.fr
DTSTAMP:20130129T113000
DTSTART:20130129T113000
DTEND:20130129T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Valentin Féray
DESCRIPTION:
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=94
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:longueur de branche extérieure du Beta-coalescent
UID:20130115T113000-a426-e93@lmv.math.cnrs.fr
DTSTAMP:20130115T113000
DTSTART:20130115T113000
DTEND:20130115T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Arno Siri-Jégousse 
DESCRIPTION:A quelle "distance" se trouve un individu du reste de la p
 opulation ? ou en d’autres termes\, combien de générations doit-on 
 remonter pour relier un individu choisi au hasard dans un échantillon
  aux autres ? Ce problème se rattache à celui de la modélisation de
 s généalogies d’une population\, dont les coalescents interchangeab
 les fournissent une représentation possible. En particulier le Beta-c
 oalescent apparaît comme l’arbre généalogique d’un processus de
  Galton Watson surcritique avec sélection des individus à chaque gé
 nération. Nous reviendrons sur l’étude de la longueur d’une bran
 che extérieure dans ce cadre et mettrons en évidence les relations e
 ntre nos résultats et ceux\, déjà connus\, dans le cadre du classique
  coalescent de Kingman. il s'agit de la présentation de cet article [
 ->http://arxiv.org/abs/1201.3983]
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=93
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Développement d'une méthode de type parallel tempering sans 
 vraisemblance (ABC-PT)
UID:20121218T103000-a426-e87@lmv.math.cnrs.fr
DTSTAMP:20121218T103000
DTSTART:20121218T103000
DTEND:20121218T113000
LOCATION:bâtiment Sophie Germain\, salle G103 
ORGANIZER:Nadège Arnaud
ATTENDEE:Agnès Grimaud 
DESCRIPTION:Les méthodes sans vraisemblance\, ou aussi appelées Appro
 ximate Bayesian Computation (ABC)\, se sont beaucoup développées ces 
 quinze dernières années. Dans un cadre bayésien\, elles permettent d
 'étudier des données lorsque la vraisemblance de celles-ci n'est pas
  disponible sous forme explicite ou que son calcul s'avère trop coût
 eux\, dans le cas où il est possible d'effectuer des simulations suiva
 nt le modèle statistique utilisé. Parmi les méthodes proposées\, on
  trouve des méthodes basées sur la théorie MCMC et des méthodes s�
 �quentielles. Les méthodes ABC-MCMC sont longtemps restées les méth
 odes de référence\, mais récemment des méthodes séquentielles\, com
 me l'ABC-PMC (Beaumont et al\, 2009) ou l'ABC-SMC (Del Moral et al\, 201
 2) sont apparues plus performantes. Nous proposons un nouvel algorithm
 e combinant le principe de l'ABC avec une approche MCMC basée sur une
  population de chaînes\, en utilisant une analogie avec l'algorithme P
 arallel Tempering (Geyer et Thompson\, 1995). Les performances de ce no
 uvel algorithme sont comparées avec celles des méthodes ABC existant
 es. Ce travail a été effectué en collaboration avec M. Baragatti et
  D. Pommeret.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=87
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Functional convergence of linear processes with heavy tail inn
 ovations
UID:20121120T113000-a426-e92@lmv.math.cnrs.fr
DTSTAMP:20121120T113000
DTSTART:20121120T113000
DTEND:20121120T123000
LOCATION:bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Adam  Jakubowski 
DESCRIPTION: A linear process built upon i.i.d. innovations $\\{Y_j\\}_{
 j\\in\\mathbb{Z}}$ is (for us) a sequence $\\{X_n\\}_{n\\in\\mathbb{Z}}$ giv
 en by $\\[ X_n = \\sum_{j\\in\\mathbb{Z}} c_{n-j} Y_j\,\\]$ where the number
 s $\\{c_j\\}_{j\\in \\mathbb{Z}}$ are such that the series defining $X_n$ 
 is convergent. We are interested in various forms of convergence of pa
 rtial sum processes $\\[ S_n(t) = \\frac{1}{a_n} \\sum_{k=1}^{[nt]} X_k.\\
 ]$ We shall consider only the case when $Y_j$'s are such that $\\[ \\fra
 c{Y_1 + Y_2 + \\ldots + Y_n}{a_n} \\indist \\mu_{\\alpha}\,\\]$ where $\\mu_{
 \\alpha}$ is a strictly $\\alpha$-stable and non-degenerate distribution
  on $\\mathbb{R}^1$\, the numbers $c_j$ are summable: $\\[ \\sum_{j\\in\\mat
 hbb{Z}} |c_j|
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=92
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Test d'adéquation pour des données sphériques bruitées
UID:20121113T113000-a426-e84@lmv.math.cnrs.fr
DTSTAMP:20121113T113000
DTSTART:20121113T113000
DTEND:20121113T123000
LOCATION:UVSQ\, batiment Fermat\, salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Claire Lacour 
DESCRIPTION: On considère une variable aléatoire X sur la sphère $S
 ^2$ (par exemple un signal astrophysique) bruitée par une rotation al
 éatoire R (par exemple un bruit de mesure)\, de telle sorte qu'on obse
 rve seulement la variable Z=R(X). On s'intéresse à l'estimation de l
 a densité f de X à partir de l'observation d'un échantillon Z1\, ...
 \, Zn et de la donnée de la densité de R. Plus précisément\, motivé
  par des questions astrophysiques\, on cherche à construire une procé
 dure de test pour savoir si cette densité est la loi uniforme sur la 
 sphère\, notée $f_0$. Comme hypothèse alternative H1\, on suppose que
  f est à distance $u_n$ de $f_0$\, et est de régularité s. On expliq
 uera comment construire une statistique de test adaptative en utilisan
 t les harmoniques sphériques. On donnera une borne inférieure et une
  borne supérieure pour la vitesse de séparation $u_n$\, qui dépend d
 e s ainsi que de la régularité de la densité de R. Cet exposé est 
 issu d'un travail en collaboration avec Thanh Mai Pham Ngoc.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=84
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Grands arbres aléatoires
UID:20121023T113000-a426-e83@lmv.math.cnrs.fr
DTSTAMP:20121023T113000
DTSTART:20121023T113000
DTEND:20121023T123000
LOCATION:UVSQ\, batiment Fermat\, salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Igor Kortchemski 
DESCRIPTION:Le comportement asymptotique d'arbres de Galton-Watson don
 t la loi de reproduction est critique et de variance finie\, conditionn
 és à avoir une grande taille fixée\, a beaucoup été étudié. Nous
  nous intéresserons à ce qui se passe lorsqu'on sort de ce cadre typ
 ique. Plus précisément\, que peut-on dire lorsque le conditionnement 
 à avoir une taille fixée est remplacée par un conditionnement à av
 oir un nombre de feuilles fixé ? Qu'advient-t-il lorsque la loi de re
 production n'est plus critique?
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=83
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Limiting Laws for of Spectral Statistics of Large Random Matri
 ces
UID:20121016T113000-a426-e86@lmv.math.cnrs.fr
DTSTAMP:20121016T113000
DTSTART:20121016T113000
DTEND:20121016T123000
LOCATION:bâtiment Fermat\, salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE: Leonid Pastur 
DESCRIPTION:We consider certain functions of eigenvalues and eigenvect
 ors (spectral statistics) of real symmetric and hermitian random matri
 ces of large size. We -rst explain that an analog of the Law of Large 
 Numbers is valid for these functions as the size of matrices tends to 
 in-nity. We then discuss the scale and the form for limiting uctuation
  laws of the statistics and show that the laws can the standard Gaussi
 an (i.e.\, analogous to usual Central Limit Theorem for appropriately n
 ormalized sums of independent or weakly dependent random variables) in
  non-standard asymptotic settings\, certain non Gaussian in seemingly s
 tandard asymptotic settings\, and other non Gaussian in non-standard as
 ymptotic settings.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=86
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Mouvement brownien branchant avec sélection
UID:20121002T113000-a426-e82@lmv.math.cnrs.fr
DTSTAMP:20121002T113000
DTSTART:20121002T113000
DTEND:20121002T123000
LOCATION:UVSQ\, bâtiment Fermat\, salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Pascal Maillard 
DESCRIPTION:Le mouvement brownien branchant (MBB) peut être vu comme 
 en système de particules qui diffusent et se reproduisent de manière
  aléatoire. Il peut également être vu comme un mouvement brownien i
 ndexé par un arbre (de Galton-- Watson)\, dit l'arbre généalogique d
 u processus. Le MBB réuni ainsi deux concepts fondamentaux en probabi
 lités : le mouvement brownien et les processus de branchement ou arbr
 es aléatoires. Ceci en fait un objet très riche en propriétés qui 
 peut être étudié (et l'a été notamment par des Versaillais) de mu
 ltiples façons probabilistes\, combinatoires ou encore analytiques. Da
 ns cet exposé je présenterai les résultats obtenu dans ma thèse su
 r un modèle de MBB unidimensionnel avec sélection\, nommé le N-MBB\, 
 où\, dès que le nombre de particules dépasse un nombre donné $N$\, s
 eules les $N$ particules les plus à droites (on voit l'espace en hori
 zontale) sont gardées tandis que les autres sont enlevées du systèm
 e. Je montrerai des asymptotiques précises sur la position du nuage d
 e particules quand $N$ est grand. Plus précisément\, celle-ci converg
 e à l'échelle de temps $\\log^3 N$ vers un processus de Lévy plus un
 e dérive linéaire\, tous les deux explicites\, ce qui a été conjectu
 ré par les physiciens Brunet\, Derrida\, Mueller et Munier\, à partir d
 e méthodes d'EDP. Notre preuve par contre est purement probabiliste. 
 J'expliquerai également comment cette étude contribue à la compréh
 ension de solutions de certaines EDPS du type FKPP avec bruit faible.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=82
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Loi des grandes urnes bicolores
UID:20120925T113000-a426-e72@lmv.math.cnrs.fr
DTSTAMP:20120925T113000
DTSTART:20120925T113000
DTEND:20120925T123000
LOCATION:UVSQ\, batiment Fermat\, salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Nicolas Pouyanne
DESCRIPTION:Les urnes de Pòlya sont des processus aléatoires très s
 imples à définir\, qui interviennent dans des modèles de l'algorithm
 ique. Dans le processus à deux couleurs\, on prend une urne contenant 
 des boules noires et rouges. On tire une boule au hasard dans l'urne\, 
 et on l'y repose après avoir noté sa couleur. Selon la couleur tiré
 e\, on ajoute dans l'urne tant de boules noires et tant de boules rouge
 s. On obtient ainsi une nouvelle composition. Le processus consiste à
  itérer ce procédé en utilisant toujours la même règle de remplac
 ement. Lorsque le nombre de tirages tend vers l'infini\, la composition
  de l'urne fait émerger de nouvelles mesures de probabilité. Toutes 
 les questions sur les propriétés de ces mesures sont permises. On mo
 ntrera comment ces lois se caractérisent par des équations de point 
 fixe dans des espaces de mesures\, ouvrant le champ à l'analyse de Fou
 rier.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=72
STATUS:CONFIRMED
END:VEVENT
BEGIN:VEVENT
SUMMARY:SUMMARY:Ergodicity of stochastic Burgers equations forced by stable no
 ises.
UID:20120918T113000-a426-e70@lmv.math.cnrs.fr
DTSTAMP:20120918T113000
DTSTART:20120918T113000
DTEND:20120918T123000
LOCATION:UVSQ\, bâtiment Fermat\, en salle 2203 
ORGANIZER:Nadège Arnaud
ATTENDEE:Lihu Xu
DESCRIPTION:In this talk\, we shall study the ergodicity of stochastic 
 Burgers equations forced by a type of alpha-stable noises called alpha
 /2-subordinated Brownian motion. We shall apply a new derivative formu
 la established by Zhang recently to show the strong Feller property. T
 hen use a classical criterion (Strong Feller+Accessibility implies erg
 odicity) to show the ergodicity. This is joint work with Zhao Dong and
  Xicheng Zhang.
CATEGORIES:02. Séminaires 2012-2013
URL:http://lmv.math.cnrs.fr/seminaires-et-groupes-de-travail/seminaire
 s-de-probabilites/article/seminaires-2012-2013?id_evenement=70
STATUS:CONFIRMED
END:VEVENT
END:VCALENDAR

