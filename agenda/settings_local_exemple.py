# -*- coding: utf-8 -*-
__author__ = 'me'
import sys
import os

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'calendar',  # Or path to database file if using sqlite3.
        'USER': 'calendar',  # Not used with sqlite3.
        'PASSWORD': 'calendar',  # Not used with sqlite3.
        'HOST': 'localhost',  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',  # Set to empty string for default. Not used with sqlite3.
    },
    # 'acm': {
    # 'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
    #     'NAME': 'acm',                      # Or path to database file if using sqlite3.
    #     'USER': 'root',                      # Not used with sqlite3.
    #     'PASSWORD': 'root',                  # Not used with sqlite3.
    #     'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
    #     'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    #     'OPTIONS': {
    #         'unix_socket': '/Applications/MAMP/tmp/mysql/mysql.sock',
    #         },
    #
    #     },
}

# change it when in production with apache
MEDIA_ROOT = ''
MEDIA_URL = ''
STATIC_ROOT = ''

# un-comment on production server
# if 'LOGNAME' in os.environ and os.environ['LOGNAME'] == 'devel':
# MONLOG = '/var/tmp/integration/agenda_mgmt.log'
# else:
#         MONLOG = '/var/tmp/integration/agenda.log'
MONLOG = 'agenda.log'


# see https://docs.djangoproject.com/en/1.7/ref/settings/#internal-ips
#INTERNAL_IPS = ('127.0.0.1',)

# ici plutôt que dans settings.py
# par exemple avec la commande: date | md5sum | head -c${1:-32} en dev, mais pas en prod, utiliser startproject!
SECRET_KEY = 'CHANGE_ME'

#EMAIL_PORT = 1025
DEFAULT_FROM_EMAIL = u'(Dev on tara) Agenda des Mathématiques'
# ajout dans le header du mail
REPLY_TO = 'no-reply@math.cnrs.fr'
X_AGENDA_EMATH = 'AGENDA_EMATH-1.0'  # pour aider au filtrage des mails

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
#EMAIL_USE_TLS = True
# Maison
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
EMAIL_HOST_USER = 'me@localhost'

# need a path for log files, written by apache
# with runserver, just let .
#LOG_PATH = '/var/tmp/agenda/logs'
LOG_PATH = 'logs'

# only used when testing
url_collect_begin = "http://localhost:8888/calendrier/exported_calendars/test_agenda/"
dir4collect = '/Applications/MAMP/htdocs/calendrier/exported_calendars/test_agenda/'

# used by PORTAIL
#URL_PORTAIL = "https://portail-pre.math.cnrs.fr/agenda"
URL_PORTAIL = "https://portail-dev.math.cnrs.fr/agenda"
URL_CONNECT = "https://calendrier.math.cnrs.fr"

# ordered sites in Site, first site is usually in https and need authentication
# , the second ones are just http and are public
SITES = [
    # ('domain', 'name')
    ('calendrier.math.cnrs.fr', 'Agenda des Maths'),
    ('consult.calendrier.emath.fr', 'ical'),
    ('consult.calendrier.emath.fr', 'rss'),
]

#exemple pour avoir tous les logs activés
local_loggers = {
    'eventsmgmt.signals': {
    'handlers': ['console', 'file'],
    'level': 'DEBUG',
    'propagate': False,
        },
    'collector.views': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'collector.models': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'collector.parsers': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'collector.tests': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'collector.management.commands.populate': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'eventsmgmt.management.commands.populate': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'eventsmgmt.management.commands.survey': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'eventsmgmt.management.commands.subscriptions': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'eventsmgmt.management.commands.smai': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'eventsmgmt.views': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'eventsmgmt.forms': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'eventsmgmt.models': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'eventsmgmt.tests': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'api.views': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'api.serializers': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'api.tests': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'api.search': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'api.events_filter': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'acm_odm_migration.views': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'acm_odm_migration.migrate': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'acm_odm_migration.management.commands.site_acm': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'agenda.interface_annuaire': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'agenda.version': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'cities': {
        'handlers': ['console'],
        'level': 'INFO',
    },
    'django_auth_ldap': {
        'handlers': ['console'],
        'level': 'DEBUG',
        'propagate': True,
    },
    'eventsmgmt.management.commands.update_groups': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'api.tests.test_filter': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'allaccess.clients': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },
    'allaccess.views': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG',
        'propagate': False,
    },

}



# un-comment to use with shibboleth and apache
# AUTHENTICATION_BACKENDS = (
#  'django.contrib.auth.backends.ModelBackend',
#   'eventsmgmt.plm.CustomRemoteUserBackend'
# )

# chemin vers le cache Django sur disk activé par défaut

if os.environ['LOGNAME'] == 'www-data':
    CACHE_DIR = '/tmp/django_cache_prod'
elif os.environ['LOGNAME'] == 'jenkins':
    CACHE_DIR = '/tmp/django_cache_jenkins'
else:
    CACHE_DIR = '/tmp/django_cache'

ADMINS = (
    ('Nom Prénom', 'mail@admin.com'),
)

# le from des mails d'erreur que l'on reçoit en prod
LOCAL_SERVER_EMAIL = 'no-reply@serveur.prod'

LOCAL_MIDDLEWARE_CLASSES = (
    # use it to difference test and production
)

LOCAL_TEMPLATES_OPTIONS_CONTEXT_PROCESSORS = [
    # use it to difference test and production
]

LOCAL_INSTALLED_APPS = (
    # use it to difference test and production
)

# django-all-access: the name you defined in /admin/allaccess/provider/1/
OAUTH2_PROVIDER_NAME = 'agenda_emath_dev'

#contexte ds agenda/settings_eventsmgmt.py
LOCAL_EVENTSMGMT_INFOS_CTX='prod'  # ou 'dev'