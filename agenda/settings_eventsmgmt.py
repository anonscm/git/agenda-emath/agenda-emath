# -*- coding: utf-8 -*-
__author__ = 'me'
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
#import ldap
#from django_auth_ldap.config import LDAPSearch
from agenda.version import my_version

"""
Number of events presented in feed
"""
NEXT_EVENTS_FEED = 50
GLOBAL_NEXT_EVENTS_FEED = 10

"""
Period between subscriptions mailing in days
"""
RECURRENCE = 7

"""
Minimal length of keyword to accept searching
"""
LG_KEYWORD_MIN = 2

# Annuaire
SERVER_ORG = ['http://api.mathdoc.fr/labo',
    # 'https://plm.math.cnrs.fr:4443/ws/directory/entities',
    #           "https://portail-dev.math.cnrs.fr/ws/directory/entities",
    #           "https://portail.math.cnrs.fr/ws/directory/entities",
              ]
# [u'status', u'entities', u'totalCount']
PLM_API_KEY_ORGANISMS = 'entities'
# for each entity
PLM_API_KEY_ORG = 'name'
PLM_API_KEY_CODE = 'id'
PLM_API_KEY_ORDER = 'NA'
PLM_API_KEY_LOCALITY = 'city'
PLM_API_KEY_ALIAS = 'alias'

CORRESP_PLM_MATHDOC = (
    ('code', 'id'),
    ('entity', 'name'),
    ('locality', 'city'),
)

MATHDOC_API_KEYS = (
    "alias",
    "city",
    "data_last_modified",
    "email",
    "fax",
    "id",
    "lab_creation_date",
    "lab_end_date",
    "lat",
    "lon",
    "manager_email",
    "manager_first_name",
    "manager_name",
    "name",
    "phone",
    "po_box",
    "street",
    "type",
    "url",
    "zipcode",
)

CLASS_REGION = {
   1: "Centre, Paris, Ile de France",
   2: "Nord-Ouest",
   3: "Nord-Est",
   4: "Sud-Est",
   5: "Sud-Ouest",
   6: "Autres pays européens",
   7: "Autres",
}

PARSERS = (
    ( 'AC', _( u'ACM' ) ),
    ( 'IC', _( u'iCal' ) ),
    ( 'JS', _( u'Indico/JSON' ) ),
    ( 'RS', _( u'RSS' ) ),
    ( 'XM', _( u'XML' ) ),
)

ORIGIN_OF_IMPORT = (
    ( 'NA', _( u'Not Applicable' ) ),
    ( 'AC', _( u'ACM' ) ),
    ( 'OD', _( u'Officiel des Maths' ) ),
)

DATE_INPUT_FORMATS = ['%Y-%m-%d',      # '2006-10-25'
'%m/%d/%Y',       # '10/25/2006'
'%m/%d/%y']       # '10/25/06'

# Collect parameters
LIMIT_EVENTS = 9999

# model MyCalendar length of fields: title, relcalid, prodid
CALENDAR_FIELD_LG_MAX = 200  # Be care look at max_length in MyCalendar model or adjust database with south !

# max number of events returned in API REST by default
NB_EVENTS_FROM_NOW = 50

# ICAL
# number of days for past events: 365 -> we include one year past events in iCal feed
ICAL_DAYS_BEFORE = 365
# default email in iCal for ATTENDEE
DEFAULT_ATTENDEE_EMAIL = "noreply@math.cnrs.fr"

# number of calendars displayed in "latest" and "errors" urls
NB_CAL_TO_DISPLAY = 5

# TIMEOUT
TIMEOUT_URL_COLLECT = 5 # 0.001

# AUTH_LDAP_SERVER_URI = "ldap://auth-lille.mathrice.fr"
# AUTH_LDAP_BIND_DN = "uid=nss,o=admin,dc=mathrice,dc=fr"
# AUTH_LDAP_BIND_PASSWORD = "asc1234"
# AUTH_LDAP_BIND_AS_AUTHENTICATING_USER = True
# AUTH_LDAP_USER_DN_TEMPLATE = "uid=%(user)s,o=People,dc=mathrice,dc=fr"
# # AUTH_LDAP_USER_SEARCH = LDAPSearch("o=People,dc=mathrice,dc=fr",
# #     ldap.SCOPE_SUBTREE, "(uid=%(user)s)")
# AUTH_LDAP_START_TLS = True

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 1025
PART1 = u'Agenda des Mathématiques'
PART2 = ""
PART3 = "<agenda@listes.math.cnrs.fr>"
DEFAULT_FROM_EMAIL = "%s %s %s" % (PART1, PART2, PART3)
#EMAIL_SUBJECT_PREFIX = '[%s] Votre abonnement' % Site.objects.get_current().name
EMAIL_SUBJECT_PREFIX = 'Votre abonnement'
EMAIL_SUBJECT_PREFIX_COLLECT_FAILED = u'Echec de la collecte de votre séminaire'
EMAIL_SUBJECT_PREFIX_CAL_CREATED = u'Nouveau séminaire'
EMAIL_SUBJECT_PREFIX_ANN_CREATED = u'Votre annonce créee'
EMAIL_SUBJECT_PREFIX_ANN_DELETED = u'Votre annonce effacée'
REPLY_TO = PART3
X_HEADER = 'X_AgendaMaths'

# do we send mail to author? No in case of tests
MAIL_TO_AUTH = True
# do we send mail to subscriber? No in case of tests
MAIL_TO_SUB = True

# contact email to display on web UI
version = "1.0"
infos = {
    'version': version,
    'contact': PART3,
    'ctx': 'prod',  # or prod
}

from settings_local import LOCAL_EVENTSMGMT_INFOS_CTX
if LOCAL_EVENTSMGMT_INFOS_CTX:
    infos['ctx']=LOCAL_EVENTSMGMT_INFOS_CTX

# concerns SMAI announces
SMAI_TITLE = [
    u"Conférences ACM",
    u"Annonce de nouveaux congrès ",
    u"Annonces de congrès",
    ]