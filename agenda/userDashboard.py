# -*- coding: utf-8 -*-
"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'mathrice.userDashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'mathrice.userDashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name

from collector.models import Collector
from django.utils.html import format_html
import re

class CollectorDashboardModule(modules.DashboardModule):
    title = _('Log des Collectes')
    template = 'admin/dashboard/last_collectors.html'
    post_content = format_html('<a href="%s">En voir plus ...</a>' % reverse('admin:collector_collector_changelist'))
    def init_with_context(self, context):
        if context.request.user.is_superuser:
            last_collectors = Collector.objects.order_by('-created_on')[:20]
        else:
            last_collectors = Collector.objects.order_by('-created_on').filter(calendar__created_by=context.request.user)[:10]
        for collector in last_collectors:
            dict = {}
            dict['title'] = collector.calendar.title
            dict['created_on'] = collector.created_on
            dict['finished_on'] = collector.finished_on
            regex = re.compile("\s*\d*\s*events\s*")
            if regex.match(collector.check_status):
                color = '#BAE6AC'
            elif collector.check_status == '' :
                color = '#FAE298'
            else:
                color = '#FFC7C7'
            dict['status'] = collector.check_status
            dict['color'] = color
            dict['id'] = collector.id
            self.children.append(dict)


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for mathrice.
    """
    title = u"Gestion de mes données"

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        # append a link list module for "quick links"
        self.children.append(modules.LinkList(
            _('Quick links'),
            layout='inline',
            draggable=False,
            deletable=False,
            collapsible=False,
            children=[
                [_(u'Consulter les évènements sur le Portail Math'), 'https://portail.math.cnrs.fr/agenda'],
                [_(u'Ajouter un séminaire'), 'eventsmgmt/mycalendar/add/'],
                [_(u'Ajouter une annonce '), 'eventsmgmt/myevent/add/'],
                [_('Log out'), reverse('%s:logout' % site_name)],
            ]
        ))

        self.children.append(modules.ModelList(
            title=u'Mes données',
            models=['eventsmgmt.*', 'collector.*'])
        )

        self.children.append(modules.ModelList(
            title=u'User Management',
            models=['django.contrib.auth.*'])
        )
        self.children.append(CollectorDashboardModule())

        # append a recent actions module
        # self.children.append(modules.RecentActions(_('Recent Actions'), 5))

        '''
        # Ce serait bien d'ajouter une aide et un feed sur les dernières mises à jour
        # Ou : les derniers évènements saisis?
        # append a feed module
        self.children.append(modules.Feed(
            "Nouvelle de l'agenda des maths",
            feed_url='http://www.djangoproject.com/rss/weblog/',
            limit=5
        ))


        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _('Support'),
            children=[
                {
                    'title': _('Django documentation'),
                    'url': 'http://docs.djangoproject.com/',
                    'external': True,
                },
                {
                    'title': _('Django "django-users" mailing list'),
                    'url': 'http://groups.google.com/group/django-users',
                    'external': True,
                },
                {
                    'title': _('Django irc channel'),
                    'url': 'irc://irc.freenode.net/django',
                    'external': True,
                },
            ]
        ))
        '''


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for mathrice.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            CollectorDashboardModule()
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
