# -*- coding: utf-8 -*-
__author__ = 'me'

URL_INDICO_REF_DOC="http://indico.cern.ch/ihelp/html/ExportAPI/access.html"
# Root of Indico Website see: https://indico.math.cnrs.fr
#URL_INDICO_GLOBAL="https://indico-new.math.cnrs.fr/export/categ/0.json"
URL_INDICO_GLOBAL="https://indico.math.cnrs.fr/export/categ/0.json"

CORRESP = (
    # numero Indico # type # id org # id_region
    (54, "ANN", 136, 5),
    (116, "COURS", 42, 1),

)

# first col: see https://indico.math.cnrs.fr/index.py
# second col: regions in Django DB
REGIONS = (
    (6, 1),
    (7, 2),
    (8, 3),
    (9, 4),
    (10, 6),
)

