# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.views.generic import ListView
from django.views.generic import TemplateView
from eventsmgmt.models import MyEvent
from eventsmgmt.models import MyOrganisation
from eventsmgmt.models import MyCalendar
from eventsmgmt.models import Subscription
from eventsmgmt.feeds import LatestEntriesFeed
from eventsmgmt.feeds import LatestAnnouncesFeed
from eventsmgmt.feeds import MyCalendarFeed
from eventsmgmt.feeds import SubscribeFeed
from eventsmgmt.feeds import SubscribeIcal
from eventsmgmt.feeds import MyEventFeed, MyEventICS, MyCalendarICS
from eventsmgmt.views import MyOrganisationMyCalendarListView
from eventsmgmt.views import MyCalendarListView, MyCalendarMyEventListView
from eventsmgmt.views import MyCalendarLatestListView
from eventsmgmt.views import MyCalendarVerifyListView
from eventsmgmt.views import MyOrganisationDetailView
from eventsmgmt.views import MyAnnounceListView
from eventsmgmt.views import OAuthCallbackPlm, OAuthRedirectPlm   # specific to Mathrice
from allaccess.views import OAuthRedirect, OAuthCallback


from datetime import datetime

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# uncomment the next 2 lines if you want have the way of @login_required work
# from django.contrib.auth.decorators import login_required
# admin.site.login = login_required(admin.site.login)


# from eventsmgmt.userAdmin import userAdmin
# url(r'^userAdmin/', include(userAdmin.urls)),

urlpatterns = patterns('',
    # Examples:

    # url(r'^$', 'eventsmgmt.views.home', name='home'),
    url(r'^$', 'eventsmgmt.views.homepage', name='homepage'),
    url(r'^faq.html$', TemplateView.as_view(template_name="faq.html")),
    url(r'^legal.html$', TemplateView.as_view(template_name="legal.html")),

    # url(r'^$', 'django.contrib.auth.views.login', name='home'), -> utilise bien la vue de login mais ne permet pas
    #    d'ajouter des infos comme le nbre d'événements


    # # url(r'^agenda/', include('agenda.foo.urls')),
    # url(r'^about/$', 'eventsmgmt.views.about', name='about'),
    # url(r'^cas_test/$', 'eventsmgmt.views.cas_test', name='cas_test'),
    #
    # url(r'^calendars/$', 'eventsmgmt.views.list_calendars', name='calendars'),
    # # url(r'^calendars/$', MyCalendarCompleteListView.as_view(
    # #     model=MyCalendar
    # # ), name='calendars'),
    url(r'^calendars/(?P<pk>\d+)/$', 'eventsmgmt.views.details_on_calendar', name='calendar_detail'),
    # # url(r'^calendars/(?P<pk>\d+)/$',
    # #     MyCalendarMyEventListView.as_view(
    # #         model=MyCalendar,
    # #         template_name='calendar/detail.html'
    # #     ), name='calendar_detail'),
    # url(r'^calendars/(?P<pk>\d+)/update/$', 'eventsmgmt.views.update_cal', name='update_cal'),
    # url(r'^calendars/(?P<pk>\d+)/delete/$', 'eventsmgmt.views.delete_cal', name='delete_cal'),
    # url(r'^calendars/add/$', 'eventsmgmt.views.create_cal', name='create_cal'),
    # url(r'^calendars/many/add/$', 'eventsmgmt.views.create_many_cal', name='create_many_cal'),
    # url(r'^calendars/new/$', 'eventsmgmt.views.create_cal', name='create_cal'),
    # url(r'^calendars/many/new/$', 'eventsmgmt.views.create_many_cal', name='create_many_cal'),
    # url(r'^calendars/upload/$', 'eventsmgmt.views.upload_file', name='upload_file'),
    # url(r'^calendars/export/$', 'eventsmgmt.views.export_calendars', name='export_calendars'),
    # url(r'^calendars/verify/$', MyCalendarVerifyListView.as_view(
    #         model=MyCalendar,
    #         template_name='calendar/verify.html'
    #     ), name='verify_calendars'),
    # url(r'^calendars/latest/$', MyCalendarLatestListView.as_view(
    #         model=MyCalendar,
    #         template_name='calendar/list.html'
    #     ), name='recent_calendars'),
    # url(r'^calendars/(?P<user_pk>\d+)/all/$',
    #     MyCalendarListView.as_view(
    #         model=MyCalendar,
    #         template_name='calendar/list.html'
    #     ), name='calendar_list'),
    #
    # url(r'^organizations/$', ListView.as_view(
    #     model=MyOrganisation,
    #     template_name = 'organisation/list.html'
    # ), name='organizations'),
    # # url(r'^organizations/(?P<pk>\d+)/$',
    # #     MyOrganisationMyCalendarListView.as_view(
    # #         model=MyOrganisation,
    # #         template_name='organisation/detail.html'
    # #     ), name='organisation_detail'),
    # # url(r'^organizations/sync_with_plm/$', 'eventsmgmt.views.synchro', name='organisation_synchro'),
    url(r'^organizations/(?P<pk>\d+)/$', 'eventsmgmt.views.details_on_organization', name='organisation_detail'),
    #

#URL utilisé par le portail : lien iCal à droite de chaque événements !!
    url(r'^events/(?P<evt_id>\d+)/ical/$', MyEventICS(), name='eventics'),

    # url(r'^events/ical/all/$', MyEventFeed(), name='latestical'),
    url(r'^calendars/(?P<cal_id>\d+)/rss/$', MyCalendarFeed(), name='calfeed'),
    url(r'^calendars/(?P<cal_id>\d+)/ical/$', MyCalendarICS(), name='calics'),
    #
    # url(r'^future_events/$', 'eventsmgmt.views.future_events', name='future_events'),
    # url(r'^future_announces/rss/$', LatestAnnouncesFeed(), name='rss_future_announces'),
    # url(r'^future_announces/$', 'eventsmgmt.views.future_announces', name='future_announces'),
    # url(r'^future_events/rss/$', LatestEntriesFeed(), name='future_rss'),
    # url(r'^events/$', ListView.as_view(model=MyEvent), name='events'),
    url(r'^events/(?P<pk>\d+)/$', 'eventsmgmt.views.details_on_event', name='details_on_event'),
    # url(r'^last_events/$', 'eventsmgmt.views.latest_events', name='latest_events'),
    # url(r'^events/add/$', 'eventsmgmt.views.create_event', name='create_event'),
    # url(r'^event/new/$', 'eventsmgmt.views.create_event', name='create_event'),
    # url(r'^events/(?P<pk>\d+)/update/$', 'eventsmgmt.views.update_evt', name='update_evt'),
    # url(r'^events/(?P<pk>\d+)/delete/$', 'eventsmgmt.views.delete_evt', name='delete_evt'),
    # url(r'^announces/(?P<user_pk>\d+)/all/$',
    #     MyAnnounceListView.as_view(
    #         model=MyEvent,
    #         template_name='announce/list.html'
    #     ), name='announce_list'),
    #
    # url(r'^subscriptions/$', 'eventsmgmt.views.subscriptions', name='subscriptions'),
    # url(r'^subscriptions/(?P<pk>\d+)/update/$', 'eventsmgmt.views.update_sub', name='update_sub'),
    # url(r'^subscriptions/(?P<pk>\d+)/delete/$', 'eventsmgmt.views.delete_sub', name='delete_sub'),

## vue dédiée à des vues d'abonnements
    url(r'^subscriptions/(?P<pk>\d+)/$', 'eventsmgmt.views.details_on_subscription', name='detail_subscription'),
    url(r'^subscriptions/(?P<sub_id>\d+)/rss/$', SubscribeFeed(), name='feed_by_user'),
    url(r'^subscriptions/(?P<sub_id>\d+)/ical/$', SubscribeIcal(), name='ical_by_user'),

##
    # url(r'^subscriptions/user/(?P<pk>\d+)/$', 'eventsmgmt.views.user_subscriptions', name='user_subscriptions'),
    # # idem as above, to keep old urls
    # url(r'^subscription/$', 'eventsmgmt.views.subscriptions', name='subscriptions'),
    # url(r'^subscription/(?P<pk>\d+)/update/$', 'eventsmgmt.views.update_sub', name='update_sub'),
    # url(r'^subscription/(?P<pk>\d+)/delete/$', 'eventsmgmt.views.delete_sub', name='delete_sub'),

    # (r'^ajax_subscribe/$', 'eventsmgmt.views.ajax_subscribe'),
    # (r'^ajax_unsubscribe/$', 'eventsmgmt.views.ajax_unsubscribe'),
    # (r'^ajax_remindme/$', 'eventsmgmt.views.ajax_remindme'),
    # (r'^ajax_forget/$', 'eventsmgmt.views.ajax_forget'),
    # (r'^ajax_set_default_cal/$', 'eventsmgmt.views.ajax_set_default_cal'),
    # (r'^ajax_create_cal/$', 'eventsmgmt.views.ajax_create_cal'),
    #
    # url(r'^prefs/$', 'eventsmgmt.views.preferences', name='preferences'),
    # url(r'^last_users/$', 'eventsmgmt.views.last_users', name='last_users'),
    # url(r'^search/$', 'eventsmgmt.views.search', name='search'),

    (r'^accounts/', include('registration.backends.default.urls')),
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # url dédiées à allacess
    url(r'^accounts/login/(?P<provider>(\w|-)+)/$', OAuthRedirectPlm.as_view(), name='allaccess-login'),
    url(r'^accounts/callback/(?P<provider>(\w|-)+)/$', OAuthCallbackPlm.as_view(), name='allaccess-callback'),
    url(r'^accounts/', include('allaccess.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),



    url(r'^admin_tools/', include('admin_tools.urls')),

    url(r'^', include('api.urls')),

    # if collector is used
    url(r'^', include('collector.urls')),

    #url(r'^', include('acm_odm_migration.urls')),

)


