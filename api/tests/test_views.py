# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

import json
import os, subprocess
import logging
import datetime
import hashlib, string, time, random, math
import dateutil.parser
from dateutil.parser import parse
from icalendar import Calendar, Event

from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.six import BytesIO

from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate
from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from eventsmgmt.models import MyEvent
from eventsmgmt.models import MyCalendar
from eventsmgmt.models import MyOrganisation
from eventsmgmt.extras import create_ical


from api.views import OrganisationList, CalendarList, MyEventList, AnnounceList
from api.serializers import EventSerializer

from eventsmgmt.tests.factories import *

from agenda.settings import REST_FRAMEWORK

"""
voir https://docs.djangoproject.com/en/1.5/topics/testing/advanced/#django.test.client.RequestFactory
et
http://www.django-rest-framework.org/api-guide/testing

"""

SERV_URL = "http://localhost:8003"
REF_MIN = 10000
REF_MAX = 10001




class OrgTest(APITestCase):
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.client = APIClient()
        self.user = User.objects.create_user(username='lauren', email='jacob@toto.fr', password='secret')
        self.user.is_staff = True
        self.user.save()
        self.client.login(username='lauren', password='secret')
        print("Login? %s" % self.user.is_authenticated())

    def test_create_org(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('myorganisation-list')
        for ref in range(REF_MIN, REF_MAX):
            self.name = "My Organism by api"
            self.ref = ref
            data = {"name": self.name,
                    "ref": self.ref,
                    'mycalendar_set': []}
            response = self.client.post(url, data, format='json')
            print response.data
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(response.data, data)

    def test_details(self):
        factory = APIRequestFactory()
        view = OrganisationList.as_view()
        # Create an instance of a GET request.
        request = factory.get('/api/organisms/')
        force_authenticate(request, user=self.user)
        response = view(request)
        response.render()
        print response.data
        self.assertEqual(response.status_code, 200)


class CalendarTest(APITestCase):
    """
    we create a new calendar with POST, and we read it with GET
    """
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.org1 = OrgFactory()
        self.user = UserFactory()
        self.user.set_password('secret')
        self.user.save()
        self.client = APIClient()

    def test_read_cal(self):
        self.cal = CalendarFactory.create(org=(self.org1,))
        self.event = EventFactory()
        response = self.client.get(reverse('mycalendar-detail', kwargs={'pk': self.cal.id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_cal(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")

        self.client.login(username=self.user.username, password='secret')
        url = reverse('mycalendar-list')
        data = {"title": u"Mon séminaire de test",
                "created_by": self.user.pk,
                "org": [self.org1.ref, ]}
        # logger.debug("url: %s" % url)
        # logger.debug("data: %s" % data)
        response = self.client.post(url, data, format='json')
        print u"POST creation cal: %s" % response.data
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.cal_id = response.data['id']
        self.assertEqual(response.data['title'], data['title'])
        self.assertEqual(response.data['created_by'], self.user.pk)

        response = self.client.get('/api/calendars/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = json.loads(response.content)
        try:
            self.assertNotIn('count', data.keys())
        except AttributeError, e:
            self.assertEqual(len(data), 1)

    def test_create_cal_without_authentication(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")

        url = reverse('mycalendar-list')
        data = {"title": u"Mon séminaire de test",
                "created_by": self.user.pk,
                "org": [self.org1.ref, ]}
        # logger.debug("url: %s" % url)
        # logger.debug("data: %s" % data)
        response = self.client.post(url, data, format='json')
        # print u"POST creation cal: %s" % response.data
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class Calendar4PdMTest(APITestCase):
    """
    verify that calendars are given without pagination
    """
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.org1 = OrgFactory()

    def test_no_paginate(self):
        """
        """
        N = 2
        self.calendars = CalendarFactory.create_batch(N, org=(self.org1,))
        self.assertEqual(MyCalendar.objects.count(), N)
        self.client = APIClient()
        response = self.client.get('/api/calendars/cache/?format=json')
        print "response.data "+ response.content
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = json.loads(response.content)
        # print data
        # import pprint
        # pprint.pprint(data)
        try:
            self.assertNotIn('count', data.keys())
        except AttributeError, e:
            self.assertEqual(len(data), N)

    def test_no_visible(self):
        """
        if a calendar is not visible, it is not included
        """
        logger = logging.getLogger(__name__)
        N = 10
        self.calendars = CalendarFactory.create_batch(N, org=(self.org1,))
        logger.debug("display new calendars ")
        for cal in MyCalendar.objects.all():
            logger.debug("cal: %s" % cal.id)
            cal.display()
        logger.debug("cal %s is set to unpublished" % cal.id)
        cal.published = False
        cal.save()
        self.assertEqual(MyCalendar.objects.count(), N)
        self.client = APIClient()
        response = self.client.get('/api/calendars/cache/?format=json')
        print "response.data "+ response.content
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = json.loads(response.content)
        # print data
        # import pprint
        # pprint.pprint(data)
        try:
            self.assertNotIn('count', data.keys())
        except AttributeError, e:
            self.assertEqual(len(data), N-1)


class Event4PdMTest(APITestCase):
    """
    this test verify that all events from dtstart are given without pagination
    tried with more than 1000 events
    """
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.org1 = OrgFactory()
        self.cal = CalendarFactory.create(org=(self.org1,))

    def test_paginate(self):
        """
        verify that settings.REST_FRAMEWORK[PAGINATE_BY]
        """
        if REST_FRAMEWORK['PAGINATE_BY']:
            N = REST_FRAMEWORK['PAGINATE_BY'] + 1
            self.events = EventFactory.create_batch(N, calendar=self.cal)
            # for evt in MyEvent.objects.all():
            #     evt.display()
            self.assertEqual(MyEvent.objects.count(), N)
            self.client = APIClient()
            response = self.client.get('/api/events/')
            data = json.loads(response.content)
            self.assertEqual(data['count'], REST_FRAMEWORK['PAGINATE_BY'])

    def test_no_paginate(self):
        """
        verify that settings.REST_FRAMEWORK[PAGINATE_BY]
        """
        self.assertIsNone(REST_FRAMEWORK['PAGINATE_BY'])
        N = 150
        self.events = EventFactory.create_batch(N, calendar=self.cal)
        # for evt in MyEvent.objects.all():
        #     evt.display()
        self.assertEqual(MyEvent.objects.count(), N)
        self.client = APIClient()
        response = self.client.get('/api/events/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = json.loads(response.content)
        # print data
        # import pprint
        # pprint.pprint(data)
        self.assertEqual(len(data), N)

    def test_events(self):
        """
        """
        N = 3
        self.events = EventFactory.create_batch(N, calendar=self.cal)
        for evt in MyEvent.objects.all():
            evt.dtend = evt.dtstart + datetime.timedelta(hours=1)
            evt.save()
            evt.display()
        self.assertEqual(MyEvent.objects.count(), N)
        self.client = APIClient()
        response = self.client.get('/api/events/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = json.loads(response.content)
        # print data
        import pprint
        pprint.pprint(data)
        self.assertEqual(len(data), N)


class EventTest(APITestCase):
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.org1 = OrgFactory()
        self.user = UserFactory()
        self.user.set_password('secret')
        self.user.save()
        self.cal = CalendarFactory.create(org=(self.org1,), created_by=self.user)
        self.client = APIClient()
        self.client.login(username=self.user.username, password='secret')

    def test_create_event(self):
        """
        to create a single event, we create a calendar (as in iCal) and associate the event.
        This calendar will contain one event
        """
        description = "Ma description"
        summary = u"Petit résumé"
        response = create_event(self.client, self.cal.id, description, summary, self.user)
        print u"POST creation event: %s" % response.data
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['description'], description)
        self.assertEqual(response.data['summary'], summary)
        self.assertEqual(response.data['calendar'], self.cal.id)

        self.assertEqual(MyEvent.objects.count(), 1)
#        self.assertEqual(response.data['description'], data['description'])
        response = self.client.get('/api/events/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = json.loads(response.content)
        self.assertEqual(len(data), 1)


class AnnounceTest(APITestCase):
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.user = User.objects.create_user(username='lauren',
                                             email='jacob@toto.fr', password='secret')
        self.user.is_staff = True
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        # self.client.login(username=self.user.username, password=self.user.password)
        # print("Login? %s" % self.user.is_authenticated())
        url = reverse('myorganisation-list')
        ref = REF_MIN
        self.name = "My Organism by api"
        self.ref = ref
        data = {"name": self.name,
                "ref": self.ref,
                'mycalendar_set': []}
        response = self.client.post(url, data, format='json')
        print u"POST creation org: %s" % response.data
        ref_id = response.data['ref']
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, data)


    def test_create_event(self):
        """
        to create a simple announce, we don't need a calendar (as in iCal).
        uid will be unique if possible
        first event at 9:00, second at 10:00
        """
        url = reverse('announces-list')
        i = 1
        for texte in (u'première', 'seconde'):
            self.title = u"Une %s annonce importante" % texte
            dtstart = timezone.now().replace(minute=0, hour=0, second=0, microsecond=0) \
                      + datetime.timedelta(days=i+1)
            dtstart = dtstart.replace(hour=9+i, minute=0)
            data = {
                    "dtstart": dtstart,
                    "summary": self.title,
                    "created_by":self.user.pk,
                    "category": "Workshop",
                    "org": [self.ref,],
                    }
            print data
            response = self.client.post(url, data, format='json')
            print u"POST creation annonce: %s" % response.data
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            if 'id' in response.data:
                evt_id = response.data['id']
            for key, val in data.items():
                if key in ('dtstart', 'dtend', 'dtstamp'):
                    self.assertEqual(dateutil.parser.parse(response.data[key]), val)
                else:
                    self.assertEqual(response.data[key], val)

            i += 1

        factory = APIRequestFactory()
        view = AnnounceList.as_view()
        # Create an instance of a GET request.
        request = factory.get('/api/announces/')
        force_authenticate(request, user=self.user)
        response = view(request)
        response.render()
        print u"recupere id ann: %s" % response.data
        self.assertEqual(response.status_code, 200)

        # url = reverse('announces-list')
        # self.title = u"Mon évènement de test"
        # uid = uniqid()
        # data = {"title": self.title,
        #         "uid": uid,
        #         "dtstart": datetime.datetime.now(),
        #         "summary": u"Petit résumé",
        #         "description": "Ma description",
        #         "calendar": cal_id}
        # response = self.client.post(url, data, format='json')
        # print u"POST création event: %s" % response.data
        # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # self.assertEqual(response.data['description'], data['description'])
        # self.assertEqual(response.data['summary'], data['summary'])
        # self.assertEqual(response.data['calendar'], data['calendar'])
        # self.assertEqual(response.data['description'], data['description'])

    def get_events(self):
        """
        on teste qu'on peut récupérer un ensemble d'events
        """
        pass

    def search_events(self):
        """
        on teste qu'on peut récupérer un ensemble d'events suite à une recherche
        """
        pass


class CreateSubscriptionTest(TestCase):
    """
    the purpose is to create a new subscription associated with a keyword
    keyword can be region, city, or string
    """
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.org1 = OrgFactory()
        self.org2 = OrgFactory()
        self.cal1 = CalendarFactory.create(org=(self.org1,))
        self.cal2 = CalendarFactory.create(org=(self.org1,))
        self.cal3 = CalendarFactory.create(org=(self.org2,))
        self.cal4 = CalendarFactory.create(org=(self.org2,))

        # containing one event
        self.nb_events = 3
        events = []
        value = timezone.now()
        self.dates = ()
        for n in range(self.nb_events):
            dtstart = value + datetime.timedelta(days=n+1)
            dtend = dtstart + datetime.timedelta(hours=2)
            self.dates += ((dtstart, dtend),)
            evt = {'summary': 'AM Test %d' % n,
                   'speaker': 'Nom%d Prenom%d' % (n, n),
                   'dtstart': dtstart,
                   'dtend': dtend,
                   'uid': '%d-%s/27346262376@%s' % (n+1, time.time(), 'agenda.local'),
                "description": "Ma description",

            }
            print evt
            events.append(evt)
    #         self.event = MyEvent.objects.create(calendar=self.cal,
    # #                                            uid=evt['uid'],
    #                                             dtstart=evt['dtstart'],
    #                                             summary=evt['summary'],
    #                                             attendee=evt['speaker'],
    #                                             created_by=self.user,
    #                                             )

    def test_subscribe(self):
        """
        Test one subscription
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        sub = SubscriptionFactory()
        # TODO
        # sub.calendar.add(self.cal)
        # sub.save()
        # sub.created_by.userprofile.my_default_subscription = sub
        # sub.created_by.userprofile.save()

class SubscriptionTest(TestCase):
    """
    on cree un calendrier (il faut org), qu'on remplit d'evenements
    on utilise l'api pour s'abonner au calendrier
    on demande la liste des evenements lies a l'abonnement
    """
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)

        url = "http://www.changeme.fr"
        title = "mon calendrier"
        self.user = User.objects.create_user(username='lauren',
                                             email='jacob@toto.fr', password='secret')
        self.user.is_superuser = True
        self.user.save()
        self.membre = User.objects.create_user(username='membre', email='membre@titi.fr', password='secret')
        self.cal = MyCalendar(title=title, url_to_parse=url, created_by=self.user)
        self.cal.parse = 'IC'
        self.cal.enabled = True
        self.cal.save()
        # on l'ajoute au labo
        self.cal.org.add(self.org1)
        self.cal.save()

        # containing one event
        self.nb_events = 3
        events = []
        value = timezone.now()
        self.dates = ()
        for n in range(self.nb_events):
            dtstart = value + datetime.timedelta(days=n+1)
            dtend = dtstart + datetime.timedelta(hours=2)
            self.dates += ((dtstart, dtend),)
            evt = {'summary': 'AM Test %d' % n,
                   'speaker': 'Nom%d Prenom%d' % (n, n),
                   'dtstart': dtstart,
                   'dtend': dtend,
                   'uid': '%d-%s/27346262376@%s' % (n+1, time.time(), 'agenda.local'),
                "description": "Ma description",

            }
            print evt
            events.append(evt)
            self.event = MyEvent.objects.create(calendar=self.cal,
    #                                            uid=evt['uid'],
                                                dtstart=evt['dtstart'],
                                                summary=evt['summary'],
                                                attendee=evt['speaker'],
                                                created_by=self.user,
                                                )

    def test_get_subscriptions(self):
        """
        Test one subscription
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        # self.client.login(username=self.user.username, password=self.user.password)
        print("Login? %s" % self.user.is_authenticated())
        url = reverse('subscription-list')
        response = self.client.get(url)
        print "response.data "+ response.content
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = json.loads(response.content)
        print data
        import pprint
        pprint.pprint(data)

    def test_subscribe(self):
        """
        Test one subscription
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        url = reverse('subscription-list')
        data = {
                "calendar": [self.cal.id, ],
                "created_by":self.membre.pk,
                "email": self.membre.email,
                }

        self.client = APIClient()
        self.client.login(username='lauren', password='secret')
        response = self.client.post(url, data, format='json')
        logger.debug(u"POST creation subscription: %s" % response.data)
        # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        if response.status_code == status.HTTP_201_CREATED:
            if 'calendar' in response.data:
                for cal in response.data['calendar']:
                    mycal = MyCalendar.objects.get(id=cal)
                    print mycal.url_to_parse, mycal.get_parse_display()
                    if MyEvent.objects.filter(calendar=mycal).count():
                        for evt in MyEvent.objects.filter(calendar=mycal):
                            print evt
                    else:
                        logger.error("aucun evenement?")

#http://consult.calendrier.emath.fr:8000/api/subscriptions/
#http://consult.calendrier.emath.fr:8000/api/subscriptions/

class SearchInCalendarTest(TestCase):
    """
    on cree un calendrier (il faut org), qu'on remplit d'evenements
    on utilise l'api pour s'abonner au calendrier
    on demande la liste des evenements lies a l'abonnement
    """
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)

        url = "http://www.changeme.fr"
        self.title = "mon calendrier"
        self.user = User.objects.create_user(username='lauren', email='jacob@toto.fr', password='secret')
        self.cal = MyCalendar(title=self.title, url_to_parse=url, created_by=self.user)
        self.cal.parse = 'IC'
        self.cal.enabled = True
        self.cal.save()
        # on l'ajoute au labo
        self.cal.org.add(self.org1)
        self.cal.save()

    def test_cal_search(self):
        """
        search one cal by title
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        url = reverse('mycalendar-list')
        # see DRF http://www.django-rest-framework.org/api-guide/filtering/#searchfilter
        url = "%s?search=%s" % (url, self.title[:3])
        data = {}
        self.client = APIClient()
        self.client.login(username='lauren', password='secret')
        logger.debug("url: %s" % url)
        response = self.client.get(url, data)
        logger.debug(u"GET search: %s" % response.status_code)
        logger.debug(u"GET search: %s" % response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # TODO finir le test
        # self.assertEqual(response.data['count'], 1)


class SearchByDateInEventTest(TestCase):
    """
    to test requests like /api/events/?start=2016-05-12&format=json
    """
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)

        url = "http://www.changeme.fr"
        title = "mon calendrier"
        self.user = User.objects.create_user(username='lauren',
                                             email='jacob@toto.fr', password='secret')
        self.user.is_superuser = True
        self.user.save()
        self.cal = MyCalendar(title=title, url_to_parse=url, created_by=self.user)
        self.cal.parse = 'IC'
        self.cal.enabled = True
        self.cal.save()
        # on l'ajoute au labo
        self.cal.org.add(self.org1)
        self.cal.save()



    def test_event_filter_dtstart(self):
        """
        Filter events by dates
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        # Nb events are created, one in the past, 2 at D-day, and he others the days after
        self.nb_events = 5
        events = []
        self.current_date = timezone.now()
        self.dates = (self.current_date + datetime.timedelta(days=-1),
                      self.current_date,
                      self.current_date,
        self.current_date + datetime.timedelta(days=+1),
        self.current_date + datetime.timedelta(days=+2),
        )
        n = 1
        for date in self.dates:
            dtstart = date  # one event in the past
            dtend = dtstart + datetime.timedelta(hours=2)
            self.dates += ((dtstart, dtend),)
            evt = {'summary': 'AM Test %d' % n,
                   'speaker': 'Nom%d Prenom%d' % (n, n),
                   'dtstart': dtstart,
                   'dtend': dtend,
                   'uid': '%d-%s/27346262376@%s' % (n+1, time.time(), 'agenda.local'),
                "description": "Ma description",

            }
            n += 1
            print evt
            events.append(evt)
            self.event = MyEvent.objects.create(calendar=self.cal,
    #                                            uid=evt['uid'],
                                                dtstart=evt['dtstart'],
                                                summary=evt['summary'],
                                                attendee=evt['speaker'],
                                                created_by=self.user,
                                                )

        url = reverse('myevent-list')
        self.client = APIClient()
        self.client.login(username='lauren', password='secret')
        # self.client.force_authenticate(user=self.user)
        print("Login? %s" % self.user.is_authenticated())
        data = {}

        # see DRF http://www.django-rest-framework.org/api-guide/filtering/#searchfilter
        # filter with the exact string, nt partial (see search)
#        url = "%s?start=%s&end=%s" % (url, self.dates[0][0].date(), self.dates[0][1].date())
        # We want the last event
        url = "%s?start=%s" % (url, self.current_date.date())
        logger.debug("url: %s" % url)
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)


    def test_event_without_time(self):
        """
        see the problem when dtstart or dtend are only date, without time
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        # Nb events are created, one in the past, 2 at D-day, and he others the days after
        events = []
        self.current_date = timezone.now()
        self.dates = (
            (self.current_date + datetime.timedelta(days=-1)).replace(hour=21, minute=0, second=0, microsecond=0),
            (self.current_date + datetime.timedelta(days=-1)).replace(hour=22, minute=0, second=0, microsecond=0),
            (self.current_date + datetime.timedelta(days=-1)).replace(hour=23, minute=0, second=0, microsecond=0),
                      self.current_date,
        self.current_date + datetime.timedelta(days=+1),
        self.current_date + datetime.timedelta(days=+2),
        )
        self.nb_events = len(self.dates)
        # dtend:"2016-05-11T23:00:00Z"
        n = 1
        for date in self.dates:
            dtstart = date
            dtend = dtstart + datetime.timedelta(hours=+1)
            self.dates += ((dtstart, dtend),)
            evt = {'summary': 'AM Test %d' % n,
                   'speaker': 'Nom%d Prenom%d' % (n, n),
                   'dtstart': dtstart,
                   'dtend': dtend,
                   'uid': '%d-%s/27346262376@%s' % (n+1, time.time(), 'agenda.local'),
                "description": "Ma description",

            }
            n += 1
            print evt
            events.append(evt)
            self.event = MyEvent.objects.create(calendar=self.cal,
    #                                            uid=evt['uid'],
                                                dtstart=evt['dtstart'],
                                                summary=evt['summary'],
                                                attendee=evt['speaker'],
                                                created_by=self.user,
                                                )

        url = reverse('myevent-list')
        self.client = APIClient()
        self.client.login(username='lauren', password='secret')
        # self.client.force_authenticate(user=self.user)
        print("Login? %s" % self.user.is_authenticated())
        data = {}

        # see DRF http://www.django-rest-framework.org/api-guide/filtering/#searchfilter
        # filter with the exact string, nt partial (see search)
#        url = "%s?start=%s&end=%s" % (url, self.dates[0][0].date(), self.dates[0][1].date())
        # We want the last event
        url = "%s?start=%s" % (url, self.current_date.date())
        logger.debug("url: %s" % url)
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.nb_events-3)


class SearchAndFilterInEventTest(TestCase):
    """
    on cree un calendrier (il faut org), qu'on remplit d'evenements
    on utilise l'api pour s'abonner au calendrier
    on demande la liste des evenements lies a l'abonnement
    """
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)

        url = "http://www.changeme.fr"
        title = "mon calendrier"
        self.user = User.objects.create_user(username='lauren',
                                             email='jacob@toto.fr', password='secret')
        self.user.is_superuser = True
        self.user.save()
        self.cal = MyCalendar(title=title, url_to_parse=url, created_by=self.user)
        self.cal.parse = 'IC'
        self.cal.enabled = True
        self.cal.save()
        # on l'ajoute au labo
        self.cal.org.add(self.org1)
        self.cal.save()

        self.nb_events = 5
        events = []
        self.current_date = timezone.now()
        logger.debug("current date is %s" % self.current_date)
        self.dates = ()
        for n in range(self.nb_events):
            dtstart = self.current_date + datetime.timedelta(days=n-1)  # one event in the past
            dtend = dtstart + datetime.timedelta(hours=2)
            self.dates += ((dtstart, dtend),)
            evt = {'summary': 'AM Test %d' % n,
                   'speaker': 'Nom%d Prenom%d' % (n, n),
                   'dtstart': dtstart,
                   'dtend': dtend,
                   'uid': '%d-%s/27346262376@%s' % (n+1, time.time(), 'agenda.local'),
                "description": "Ma description",

            }
            print evt
            events.append(evt)
            self.event = MyEvent.objects.create(calendar=self.cal,
    #                                            uid=evt['uid'],
                                                dtstart=evt['dtstart'],
                                                summary=evt['summary'],
                                                attendee=evt['speaker'],
                                                created_by=self.user,
                                                )



    def test_event_filter_dtstart(self):
        """
        Filter events by dates
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        url = reverse('myevent-list')
        self.client = APIClient()
        self.client.login(username='lauren', password='secret')
        # self.client.force_authenticate(user=self.user)
        print("Login? %s" % self.user.is_authenticated())
        data = {}

        # see DRF http://www.django-rest-framework.org/api-guide/filtering/#searchfilter
        # filter with the exact string, nt partial (see search)
#        url = "%s?start=%s&end=%s" % (url, self.dates[0][0].date(), self.dates[0][1].date())
        # We want the last event
        url = "%s?start=%s" % (url, self.dates[-1][0].date())
        logger.debug("url: %s" % url)
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        # same with date other formatted and nb_events -1
        # url = reverse('myevent-list')
        # url = "%s?start=%s" % (url, self.dates[1][0].date().strftime('%d-%m-%Y'))
        # logger.debug("url: %s" % url)
        # response = self.client.get(url, data, format='json')
        # self.assertEqual(response.status_code, status.HTTP_200_OK)
        # self.assertEqual(len(response.data), self.nb_events -1)


    def test_event_filter_daterange(self):
        """
        Filter events by dates, the first event will be returned
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        url = reverse('myevent-list')
        self.client = APIClient()
        self.client.login(username='lauren', password='secret')
        data = {}

        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.nb_events-1)  # never return past events

        # see DRF http://www.django-rest-framework.org/api-guide/filtering/#searchfilter
        # filter with the exact string, nt partial (see search)
        url = "%s?start=%s&end=%s" % (url, self.dates[1][0].date(), self.dates[3][1].date())
        logger.debug("url: %s" % url)
        response = self.client.get(url, data, format='json')
        logger.debug(u"GET search: %s" % response.status_code)
        logger.debug(u"GET search: %s" % response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        logger.debug("len: %s" % len(response.data))
        # TODO finir le test
        self.assertEqual(len(response.data), self.nb_events-2)


    def test_event_search(self):
        """
        Test one subscription
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        url = reverse('myevent-list')
        # see DRF http://www.django-rest-framework.org/api-guide/filtering/#searchfilter
        url = "%s?search=Test 1" % url
        data = {}
        self.client = APIClient()
        self.client.login(username='lauren', password='secret')
        logger.debug("url: %s" % url)
        response = self.client.get(url, data)
        logger.debug(u"GET search: %s" % response.status_code)
        logger.debug(u"GET search: %s" % response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # TODO finir le test
        # self.assertEqual(response.data['count'], 1)
        # self.assertEqual(response.data['results'][0]['summary'], 'AM Test 1')



def uniqid(prefix='', more_entropy=False):
    m = time.time()
    uniqid = '%8x%05x' % (math.floor(m), (m - math.floor(m)) * 1000000)
    if more_entropy:
        valid_chars = list(set(string.hexdigits.lower()))
        entropy_string = ''
        for i in range(0, 10, 1):
            entropy_string += random.choice(valid_chars)
        uniqid = uniqid + entropy_string
    uniqid = prefix + uniqid
    return uniqid


def my_random():
    md5 = hashlib.md5()
    md5.update(uniqid(more_entropy=True))
    my_rand = md5.hexdigest()
    return my_rand


class SerializersTest(TestCase):
    """
    """
    def setUp(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        self.ref = 10001
        self.name = u"1. Mon Organisme de Test écouté"
        self.auteur = User.objects.create(username='auteur')
        self.org1 = MyOrganisation.objects.create(name=self.name, ref=self.ref)
        self.user = User.objects.get(username='auteur')
        self.category = CategoryFactory()


    def test_serialize(self):
        """
        Test one subscription
        """

        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")

        i = 1
        dtstart = timezone.now().replace(hour=9+i, minute=0, second=0, microsecond=0) \
                  + datetime.timedelta(days=i+1)
        evt = {
            'dtstart': dtstart,
            'summary': u'SUMMARY:Announce Number 1 from Me',
            'description': u"La structure arborescente de l'urne nous permet de voir W ",
            'location': u'en salle 2203 ',
            'organizer': u'Nadege Arnaud',
            'category': 'Workshop',
            'url': 'http://it.describes.the.announce/',
            }

        self.event = MyEvent.objects.create(dtstart=evt['dtstart'],
                                            summary=evt['summary'],
                                            category=evt['category'],
                                            created_by=self.auteur,
                                            )
        self.event.org.add(self.org1)
        self.event.save()
        self.event.uid = "UID_42"
        self.event.save()

        # serialization
        serializer = EventSerializer(self.event)
        print(u"Serialized data: %s" % repr(serializer.data))
        content = JSONRenderer().render(serializer.data)

        # de-serialization
        stream = BytesIO(content)
        data = JSONParser().parse(stream)
        print u"De-Serialized data: %s" % data
        new_serializer = EventSerializer(data=data)
        self.assertTrue(new_serializer.is_valid())
        print new_serializer.validated_data
        # OrderedDict([('title', ''), ('code', 'print "hello, world"\n'), ('linenos', False), ('language', 'python'), ('style', 'friendly')])
        new_serializer.save()
        # <Snippet: Snippet object>

def create_org(client, user):
    """

    :return:
    """
    print("Login? %s" % user.is_authenticated())
    url = reverse('myorganisation-list')
    ref = REF_MIN
    name = "My Organism by api"
    data = {"name": name,
            "ref": ref,
            'mycalendar_set': []}
#    print u"POST création org: %s" % response.data
    return client.post(url, data, format='json')

def create_cal(title, client, user, ref):
    """

    :param client:
    :param user:
    :param ref:
    :return:
    """
    url = reverse('mycalendar-list')
    data = {"title": title,
            "created_by": user.pk,
            "org": [ref, ]}
    response = client.post(url, data, format='json')
    print u"POST creation cal: %s" % response.data
    return response

def create_event(client, cal_id, description, summary, user):
    """

    :param client:
    :param cal:
    :return:
    """
    url = reverse('myevent-list')
    title = u"Mon évènement de test"
    uid = uniqid()
    print "uid: %s" % uid
    data = {"uid": uid,
            "created_by": user.pk,
            "dtstart": timezone.now(),
            "summary": summary,
            "description": description,
            "calendar": cal_id}
    response = client.post(url, data, format='json')
    return response
