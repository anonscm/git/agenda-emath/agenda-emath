# -*- coding: utf-8 -*-
# Create your views here.

from dateutil import parser
from dateutil import parser
import datetime
import json
from bs4 import BeautifulSoup

import logging

from django.http import HttpResponse
from django.http import JsonResponse

from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User, Group
from django.utils import timezone
from django.db.models import Q
from django.db.utils import IntegrityError
import django_filters
from django.utils.translation import ugettext_lazy as _

from django.views.decorators.cache import cache_page
from django.core.cache import cache

from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.decorators import permission_classes
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import filters
# from rest_framework.pagination import LimitOffsetPagination
from rest_framework.test import APIClient

from eventsmgmt.models import MyEvent
from eventsmgmt.models import MyCategory
from eventsmgmt.models import MyCalendar, MyOrganisation, Subscription
from api.serializers import UserSerializer
from api.serializers import EventSerializer
from api.serializers import CalendarSerializer, OrganismSerializer, SubscriptionSerializer
from api.serializers import CalendarPortalSerializer
from api.serializers import OrgSerializer
from api.serializers import AnnounceSerializer
from api.serializers import MyCategorySerializer

from agenda.settings_eventsmgmt import NB_EVENTS_FROM_NOW
from api.search import filter_events
from api.events_filter import filter
from agenda.interface_annuaire import get_orgs
from agenda.interface_annuaire import get_orgs_by_id


@api_view(['GET'])
def api_root(request, format=None):
    """
    The entry endpoint of our API.
    """
    return Response({
        'users': reverse('user-list', request=request),
        'events': reverse('myevent-list', request=request),
        'announces': reverse('announces-list', request=request),
        'calendars': reverse('mycalendar-list', request=request),
        'organisms': reverse('myorganisation-list', request=request),
        'subscriptions': reverse('subscription-list', request=request),
#        'mysubscriptions': reverse('subscription-by-user', args=('157',), request=request),
#        'calendarbysubscription': reverse('calendarbysubscription-list', request=request, args=['user_id']),
#subscription-by-user
    })


class UserList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of users.
    """
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.exclude(is_superuser=True).filter(is_active=True)

    model = User
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single user.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        content = {
            'status': 'request was permitted'
        }
        return Response(content)

    model = User
    serializer_class = UserSerializer
    queryset = User.objects.exclude(is_superuser=True).filter(is_active=True)


class MyEventList(generics.ListAPIView):
    """
    API endpoint that represents a list of events.
    without start, only NB_EVENTS_FROM_NOW events from now are returned
    without end, all future events are returned
    events or calendars not published are excluded
    for instance: /api/events/?start=2014-12-17&end=2014-12-17 (YYYY-MM-DD)

    You can also filter events by 'summary', 'description', 'attendee',
                      'location', 'category'
    /api/events/?summary=exact_phrase

    or search a word in 'summary', 'description', 'attendee',
                      'location', 'category'
    /api/events/?search=a_word_or_part_of_word
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = MyEvent.objects.all()
    serializer_class = EventSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('summary', 'description', 'attendee',
                      'location', 'category', 'published')
    search_fields = ('summary', 'description', 'attendee',
                      'location', 'category')

    def get_queryset(self):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut --")
        user = self.request.user
        queryset = MyEvent.objects.exclude(published=False)\
                 .exclude(calendar__published=False)
        search_param = self.request.query_params.get('search', None)
        if search_param is not None:
            logger.debug("search: %s" % search_param)
        else:
            search_param = ''
        include_past_event = self.request.query_params.get('include_past_event', None)
        queryset = filter_events(category='ALL', user=user, include_past_event=include_past_event, keywords=search_param)
        # date_filter = Q(start_date__range=[self.start_date, self.end_date]) | Q(end_date__range=[self.start_date,self.end_date])
        filter = {}

        start_date = self.request.query_params.get('start', None)
        end_date = self.request.query_params.get('end', None)

        if start_date is not None:
            logger.debug("start_date: %s" % start_date)
            filter['dtstart__gte'] = correct_date(start_date)

        if end_date is not None:
            end_date = (correct_date(end_date) + datetime.timedelta(days=1)).date()  #
            logger.debug("end_date: %s" % end_date)
            filter['dtstart__lte'] = end_date

        if filter:
            logger.debug("filter is %s" % filter)
        queryset = queryset.filter(**filter)

        return queryset


    def post(self, request, format=None):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        logger.debug("POST %s" % request.POST)
        serializer = EventSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EventDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single event.
    """
    model = MyEvent
    serializer_class = EventSerializer
    queryset = MyEvent.all_published_events.all()


class CalendarList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of calendars.
    """
    serializer_class = CalendarSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = MyCalendar.objects.all()
    filter_fields = ('title', )
    search_fields = ('title', )

    def post(self, request, *args, **kwargs):
        logger = logging.getLogger(__name__)
        logger.debug("-- request.data %s -- " % request.data)
        user = User.objects.get(id=request.data['created_by'])
        logger.debug("user %s" % user)
        for org in request.data['org']:
            logger.debug("org %s" % MyOrganisation.objects.get(ref=org))
        return self.create(request, *args, **kwargs)


class CalendarEventList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of calendars.
    """
    model = MyCalendar
    serializer_class = CalendarSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        logger = logging.getLogger(__name__)
        logger.debug("GET %s" % request.GET)
        if 'start' in request.GET:
            value = parser.parse(str(request.GET['start']))
            if timezone.is_naive(value):
                try:
                    dtstart = timezone.make_aware(value, timezone.utc)
                except AttributeError:
                    dtstart = value
            else:
                dtstart = value
            logger.debug("search events starting after %s" % dtstart)
            q1 = MyCalendar.objects.get(pk=kwargs['pk']).myevent_set.filter(dtstart__gte=dtstart)
            if 'end' in request.data:
                value = parser.parse(str(request.GET['end']))
                if timezone.is_naive(value):
                    try:
                        dtend = timezone.make_aware(value, timezone.utc)
                    except AttributeError:
                        dtend = value
                else:
                    dtend = value
                logger.debug("exclude events starting after %s" % dtend)
                Events = q1.exclude(dtstart__gt=dtend + datetime.timedelta(days=1))
            else:
                Events = q1
        else:
            Events = MyCalendar.objects.get(pk=kwargs['pk']).myevent_set.filter(
                dtstart__gte=timezone.make_aware(datetime.datetime.now(), timezone.utc))[:NB_EVENTS_FROM_NOW]
        if not Events:
            logger.debug("No event")
        else:
            logger.debug("%d events" % Events.count())

        serializer = EventSerializer(Events)
        return Response(serializer.data)


class CalendarDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single calendar.
    """
    model = MyCalendar
    serializer_class = CalendarSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = MyCalendar.objects.all()


class CalendarBySubscriptionList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        content = {
            'status': 'request was permitted'
        }
        return Response(content)

    model = Subscription
    serializer_class = SubscriptionSerializer

    def get_queryset(self):
        user_pk = self.kwargs.get('user_pk', None)
        if user_pk is not None:
            return Subscription.objects.filter(user__pk=user_pk)
        return []


class MyCategoryList(generics.ListCreateAPIView):
    """
    API endpoint that shows all categories.
    """
    model = MyCategory
    serializer_class = MyCategorySerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)


class MyCategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that lists all the calendars in one category.
    """
    model = MyCategory
    serializer_class = MyCategorySerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)


class OrganisationList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of organism.
    """
    model = MyOrganisation
    serializer_class = OrganismSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = MyOrganisation.objects.all()


class OrganisationDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single organism.
    """
    model = MyOrganisation
    serializer_class = OrganismSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = MyOrganisation.objects.all()


class AnnounceList(APIView):
    """
    API endpoint that represents a list of events.
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, format=None):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        logger.debug("GET %s" % request.GET)
        q1 = MyEvent.objects.filter(calendar=None)
        if 'start' in request.GET:
            value = parser.parse(str(request.GET['start']))
            if timezone.is_naive(value):
                try:
                    dtstart = timezone.make_aware(value, timezone.utc)
                except AttributeError:
                    dtstart = value
            else:
                dtstart = value
            logger.debug("search events starting after %s" % dtstart)
            q1 = q1.filter(dtstart__gte=dtstart)
            if 'end' in request.GET:
                value = parser.parse(str(request.GET['end']))
                if timezone.is_naive(value):
                    try:
                        dtend = timezone.make_aware(value, timezone.utc)
                    except AttributeError:
                        dtend = value
                else:
                    dtend = value
                logger.debug("exclude events starting after %s" % dtend)
                if q1:
                    q1 = q1.exclude(dtstart__gt=dtend + datetime.timedelta(days=1))
            else:
                Events = q1
        else:
            Events = q1.filter(dtstart__gte=timezone.make_aware(datetime.datetime.now(), timezone.utc))[
                     :NB_EVENTS_FROM_NOW]
        if not Events:
            logger.debug("No event")
        else:
            logger.debug("%d events" % Events.count())

        serializer = AnnounceSerializer(Events, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        logger = logging.getLogger(__name__)
        logger.debug("-- Begin create event -- ")
        logger.debug("POST %s" % request.POST)
        if not request.user.is_authenticated():
            content = {
                'status': 'request was permitted'
            }
            return Response(content)
        serializer = AnnounceSerializer(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save()
            except IntegrityError, e:
                logger.error(e)
            else:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AnnounceDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single event.
    """
    model = MyEvent
    serializer_class = AnnounceSerializer
    queryset = MyEvent.all_announces.all()


class SubscriptionList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of subscriptions.
    """
    permission_classes = (IsAuthenticated,)

    model = Subscription
    serializer_class = SubscriptionSerializer
    queryset = Subscription.objects.all()

    # def post(self, request, format=None):
    #     logger = logging.getLogger(__name__)
    #     logger.debug("-- Begin create subscription -- ")
    #     logger.debug("POST %s" % request.POST)
    #     if not request.user.is_authenticated():
    #         content = {
    #             'status': 'request was permitted'
    #         }
    #         return Response(content)
    #     serializer = SubscriptionSerializer(data=request.DATA)
    #     if serializer.is_valid():
    #         try:
    #             serializer.save()
    #         except IntegrityError, e:
    #             logger.error(e)
    #         else:
    #             return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SubscriptionDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single subscription.
    """
    model = Subscription
    serializer_class = SubscriptionSerializer
    queryset = Subscription.objects.all()


class SubscriptionByUserList(generics.ListAPIView):
    """

    """
    permission_classes = (IsAuthenticated,)

    # def get(self, request, format=None,*args, **kwargs):
    #     content = {
    #         'status': 'request was permitted'
    #     }
    #     return Response(content)
    #
    # model = MyCalendar
    serializer_class = SubscriptionSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        # user = User.objects.get(id=self.kwargs['user_id'])
        return Subscription.objects.filter(created_by=self.kwargs['user_id'])
        # serializer_class=
        # return MyCalendar.objects.filter(subscription_by_cal__in=user.sub_created_by.all())



class CalendarSearchList(generics.ListAPIView):
    """

    """
    model = MyCalendar
    serializer_class = CalendarSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = MyCalendar.objects.exclude(published=False)
        username = self.request.query_params.get('username', None)
        if username is not None:
            queryset = queryset.filter(created_by__username=username)
        email = self.request.query_params.get('email', None)
        if email is not None:
            queryset = queryset.filter(created_by__email=email)
        return queryset


@api_view(['GET', 'OPTIONS'])
def event_latest(request):
    """
    List latest 5 ? events TODO
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    if request.method == 'GET':
        Events = MyEvent.objects.exclude(calendar__published=False).order_by('-created_on')[:5]
        serializer = EventSerializer(Events)
        response = Response(serializer.data)
        #        response['Access-Control-Allow-Origin'] = '*'
        #        response['Access-Control-Allow-Headers'] = '*'
        return response
    elif request.method == 'OPTIONS':
        response = Response()
        response['Allow'] = '*'
        return response


@api_view(['GET', 'PUT'])
@permission_classes((IsAuthenticatedOrReadOnly, ))
def event_detail(request, pk, format=None):
    """
    Retrieve, update or delete a Event instance.
    """
    try:
        Event = MyEvent.objects.get(pk=pk)
    except MyEvent.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = EventSerializer(Event)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = EventSerializer(Event, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#    elif request.method == 'DELETE':
#        Event.delete()
#        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST', 'PUT', 'DELETE', 'HEAD'])
@permission_classes((IsAuthenticatedOrReadOnly, ))
def calendar_list(request, format=None):
    """
    List all calendars, or create a new calendar..
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")

    if request.method == 'GET':
        logger.debug("GET %s" % request.GET)
        logger.debug("return all calendars")
        cals = MyCalendar.objects.exclude(published=False)
        serializer = CalendarSerializer(cals)
        return Response(serializer.data)

    elif request.method == 'POST':
        logger.debug("POST %s" % request.POST)
        data = JSONParser().parse(request)
        logger.debug("request to create a new calendar (POST) %s" % data)

        serializer = CalendarSerializer(data=data['calendar'])
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    else:
        logger.debug("%s FIXME" % request.method)


#essai fonction CORS
def addCORSHeaders(theHttpResponse):
    if theHttpResponse and isinstance(theHttpResponse, HttpResponse):
        theHttpResponse['Access-Control-Allow-Origin'] = '*'
        theHttpResponse['Access-Control-Max-Age'] = '120'
        theHttpResponse['Access-Control-Allow-Credentials'] = 'true'
        theHttpResponse['Access-Control-Allow-Methods'] = 'HEAD, GET, OPTIONS, POST, DELETE'
        theHttpResponse['Access-Control-Allow-Headers'] = 'origin, content-type, accept, x-requested-with'
    return theHttpResponse


# Vue dédiée au portail des maths agenda : c'est liste des calendar actifs mais on ressort les orgs en premier pour un tri par location
@cache_page(60 * 15)
@api_view(['GET'])
# @renderer_classes((JSONRenderer))
def calendars_portal(request):
    logger = logging.getLogger(__name__)
    logger.debug("cals: %d" % MyCalendar.objects.count())
    logger.debug("published: %d" % MyCalendar.objects.filter(published=True).count())
    logger.debug("unpublished: %d" % MyCalendar.objects.exclude(published=True).count())
    cals = MyCalendar.objects.filter(published=True).exclude(category__isnull=True)
    serializer = CalendarPortalSerializer(cals,many=True)
    return addCORSHeaders(Response(serializer.data))


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((IsAuthenticatedOrReadOnly, ))
def calendar_detail(request, pk, format=None):
    """
    Retrieve, update or delete a Calendar instance.
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    try:
        Cal = MyCalendar.objects.get(pk=pk)
    except MyCalendar.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        logger.debug("return %s" % Cal)
        serializer = CalendarSerializer(Cal)
        return Response(serializer.data)

    elif request.method == 'POST':
        logger.debug("request to create a new calendar (POST) %s" % Cal)
        serializer = CalendarSerializer(Cal, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'PUT':
        logger.debug("return %s" % Cal)
        serializer = CalendarSerializer(Cal, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        logger.debug("want to delete %s FIXME" % Cal)
        #Cal.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def org_list(request, format=None):
    """
    List all calendars, or create a new calendar..
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    if request.method == 'GET':
        logger.debug("GET %s" % request.GET)
        logger.debug("return all organisms with their calendars")
        orgs = MyOrganisation.objects.all()
        serializer = OrgSerializer(orgs)
        res = {"count": orgs.count(), "results": serializer.data}
        return Response(res)

    else:
        logger.debug("%s FIXME" % request.method)


@api_view(['GET'])
def search(request, format=None):
    """
    search by keywords

    :returns
    response_data = {count: N, results: {...}}
    results is a dict composed of:
                    id_org, id_cal, dtstart, dtend, summary

    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")

    if request.method == 'GET':
        res = None
        logger.debug("GET %s type %s" % (request.data, type(request.data)))
        queryset = None
        response_data = {}
        if 'keywords' in request.data and request.data['keywords']:
            keywords = request.data['keywords']
            logger.debug('"search for "%s"' % keywords)
            filter_events(queryset, None, include_past_event=None, keywords=None, created_by=None,
                  regions=None, start_date=None, end_date=None)
            if keywords:
                queryset = MyEvent.objects.exclude(calendar__published=False).filter(Q(summary__icontains=keywords)
                                                  | Q(description__icontains=keywords)
                                                  | Q(attendee__icontains=keywords)).order_by('-dtstart')
                logger.debug("%d events match" % queryset.count())
                response_data['count'] = queryset.count()
                response_data['results'] = []
            for event in queryset:
                line = {}
                id_org = event.calendar.org.all()[0].ref
                id_cal = event.calendar.id
                line['id_org'] = id_org
                line['id_cal'] = id_cal
                line['dtstart'] = str(event.dtstart)
                line['dtend'] = str(event.dtend)
                line['summary'] = event.summary
                response_data['results'].append(line)
        else:
            logger.debug("nothing to search ?")
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        logger.debug("%s FIXME" % request.method)

# vue des events dédiés au portail : avec les filtres de api.mathdoc.fr
# schema du json different du model Event
#   {
#     "start_day": "string",
#     "start_month": "string",
#     "start_year": "string",
#     "end_day": "string",
#     "end_month": "string",
#     "end_year": "string",
#     "url": "string",
#     "summary": "string",
#     "agenda_id": 0,
#     "lab_id": 0,
#     "city": "string",
#     "country": "string",
#     "zipcode": "string",
#     "zone": "string",
#     "seminar": "string",
#     "lat": "string",
#     "lon": "string",
#     "attendee": "string",
#     "id": 0
#   }

def events_filter(today=False):
    """
    :param today: if true, return event only for today
    :return: a list of futur published events filtered by rules like : title doesn't contain 'A confirmer' etc...
    """
    logger = logging.getLogger(__name__)
    if today:
        tz = timezone.get_current_timezone()
        now = datetime.datetime.now(tz)
        tomorrow = datetime.datetime.now(tz)+ datetime.timedelta(days=1)

        today_midnight = datetime.datetime(now.year, now.month, now.day, tzinfo=tz)

        events = MyEvent.objects.filter(dtstart__gte=today_midnight).filter(dtstart__lte=tomorrow).filter(published=True).exclude(
            calendar__published=False).order_by('dtstart')
    else:
        events = MyEvent.objects.filter(dtstart__gte=datetime.date.today()).filter(published=True).exclude(
            calendar__published=False).order_by('dtstart')
    # logger.debug("les events a filtrer %s depuis le %s" % (events,timezone.make_aware(datetime.datetime.now(), timezone.utc)))
    response_data = []
    for event in events:
        logger.debug("on envoie au filtre l'event %s" % event)
        if filter(event):
            response_data.append(filter(event))
    return response_data

# vue dédiée au portail avec un filtrage des événements très précis : cf. doc
@cache_page(60*60*24,cache="cache_events")
@api_view(['GET'])
def events_portal(request):
    data = events_filter()
    # mise en cache des 5 premiers résultats pour la vue partielle
    cache.delete('partial')
    cache.set('partial', data[0:5])
    return addCORSHeaders(JsonResponse(data, safe=False))

@cache_page(60*60*24,cache="cache_events")
@api_view(['GET'])
def first_events_portal(request):
    data = cache.get('partial')
    if not data:
        data = events_filter()
        cache.delete('partial')
        cache.set('partial', data[0:5])
    return addCORSHeaders(JsonResponse(data[0:5], safe=False))



def correct_date(my_date):
    """

    :param my_date:
    :return:
    """
    value = parser.parse(my_date)
    if timezone.is_naive(value):
        try:
            date_converted = timezone.make_aware(value, timezone.utc)
        except AttributeError:
            date_converted = value
    else:
        date_converted = value

    return date_converted
