# -*- coding: utf-8 -*-
__author__ = 'me'

from django.contrib.auth.models import User, Group, Permission

from rest_framework import serializers
from eventsmgmt.models import MyEvent
from eventsmgmt.models import MyCategory
from eventsmgmt.models import MyCalendar, MyOrganisation, Subscription
from eventsmgmt.models import create_uid

import logging

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class EventSerializer(serializers.ModelSerializer):
#    uid = serializers.SerializerMethodField('gen_uid')
    org = serializers.SerializerMethodField('get_orgs')
    created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    def gen_uid(self, event):
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        logger.debug("-- self.context %s -- ", self.context.keys())
        if event.uid:
            logger.debug("uid given by remote")
            return event.uid
        else:
            logger.debug("uid created")
            return create_uid(event)

    def get_orgs(self, event):
        orgs = []
        if event.calendar:
            source = event.calendar.org.all()
        else:
            source = event.org.all()
        for org in source:
            orgs.append(org.ref)
        return orgs

    class Meta:
        model = MyEvent
#        fields = ('id', 'title', 'code', 'linenos', 'language', 'style')
        exclude = ('rrule', 'status', 'created_on', 'edited_on')

class AnnounceSerializer(serializers.ModelSerializer):
    uid = serializers.SerializerMethodField('gen_uid')
    created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    def gen_uid(self, event):
        return create_uid(event)

    class Meta:
        model = MyEvent
#        fields = ('id', 'title', 'code', 'linenos', 'language', 'style')
        exclude = ('rrule', 'status', 'created_on', 'edited_on', 'calendar')


class SubscriptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subscription
        #fields = ('calendar', 'enabled', 'subscriber')
        fields = '__all__'

class MyCategorySerializer(serializers.ModelSerializer):
    # mycalendar_set = serializers.HyperlinkedRelatedField(many=True, read_only=True,
    #                                                     view_name='mycalendar-detail')
    class Meta:
        model = MyCategory
        fields = ('name',)

class CalendarSerializer(serializers.ModelSerializer):
    #subscription_by_cal = SubscriptionSerializer(many=True, read_only=True)
    # category = MyCategorySerializer(many=False, read_only=True)
    category = serializers.StringRelatedField()
    class Meta:
        model = MyCalendar
        fields = ('title','url_to_desc', 'id', 'created_by', 'url_to_parse', 'org','category',)
#        fields = ('title', 'url_to_desc', 'id', 'created_by', 'url_to_parse', 'org', 'subscription_by_cal')
#        exclude = ('rrule', 'status', 'created_by', 'created_on', 'edited_on')
#        exclude = ('subscribers', )

    # def to_native(self, obj):
    #     logger = logging.getLogger(__name__)
    #     logger.debug("-- obj %s -- " % obj)
    #     return self
    #
    # def from_native(self, data):
    #     logger = logging.getLogger(__name__)
    #     logger.debug("-- data %s -- " % data)
    #     return self



class CalSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyCalendar
        fields = ('title',)

class OrgSerializer(serializers.ModelSerializer):
    cals = CalSerializer(many=True)
    class Meta:
        model = MyOrganisation
        fields = ('name', 'ref', 'mycalendar_set',)

class OrgPortalSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyOrganisation
        fields = ( 'alias','location','region',)



class CalendarPortalSerializer(serializers.ModelSerializer):
    #subscription_by_cal = SubscriptionSerializer(many=True, read_only=True)
    #category = MyCategorySerializer(many=False, read_only=True)
    category = serializers.StringRelatedField()
    org = OrgPortalSerializer(many=True, read_only=True)
    #created_by = serializers.StringRelatedField()
    class Meta:
        model = MyCalendar
        # fields = ('title', )
        fields = ('id','title','category', 'org')
#        exclude = ('rrule', 'status', 'created_by', 'created_on', 'edited_on')
#        exclude = ('subscribers', )

    # def to_native(self, obj):
    #     logger = logging.getLogger(__name__)
    #     logger.debug("-- obj %s -- " % obj)
    #     return self
    #
    # def from_native(self, data):
    #     logger = logging.getLogger(__name__)
    #     logger.debug("-- data %s -- " % data)
    #     return self


    def validate(self, attrs):
        """
        Check that the start is before the stop.
        """
        logger = logging.getLogger(__name__)
        logger.debug("-- Debut -- ")
        for attr in attrs:
            logger.debug("%s %s" % (attr, attrs[attr]))
        return attrs


class OrganismSerializer(serializers.ModelSerializer):
    # mycalendar_set = serializers.HyperlinkedRelatedField(many=True, read_only=True,
    #                                                      view_name='mycalendar-detail')
    mycalendar_set = CalendarSerializer(many=True, read_only=True)

    class Meta:
        model = MyOrganisation
        fields = ('name', 'ref', 'mycalendar_set')

