# -*- coding: utf-8 -*-
__author__ = 'me'

import logging
import datetime


from django.utils import timezone
from django.db.models import Q
from django.contrib.auth.models import User
from eventsmgmt.models import Subscription
from eventsmgmt.models import MyEvent

from agenda.settings_eventsmgmt import DEFAULT_ATTENDEE_EMAIL

def search(keywords):
    """
    Define where keywords are searched in the event
    summary, description, location, attendee
    :param keywords:
    :return:
    a queryset
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut --")
    message = ''

    if keywords:
        queryset = MyEvent.objects.filter(Q(summary__icontains=keywords)
                                      | Q(description__icontains=keywords)
                                      | Q(location__icontains=keywords)
                                      | Q(attendee__icontains=keywords))
    else:
        queryset = MyEvent.objects.all()
    logger.debug("%d events found for keywords %s" % (queryset.count(), keywords))
    return queryset

def search_in_events(keywords=None):
    """
    Define where keywords are searched in the event
    summary, description, location, attendee
    :param keywords:
    :return:
    a queryset
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut --")
    message = ''

    queryset = MyEvent.all_published_events.filter(Q(summary__icontains=keywords)
                                      | Q(description__icontains=keywords)
                                      | Q(location__icontains=keywords)
                                      | Q(attendee__icontains=keywords))
    logger.debug("%d events found for keywords %s" % (queryset.count(), keywords))

    return queryset

def search_in_announces(keywords):
    """
    Define where keywords are searched in the event
    summary, description, location, attendee
    :param keywords:
    :return:
    a queryset
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut --")
    message = ''

    queryset = MyEvent.all_announces.filter(Q(summary__icontains=keywords)
                                      | Q(description__icontains=keywords)
                                      | Q(location__icontains=keywords)
                                      | Q(attendee__icontains=keywords))
    logger.debug("%d events found for keywords %s" % (queryset.count(), keywords))

    return queryset

def filter_events(keywords, category, user=None, include_past_event=None, created_by=None,
                  regions=None, start_date=None, end_date=None):
    """
    takes a queryset of events, and a user
    if "user" is not authenticated, the return results are partial
    category is EVT, ANN or ALL
    :return:
    list of tuples containing (event, subsciption_to_event)
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut --")
    message = ''

    if 'ALL' in category:
        queryset = search(keywords)
    if 'ANN' in category:
        queryset = search_in_announces(keywords)
    if 'EVT' in category:
        queryset = search_in_events(keywords)

    if  include_past_event:
        logger.debug("include_past_event")
    current_day = timezone.now().date()
    if user and user.is_authenticated():
        logger.debug("user %s log in"% user)
        if created_by:
            queryset = queryset.filter(created_by=created_by)
            logger.debug("%d events created by %s" % (queryset.count(), created_by))
        if not start_date or start_date > current_day:
            if not include_past_event:
                logger.debug("user log in, include_past_event missing >= %s given only "% current_day)
                queryset = queryset.filter(dtstart__gte=current_day)
    else:
        logger.debug("user not log in, only future events given")
        queryset = queryset.filter(dtstart__gte=current_day)
        if keywords:
            logger.debug("%d events found for keywords %s" % (queryset.count(), keywords))
        else:
            logger.debug("%d events found" % queryset.count())
    if regions:
        queryset = queryset.filter(Q(org__region__in=regions)|Q(calendar__org__region__in=regions))
        logger.debug(u'found %d evts for location %s' % (queryset.count(), regions))
    if start_date:
        message += "events starting after %s " % start_date
        queryset = queryset.filter(dtstart__gte=start_date)
        logger.debug(u'found %d evts after %s' % (queryset.count(), start_date))
    if end_date:
        message += "events before %s " % end_date
        queryset = queryset.filter(dtstart__lte=end_date)
        logger.debug(u'found %d evts for before %s' % (queryset.count(), end_date))
    queryset = queryset.order_by('dtstart')

    return queryset

    #
    # ret = {
    #     'lines': lines,
    #     'nb_lines': queryset.count(),
    # }
    # return ret


def get_default_subscription(user):
    """
    if tne user has no default subscription, it creates a new one, with the email if it is known
    :param user:
    :return:
    """
    logger = logging.getLogger(__name__)
    message = ''
    my_default_subscription = None
    if user.is_authenticated():
        try:
            up = User.objects.get(id=user.id).userprofile
            if up.my_default_subscription:
                my_default_subscription = up.my_default_subscription
            else:
                logger.debug("no default subscription")
                # we take the first calendar/subscription created
                if user.sub_created_by.all():
                    my_default_subscription = user.sub_created_by.order_by('created_on')[0]
                    logger.debug(
                        'default personnal calendar is the first one created %s %s' % (user, my_default_subscription))
                else:
                    my_default_subscription = Subscription.objects.create(created_by=user)
                    logger.debug('default personnal calendar created %s %s' % (user, my_default_subscription))
                up.my_default_subscription = my_default_subscription
                up.save()
                user.save()

                if not my_default_subscription.description:
                    my_default_subscription.description = "My personnal calendar"
                if not my_default_subscription.email or my_default_subscription.email == DEFAULT_ATTENDEE_EMAIL:
                    if user.email:
                        my_default_subscription.email = user.email
                my_default_subscription.save()
        except User.DoesNotExist, e:
            logger.error("%s" % e)
    else:
        return None

    return my_default_subscription


