__author__ = 'me'

from django.conf.urls import patterns, url, include

from rest_framework.urlpatterns import format_suffix_patterns
from api.views import UserList, UserDetail
from api.views import MyEventList, EventDetail, CalendarList, CalendarDetail, \
    OrganisationList, OrganisationDetail, SubscriptionList, SubscriptionDetail
from api.views import CalendarSearchList, CalendarEventList
from api.views import CalendarBySubscriptionList
from api.views import SubscriptionByUserList
from api.views import AnnounceList, AnnounceDetail
from api.views import MyCategoryList
from api.views import MyCategoryDetail

urlpatterns = patterns('api.views',
                       url(r'^api/$', 'api_root'),
                       # TODO : un jour re regarder swagger
                       # url(r'^api/docs/', include('rest_framework_swagger.urls')),
                       url(r'^api/users/$', UserList.as_view(), name='user-list'),
                       url(r'^api/users/(?P<pk>\d+)/$', UserDetail.as_view(), name='user-detail'),

                       url(r'^api/events/$', MyEventList.as_view(), name='myevent-list'),

                       url(r'^api/events/(?P<pk>\d+)/$', EventDetail.as_view(), name='myevent-detail'),
                       url(r'^api/events/portal/$', 'events_portal'),
                       url(r'^api/events/portal/first$', 'first_events_portal'),

                       url(r'^api/announces/$', AnnounceList.as_view(), name='announces-list'),
                       url(r'^api/announces/(?P<pk>\d+)/$', AnnounceDetail.as_view(), name='announces-detail'),

                       url(r'^api/calendars/$', CalendarList.as_view(), name='mycalendar-list'),
                       url(r'^api/calendars/(?P<pk>\d+)/$', CalendarDetail.as_view(), name='mycalendar-detail'),
                       url(r'^api/calendars/(?P<pk>\d+)/events/$', CalendarEventList.as_view(), name='mycalendar-event-list'),
                       url(r'^api/calendars/cache/$', 'calendars_portal'),

                       url(r'^api/organisms/$', OrganisationList.as_view(), name='myorganisation-list'),

                       url(r'^api/subscriptions/$', SubscriptionList.as_view(), name='subscription-list'),
        #               url(r'^api/subscriptions/(?P<pk>\d+)/$', SubscriptionDetail.as_view(), name='subscription-detail'),
                        url(r'^api/subscriptions/user/(?P<user_id>\d+)/$', SubscriptionByUserList.as_view(), name='subscription-by-user'),
                       # url(r'^api/subscriptions/(?P<user_pk>\d+)/calendars/$',
                       #     CalendarBySubscriptionList.as_view(), name='calendarbysubscription-list'),
                       url(r'^api/categ/$', MyCategoryList.as_view(), name='mycategory-list'),
                       url(r'^api/categ/(?P<pk>\d+)/$', MyCategoryDetail.as_view(), name='mycategory-detail'),

                       url(r'^api/events/latest/$', 'event_latest'),
)

urlpatterns = format_suffix_patterns(urlpatterns)
