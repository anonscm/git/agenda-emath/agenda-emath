# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import ListView, DetailView
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotModified, HttpResponseServerError
from django.contrib.auth.decorators import login_required
from django.db.utils import IntegrityError
from django.db import transaction
from django.db import connection

from eventsmgmt.models import *
from eventsmgmt.forms import *
from agenda.version import my_version
from migrate import import_from_acm, import_sem_from_acm
from migrate import correspondances

from agenda.settings import *
from agenda.settings_eventsmgmt import *
from agenda.interface_annuaire import *

import logging
import requests

def corresp_labs(request):
    """
    debug correspondance labos ACM et LDAP
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    message = ''
    version = my_version()

    if request.user.is_authenticated():
        if request.user.is_staff or request.user.is_superuser:
            liste_acm = import_from_acm(False)
            return render_to_response('corresp_labs.html',
                                      {'message': message,
                                       'liste_acm': liste_acm,
                                       'liste_ldap': get_organisms(),
                                       'version': version, },
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/accounts/login/?next=%s' % request.path)


def debug(request):
    """
    affiche les id dans la table ACM et les id venant du LDAP
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    message = ''
    version = my_version()

    # TODO protege par htaccess pour l'instant
    if True:  # request.user.is_authenticated():
        if True:  # request.user.is_staff or request.user.is_superuser:
            liste_acm = import_from_acm(False)
            liste_ldap = get_organisms()

            liste_init = []
            # pour creer la liste correspondances dans migration.py
            nb = 0
            len_acm = len(liste_acm)
            len_ldap = len(liste_ldap)
            for item in range(max(len_acm, len_ldap)):
                if nb < len_acm:
                    left = liste_acm.keys()[nb]
                else:
                    left = ''
                if nb > len_ldap:
                    right = ''
                else:
                    right = liste_ldap.keys()[nb]
                liste_init.append([left, right])
                nb += 1
            # pour afficher les correspondances apres modification manuellle
            nb = 0
            liste_final = []
            if correspondances:
                logger.debug("correspondances existent")
                for item in correspondances:
                    acm = None
                    if item[0]:
                        acm = liste_acm[item[0]]
                    ldap = None
                    if item[1]:
                        ldap = liste_ldap[item[1]]
                        ldap = get_organism_id(item[1])
                    logger.debug(u'%s %s' % (acm, ldap))

                    liste_final.append([acm, ldap])
                    nb += 1

            else:
                for item in range(max(len_acm, len_ldap)):
                    if nb < len_acm:
                        left = liste_acm[nb]
                    else:
                        left = ''
                    if nb > len_ldap:
                        right = ''
                    else:
                        right = liste_ldap[nb]
                    liste_final.append([left, right])
                    nb += 1

            return render_to_response('corresp_labs.html',
                                      {'message': message,
                                       'liste_init': liste_init,
                                       'liste_final': liste_final,
                                       'liste_acm': liste_acm,
                                       'liste_ldap': liste_ldap,
                                       'version': version, },
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/accounts/login/?next=%s' % request.path)


def import_sem(request):
    """
    importe les seminaires de l'ACM
    si les labos existent toujours
    si les urls repondent
    """
    logger = logging.getLogger(__name__)
    logger.debug("-- Debut -- ")
    message = ''
    version = my_version()

    if request.user.is_authenticated():
        if request.user.is_staff or request.user.is_superuser:
            liste_acm = import_from_acm(False)
            liste_ldap = get_organisms()
            nb = 0
            liste_sems = []
            if correspondances:
                logger.debug("correspondances existent")
                for item in correspondances:
                    acm = None
                    if item[0]:
                        acm = liste_acm[item[0]]
                    ldap = None
                    if item[1]:
                        ldap = liste_ldap[item[1]]
                        ldap = get_organism_id(item[1])
                    logger.debug(u'cherche les seminaires de ACM: "%s" LDAP: "%s"' % (acm, ldap))
                    liste_sems = import_sem_from_acm(acm)

                    liste_sems.append([acm, ldap])
                    nb += 1

            return render_to_response('import_sem.html',
                                      {'message': message,
                                       'liste_sems': liste_sems,
                                       'version': version, },
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/accounts/login/?next=%s' % request.path)



