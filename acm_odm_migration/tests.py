"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from eventsmgmt.models import *
from acm_odm_migration.management.commands.site_acm import *

from django.test import TestCase


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)
        sem = {'academie': u'fr2', 'email': u'web@labomath.univ-orleans.fr', 'id': 55L, 'org': {'entitled': u"Math\xe9matiques et Applications, Physique Math\xe9matique d'Orl\xe9ans",         'id': 85,         'location': u'Orl\xe9ans',         'mail': u'Romain.Theron@univ-orleans.fr',         'uri': u'http://www.univ-orleans.fr/mapmo',
                                                                                                'wardship': u"CNRS - Universit\xe9 d'Orl\xe9ans"},
               's_URL': [u'http://www.univ-orleans.fr/mapmo/seminaires/seminaires.php?data=operateurs'],
               's_email': [u'jean.renault@univ-orleans.fr'],
               's_theme': u'anal',
               's_titre': [u"S\xe9minaire alg\xe8bres d'op\xe9rateurs du MAPMO",
                           u' <B>Orl\xe9ans</B>'],
               'titre': u"Laboratoire de Math., Appl. et Phys. Math. d'<B>Orl\xe9ans</B>",
               'url': u'http://www.univ-orleans.fr/SCIENCES/MAPMO/'}
        myorg, created = MyOrganisation.objects.get_or_create(ref=sem['org']['id'])
        if created:
            myorg.name = sem['org']['entitled']
            myorg.save()
        ajoute_sem(sem)

        sem = {'academie': u'fr3', 'email': u'Nikolay.Tzvetkov@math.univ-lille1.fr', 'id': 54L, 'org': {'entitled': u'Laboratoire Paul Painlev\xe9',         'id': 64,         'location': u'Lille',         'mail': u'Sebastien.Huart@math.univ-lille1.fr',         'uri': u'http://math.univ-lille1.fr/',         'wardship': u'CNRS - Universit\xe9 des Sciences et Technologies de Lille (Lille 1)'},
               's_URL': [u'http://math.univ-lille1.fr/seminaire/affiche.php?id=13'],
               's_email': [u'Nikolay.Tzvetkov@math.univ-lille1.fr'],
               's_theme': u'appl,anal',
               's_titre': [u"S\xe9minaire d'Analyse Num\xe9rique et Equations aux D\xe9riv\xe9es Partielles  du Laboratoire Paul Painlev\xe9 de l'USTL",
                           u' <B>Lille 1</B>'],
               'titre': u"Laboratoire Paul Painlev\xe9 de l' UST de <B>Lille</B> ",
               'url': u'http://math.univ-lille1.fr'}
        myorg, created = MyOrganisation.objects.get_or_create(ref=sem['org']['id'])
        if created:
            myorg.name = sem['org']['entitled']
            myorg.save()
        ajoute_sem(sem)

        sem = {'academie': u'fr3', 'email': u'Nikolay.Tzvetkov@math.univ-lille1.fr', 'id': 54L, 'org': {'entitled': u'Laboratoire Paul Painlev\xe9',         'id': 64,         'location': u'Lille',         'mail': u'Sebastien.Huart@math.univ-lille1.fr',         'uri': u'http://math.univ-lille1.fr/',         'wardship': u'CNRS - Universit\xe9 des Sciences et Technologies de Lille (Lille 1)'},
               's_URL': [u'http://math.univ-lille1.fr/seminaire/affiche.php?id=13'],
               's_email': [u'Nikolay.Tzvetkov@math.univ-lille1.fr'],
               's_theme': u'appl,anal',
               's_titre': [u"S\xe9minaire d'Analyse Num\xe9rique et Equations aux D\xe9riv\xe9es Partielles  du Laboratoire Paul Painlev\xe9 de l'USTL",
                           u' <B>Lille 1</B>'],
               'titre': u"Laboratoire Paul Painlev\xe9 de l' UST de <B>Lille</B> ",
               'url': u'http://math.univ-lille1.fr'}
        myorg, created = MyOrganisation.objects.get_or_create(ref=sem['org']['id'])
        if created:
            myorg.name = sem['org']['entitled']
            myorg.save()
        ajoute_sem(sem)


        print "tous les orgs"
        print MyOrganisation.objects.all()
        print "tous les calendriers"
        print MyCalendar.objects.all()
        print "tous les users"
        print User.objects.all()
