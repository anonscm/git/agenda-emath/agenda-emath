# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db import connections
from django.db.utils import ConnectionDoesNotExist

from eventsmgmt.models import *
from acm_odm_migration.migrate import *
from optparse import make_option

from icalendar import Calendar, Event
from icalendar.prop import vDDDTypes
import pytz
from django.utils.timezone import utc
from django.utils import timezone
import datetime
import logging
import os
import urllib
import requests
import vobject
from dateutil import parser
from bs4 import BeautifulSoup
import re
import lxml
import sys


class Command(BaseCommand):
    args = ''
    help = u'Importer les données du site ACM/SMAI'
    option_list = BaseCommand.option_list + (
        make_option('--modify-db',
                    action="store_true", dest="modify_db",
                    help='accept to modify the database. Use with caution!'),
        make_option("-n", type="int", dest="arret", default=10,
                    help='number of elements to treat (default=10)'),
        make_option("--no-control",
                    action="store_true", dest="no_control",
                    help="Don't test the url (network)"),
    )

    def handle(self, *args, **options):
        logger = logging.getLogger(__name__)

        # init
        liste_acm = import_from_acm(False)
        #liste_ldap = get_organisms()
        nb = 0
        liste_sems = []

        if correspondances:
            logger.debug("correspondances existent")
            for item in correspondances:
                acm = None
                if item[0]:
                    acm = liste_acm[item[0]]
                ldap = None
                if item[1]:
                    ldap = get_organism_id(item[1])
                else:
                    # le labo n a pas de correspondance dans le nouvel annuaire agenda
                    continue

                logger.debug(u'cherche les seminaires de ACM: "%s" LDAP: "%s"' % (acm['titre'], ldap['entitled']))
                liste_sems = import_sem_from_acm(acm['id'], no_test=options['no_control'])
                for sem in liste_sems:
                    sem['org'] = ldap  # on ajoute le detail sur l'organism
                    logger.debug(u'trouve: "%s" %s' % (sem['s_titre'][0], sem['s_URL'][0]))
                    if options['modify_db']:
                        ajoute_sem(sem)

                nb += 1
                if nb >= options['arret']:
                    logger.error("Beware stopped to %s components: FIXME after debug!!!!" % options['arret'])
                    break  # pour stopper sinon trp long



def ajoute_sem(sem):
    """
    ajoute un seminaire ACM dans la nouvelle BD
    on conserve le nom du createur connu par son adresse mail
    """
    logger = logging.getLogger(__name__)
    logger.debug(u"ajoute %s" % sem)
    try:
        myorg = MyOrganisation.objects.get(ref=sem['org']['id'])
    except MyOrganisation.DoesNotExist, e:
        logger.error(u"%s doesn't exist FIXME" % sem['org']['entitled'])
        return False
    queryset = User.objects.filter(email=sem['s_email'][0])
    if queryset:
        if len(queryset) == 1:
            user = queryset[0]
        elif len(queryset) > 1:
            logger.error("%d users avec le meme mail FIXME! %s" % (len(queryset), sem['s_email'][0]))
            return False
    else:
        user = User(username=sem['s_email'][0].split('@')[0], email=sem['s_email'][0])
        user.save()
    if user:
        queryset = MyCalendar.objects.filter(url_to_parse=sem['s_URL'][0])
        if queryset:
            if len(queryset) == 1:
                mycal = queryset[0]
            elif len(queryset) > 1:
                logger.error("%d cal avec le meme url FIXME! %s" % (len(queryset), sem['s_URL'][0]))
                return False
        else:
            mycal = MyCalendar(title=sem['s_titre'][0],
                               url_to_desc=sem['url'],
                               url_to_parse=sem['s_URL'][0],
                               contact=sem['s_email'][0],
                               parse='AC',
                               enabled=True,
                               created_by=user,
                               origin_if_imported='AC',
                               )
            mycal.save()
        mycal.org.add(myorg)

        return True
    else:
        return False