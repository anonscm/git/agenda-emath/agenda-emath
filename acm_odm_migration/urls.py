__author__ = 'me'

from django.conf.urls import patterns, url

urlpatterns = patterns('acm_odm_migration.views',
                       url(r'^corresp_labs/$', 'corresp_labs'),
                       url(r'^corresp_labs/debug/$', 'debug'),
                       url(r'^corresp_labs/import_sem/$', 'import_sem'),
                       )

