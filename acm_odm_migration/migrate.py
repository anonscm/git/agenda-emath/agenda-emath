# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db import connections
from django.db.utils import ConnectionDoesNotExist

from eventsmgmt.models import *
from agenda.interface_annuaire import *
from optparse import make_option


from icalendar import Calendar, Event
from icalendar.prop import vDDDTypes
import pytz
from django.utils.timezone import utc
from django.utils import timezone
import datetime
import logging
import os
import urllib
import requests
from requests import exceptions
import vobject
from dateutil import parser
from bs4 import BeautifulSoup
import re
import lxml
import sys


correspondances = [
    [54, 64],
    [55, 85],
    [56, ''],
    [57, 10],
    [58, ''],
    [59, ''],
    [60, ''],
    [61, ''],
    [62, 134],
    [63, 59],
    [64, 66],
    [65, 12],
    [66, 64],
    [67, 55],
    [68, 43],
    [69, 55],
    [70, 101],
    [71, 53],
    [72, 43],
    [73, 69],
    [74, 42],
    [75, ''],
    [76, ''],
    [77, 18],
    [78, 87],
    [79, 132],
    [80, 81],
    [81, 37],
    [82, ''],
    [83, 83],
    [84, 62],
    [85, 14],
    [86, 50],
    [87, ''],
    [88, 8],
    [89, ''],
    [90, 73],
    [91, 46],
    [92, 25],
    [93, 98],
    [94, ''],
    [95, ''],
    [96, 110],
    [97, 107],
    [98, 41],
    [99, 90],
    [100, ''],
    [101, ''],
    [102, 19],
    [103, 138],
    [104, ''],
    [105, ''],
    [106, ''],
    [107, 71],
    [108, 37],
    [109, 106],
    [110, 95],
    [111, ''],
    [112, ''],
    [113, ''],
    [114, ''],
    [115, ''],
    [116, ''],
    [117, ''],
    [118, 1],
    [119, 53],
    [120, 136],
    [121, ''],
]


def setup_cursor():
    logger = logging.getLogger(__name__)
    logger.debug("")
    database = 'acm'
    try:
        cursor = connections[database].cursor()
    except ConnectionDoesNotExist:
        logger.error('Legacy database is not configured "%s"\n' % database)
        return None
    return cursor


def search_org(lab, ldap):
    """
    cherche une correspondance entre le labo declare dans ACM et le labo declare dans LDAP
    """
    logger = logging.getLogger(__name__)
    logger.debug("")

    found = False
    for new_lab in ldap:
        logger.debug(u"examine %s in %s:q" % (lab['sigle'], new_lab))
        if lab['sigle'] in new_lab:
            logger.debug(u"Found %s in %s" % (lab['sigle']), new_lab)
            found = True
            break

    if found:
        return new_lab['']
    else:
        return None

def import_from_acm(modify_db):
    """
    inspire de http://jantoniomartin.tumblr.com/post/15233766067/django-how-to-import-data-from-an-external-database
    """
    logger = logging.getLogger(__name__)
    logger.debug("Import URL from ACM database")

    myCursor = setup_cursor()
    if myCursor is None:
        logger.error("unable to get cursor ")
        raise NameError("unable to get cursor")
    else:
        sql = "select  * from laboratoires order by id asc"
        myCursor.execute(sql)
        desc = myCursor.description
        cles = []
        existing_obj = 0
        new_obj = 0
        nb_error = 0
        for cle in desc:
            cles.append(cle[0])
        labos_acm = {}
        for row in myCursor.fetchall():
            obj_old = dict(zip(cles, row))
            #print obj_old
            labos_acm[obj_old['id']] = obj_old
            #labos_acm.append(obj_old)
            #print u'%s %s' % (obj_old['id'], obj_old['titre'])


    if True:
        return labos_acm

    correspondance = []
    labos_ldap = get_organisms()
    for lab in labos_acm:
        id = search_org(lab, labos_ldap)
        if id > 0:
            correspondance.append((lab.cle, ))
        else:
            logger.error(u"pas de correspondance pour %s FIXME" % lab)


def import_sem_from_acm(id_labo, no_test=None):
    """
    on recupere les urls de collecte pour le labo declare dans ACM
    """
    logger = logging.getLogger(__name__)
    logger.debug("Import sem URL from ACM database")

    seminaires = []
    myCursor = setup_cursor()
    if myCursor is None:
        logger.error("unable to get cursor ")
        raise NameError("unable to get cursor")
    else:
    # la liste des eminaires sur le site de l'ACM
        sql = "select s_titre,s_URL,s_email,s_theme,titre, laboratoires.id, url,email,academie from seminaires join laboratoires on laboratoires.sigle=seminaires.sigle_laboratory where laboratoires.id = %s order by seminaires.id" % id_labo
        myCursor.execute(sql)
        desc = myCursor.description
        cles = []
        existing_obj = 0
        new_obj = 0
        nb_error = 0
        for cle in desc:
            cles.append(cle[0])
        nb = 0
        for row in myCursor.fetchall():
            obj_old = dict(zip(cles, row))
            urls_collecte = []
            for url in obj_old['s_URL'].split(','):
                if not url:
                    nb_error += 1
                    continue
                # test url
                if no_test:
                    urls_collecte.append(url)
                else:
                    try:
                        requests.get(url, timeout = 1)
                    except exceptions.Timeout, e:
                        nb_error += 1
                        logger.error("%s FORGET %s ?" % (e, url))
                    except exceptions.HTTPError, e:
                        nb_error += 1
                        logger.error("%s FORGET %s ?" % (e, url))
                    except exceptions.ConnectionError, e:
                        nb_error += 1
                        logger.error("%s FORGET %s ?" % (e, url))
                    except:
                        nb_error += 1
                        logger.error("Error unable to test %s ?" % url)
                    else:
                        urls_collecte.append(url)
            if urls_collecte:
                obj_old['s_URL'] = urls_collecte
                mails = []
                for mail in obj_old['s_email'].split(','):
                    mails.append(mail)
                obj_old['s_email'] = mails
                titre = []
                for item in obj_old['s_titre'].split(','):
                    titre.append(item)
                obj_old['s_titre'] = titre
                seminaires.append(obj_old)
            #print obj_old
                nb += 1
            else:
                logger.debug(u'%s dropped because urls errors' % obj_old)

    logger.debug("%d seminaires ok %d en erreur " % (nb, nb_error))
    return seminaires
